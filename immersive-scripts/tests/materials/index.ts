import { Color, Entity, experience, Material, MeshRenderableComponent } from "arsdk";
import { Interpolator, TweenComponent, TweenDirection } from "arsdk/tween";
import {
  assertEqual,
  assertNull,
  assertStrictEqual,
  assertColorsEqual,
  assertThrows,
  assert,
  assertNotStrictEqual,
  assertColorsAlmostEqual,
} from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

function checkBigCubeMaterials(rootEntity: Entity) {
  const cube1 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "BigCube1";
  })!;
  const cube2 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "BigCube2";
  })!;

  let material = cube1.getComponent(MeshRenderableComponent)!.material;

  //check all uniforms that are defined in the GLTF are available
  assertColorsAlmostEqual(material.color, new Color(0.8, 0, 0, 0.5), "Material uniform doesn't match GLTF definition");
  assertEqual(material.opacity, 1.0), "Material is transparent so it must have an opacity factor";
  assertEqual(material.metalness, 0), "Material uniform doesn't match GLTF definition";
  assertEqual(material.roughness, 0.5, "Material uniform doesn't match GLTF definition");
  assertColorsEqual(material.emissive!, new Color(0, 0, 0, 1), "Material uniform doesn't match GLTF definition");

  assertStrictEqual(material, cube2.getComponent(MeshRenderableComponent)!.material, "BigCube1 and BigCube2 should share the same material");

  //test material clone
  let clonedMaterial = material.clone();
  assertColorsEqual(clonedMaterial.color, material.color);
  cube1.getComponent(MeshRenderableComponent)!.material = clonedMaterial;
  assertStrictEqual(cube1.getComponent(MeshRenderableComponent)!.material, clonedMaterial);
  assertNotStrictEqual(clonedMaterial, cube2.getComponent(MeshRenderableComponent)!.material, "BigCube1 material was cloned");
  clonedMaterial.color.set(0.0, 1.0, 0.0, 1.0);
  assertColorsEqual(clonedMaterial.color, new Color(0.0, 1.0, 0.0, 1.0));
  assertColorsAlmostEqual(cube2.getComponent(MeshRenderableComponent)!.material.color, new Color(0.8, 0, 0, 0.5));
}

function checkDefaultMaterial(rootEntity: Entity) {
  const sphere = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Sphere";
  })!;
  const plane = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Plane";
  })!;
  const meshRenderable = sphere.getComponent(MeshRenderableComponent)!;
  assertEqual(meshRenderable.materials.length, 1);
  assertThrows(() => {
    meshRenderable.setMaterialAt(1, meshRenderable.material);
  }, "Can not set material to index 1. Material only has 1 primitive.");
  assertStrictEqual(meshRenderable.material, meshRenderable.materials[0]);
  const material = meshRenderable.material;

  //Sphere and Plane have no material defined in the GLTF. The asset pipeline then adds a default material for each of these.
  assertNotStrictEqual(material, plane.getComponent(MeshRenderableComponent)!.material, "Sphere and Plane should not share the same (default) material");

  //check all uniforms of the default material
  assertColorsEqual(material.color!, new Color(1, 1, 1, 1));
  assertEqual(material.metalness, 1);
  assertEqual(material.roughness, 1);
  assertColorsEqual(material.emissive!, new Color(0, 0, 0, 1));
  assertNull(material.opacity);

  material.metalness = 1.0;
  material.roughness = 0.0;
  material.emissive?.set(0.0, 0.0, 0.0);
}

experience.addImmersiveObjectLifecycleListeners(CUBE_SCENE_OBJECT_UID, (rootEntity) => {
  checkBigCubeMaterials(rootEntity);
  checkDefaultMaterial(rootEntity);

  const sphere = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Sphere";
  })!;

  const meshRenderable = sphere.getComponent(MeshRenderableComponent)!;
  assertEqual(meshRenderable.materials.length, 1);
  assertThrows(() => {
    meshRenderable.setMaterialAt(1, meshRenderable.material);
  }, "Can not set material to index 1. Material only has 1 primitive.");
  assertStrictEqual(meshRenderable.material, meshRenderable.materials[0]);
  const sphereMaterial = meshRenderable.material;

  //test color reference semantics
  assertEqual(sphereMaterial.color.r, 1);
  let sphereMaterialColorReference = sphereMaterial.color;
  sphereMaterialColorReference.r = 0.2;
  assertEqual(sphereMaterial.color.r, 0.2);
  sphereMaterial.color.set(3, 0, 1);
  assertEqual(sphereMaterial.color.r, 3);
  sphereMaterial.color.r = 0.3;
  assertEqual(sphereMaterialColorReference.r, 0.3);
  assertEqual(sphereMaterial.color.r, 0.3);

  sphereMaterial.opacity = 0.5;

  //fade material in and out
  sphere.addComponent(
    new TweenComponent(
      0,
      1,
      3.0,
      (entity: Entity, a: number) => {
        sphereMaterial.setFade(a);
      },
      Interpolator.easeInOutQuad,
      null,
      TweenDirection.Alternate,
      true
    )
  );

  //test shadow casting/receiving, material reassignement
  const cubesRoot = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cubes";
  })!;

  let i = 0;
  cubesRoot.forEachChild((cube) => {
    let renderable = cube.getComponent(MeshRenderableComponent)!;
    renderable.material.color.set(Math.random(), Math.random(), Math.random(), 0);

    if (i < 20) {
      let materialsArray = renderable.materials;
      renderable.setMaterialAt(0, sphereMaterial);
      assertStrictEqual(materialsArray[0], sphereMaterial);
    }
    if (i++ % 2 == 0) {
      cube.transform.localPosition.y += 1.0;
      renderable.castShadows = false;
      if (i % 4 == 0) {
        renderable.receiveShadows = false;
        cube.transform.localPosition.y += 0.2;
      }
    } else {
      cube.transform.localPosition.y += 2.0;
    }
  });

  assertEqual(i, 278);

  console.log("All tests succeeded! :)");
});

//TODO more tests for different materials on different GLTFs
