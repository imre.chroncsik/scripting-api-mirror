const path = require("path");
const typescript = require("typescript");
const webpack = require("webpack");
const webpackConfig = require("./webpack.config");

module.exports = {
  intellisenseCompilerOptions: typescript.parseJsonConfigFileContent(
    require("./intellisense.tsconfig.json"),
    typescript.sys,
    __dirname
  ).options,

  webpackConfig,

  compile: (platform, input, output) =>
    new Promise((resolve, reject) =>
      webpack(
        webpackConfig(
          {
            ...process.env,
            SCRIPTING_TARGET: platform,
            SCRIPTING_INPUT: input,
            SCRIPTING_OUTPUT: output,
          },
          {
            mode: "production",
          }
        ),
        (err, stats) => {
          if (err) {
            return reject(err);
          }
          if (stats.hasErrors()) {
            return reject(new Error(JSON.stringify(stats.toJson().errors)));
          }
          return resolve();
        }
      )
    ),
};
