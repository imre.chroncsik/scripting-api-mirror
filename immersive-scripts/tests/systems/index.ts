import { experience, System } from "arsdk";
import { assert, assertEqual } from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

class TestSystem extends System {
  numUpdates = 0;
  update(dt: number): void {
    assert(dt > 0);
    this.numUpdates++;
  }
}
let testSystem = new TestSystem();
let testSystem2 = new TestSystem();

class ValidatorSystem extends System {
  numUpdates = 0;
  failed = false;
  update(dt: number): void {
    if (this.failed) return;
    this.failed = true;
    //systems should be updated in the order that they have been added
    assertEqual(testSystem.numUpdates, this.numUpdates);
    assertEqual(testSystem2.numUpdates, this.numUpdates);
    this.numUpdates++;
    this.failed = false;

    if (this.numUpdates == 60) {
      console.log("All tests succeeded! :)");
    }
  }
}

experience.addSystem(new ValidatorSystem());

//removing a system that is not added does not have an effect
experience.removeSystem(testSystem);
experience.addSystem(testSystem);
//adding the system twice should not have an effect
experience.addSystem(testSystem);
//adding another instance of the same system is allowed
experience.addSystem(testSystem2);
