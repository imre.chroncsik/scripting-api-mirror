/* Expert mode for Interactive Engine. */
import * as arsdk from "arsdk";
import * as tween from "arsdk/tween";
import { ObjectId, SoundId } from "./enums";
import {
  Entity,
  MeshRenderableComponent,
  BoxColliderComponent,
  experience,
  OrbitCameraControllerComponent,
  OnTapComponent,
  Vector3,
  ModelAnimatorComponent,
  Animation,
} from "arsdk";

let currentHotspot: Entity | null = null;
let triggers = new Array<Entity>();
let textBoxes = new Array<Entity>();
let cameraAnimations = new Array<Animation>();
let currentIntroAnimation = 0;
let introPlaying = false;

class BillboardComponent extends arsdk.Component {}

class HotspotTriggerComponent extends arsdk.Component {
  startScale: Vector3;
  constructor(startScale: Vector3) {
    super();
    this.startScale = startScale;
  }
}

class CameraFollowComponent extends arsdk.Component {
  camera: Entity;
  target: Entity;
  animation: Animation;
  orbitCameraController: OrbitCameraControllerComponent;
  constructor(animation: Animation, camera: Entity, target: Entity, orbitCameraController: OrbitCameraControllerComponent) {
    super();
    this.camera = camera;
    this.target = target;
    this.animation = animation;
    this.orbitCameraController = orbitCameraController;
  }
}

class HotspotTriggerAnimationSystem extends arsdk.System {
  totalTime = 0;
  billboardRotationOffset = new arsdk.Quaternion()
    .setFromAxisAngle(new Vector3(1, 0, 0), -90)
    .multiply(new arsdk.Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), 180));

  update(dt: number): void {
    this.totalTime += dt;
    let factor = (Math.sin(this.totalTime) * 0.5 + 0.5) * 5;
    experience.forEachComponent(HotspotTriggerComponent, (comp) => {
      comp.entity!!.transform.localScale.copy(comp.startScale);
      comp.entity!!.transform.localScale.multiplyScalar(1 + (0.1 * factor - 0.12));
      comp.entity!!.getComponent(arsdk.MeshRenderableComponent)!!.material.emissive!!.set(factor, factor, factor, 1);
    });

    if (!experience.currentMode.isAR) {
      experience.forEachComponent(BillboardComponent, (comp) => {
        comp.entity!!.transform.lookAt(experience.currentMode.camera.transform.worldPosition);
        comp.entity!!.transform.localRotation.multiply(this.billboardRotationOffset);
      });
    }

    experience.forEachComponent(CameraFollowComponent, (comp) => {
      comp.camera.transform.worldPosition.copy(comp.entity!!.transform.worldPosition);
      // comp.camera.transform.worldRotation.copy(comp.entity!!.transform.worldRotation);

      comp.orbitCameraController.pivotPosition.copy(comp.target.transform.worldPosition);

      // console.log("following camera");
      if (!comp.animation.isPlaying()) {
        comp.entity!!.removeComponent(comp);
        //re-add orbit camera controller to give user control
        // comp.camera.addComponent(comp.orbitCameraController);
        triggers.forEach((trigger) => {
          trigger.setActive(true);
        });
      }
    });

    // if (introPlaying) {
    //   let currentAnim = cameraAnimations[currentIntroAnimation];
    //   if (!currentAnim.isPlaying()) {
    //     currentIntroAnimation++;
    //     if (currentIntroAnimation >= cameraAnimations.length) {
    //       introPlaying = false;
    //       console.log("intro finished");

    //       experience.forEachComponent(CameraFollowComponent, (comp) => {
    //         comp.entity!!.removeComponent(comp);
    //       });
    //       triggers.forEach((trigger) => {
    //         trigger.setActive(true);
    //       });
    //     } else {
    //       cameraAnimations[currentIntroAnimation].play();
    //       console.log("playing next camera animation " + currentIntroAnimation);
    //     }
    //   }
    // }
  }
}

experience.addSystem(new HotspotTriggerAnimationSystem());

function moveToHotspot(camera: Entity, pivotPos: Vector3, cameraPos: Vector3, hotspot: Entity, triggerEntity: Entity, textbox: Entity | null) {
  camera.getComponent(OrbitCameraControllerComponent)!.animateTo(pivotPos, cameraPos, 1);
  currentHotspot = hotspot;

  triggers.forEach((trigger) => {
    trigger.setActive(true);
  });
  triggerEntity.setActive(false);

  textBoxes.forEach((textbox) => {
    textbox.setActive(false);
  });
  textbox?.setActive(true);

  //   //fade in textbox over time
  //   textbox?.addComponent(new tween.TweenComponent(0, 1, 1, (entity, a) => {
  //       entity.getComponent(arsdk.MeshRenderableComponent)!.materials.forEach((m) => {
  //           m.setFade(a)
  //       })
  //       entity.getChild(0)!.getComponent(arsdk.MeshRenderableComponent)!.material.setFade(a)
  //   }))
}

experience.addImmersiveObjectLifecycleListeners(
  ObjectId.Object1,
  (rootEntity) => {
    let shadowCatcher = rootEntity
      .findDescendantInHierarchy((entity) => {
        return entity.name == "ShadowCatcher";
      })!!
      .getComponent(MeshRenderableComponent)!!;
    shadowCatcher.castShadows = false; //can be authored with GLTF extra
    shadowCatcher.material.setFade(0.999); //force to fade mode for now //TODO should be authored in GLTF

    if (!experience.currentMode.isAR) {
      let camera = experience.currentMode.camera;

      cameraAnimations = rootEntity
        .getComponent(ModelAnimatorComponent)!!
        .getAnimations()
        .filter((anim) => {
          return anim.name.search("Camera") != -1;
        });

      let introAnimation = cameraAnimations[0];
      console.log("animations " + cameraAnimations.length);
      introAnimation.play();
      let cameraFollowEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "Camera_1_Animated_Intro_Orientation";
      })!!;
      let cameraTarget = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "Target_Animated";
      })!!;
      introPlaying = true;

      let orbitCameraController = camera.getComponent(OrbitCameraControllerComponent);
      cameraFollowEntity.addComponent(new CameraFollowComponent(introAnimation, camera, cameraTarget, orbitCameraController!));

      let hotspotsEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "hotspots";
      })!!;

      hotspotsEntity.forEachChild((hotspot) => {
        let pivotEntity = hotspot.findChild((entity) => {
          return entity.name.search("cameraPivot") == 0;
        });

        if (pivotEntity == null) {
          console.log("could not find cameraPivot");
        }

        if (pivotEntity) {
          let cameraPos = pivotEntity.getChild(0)!!.transform.worldPosition;

          let triggerEntity = hotspot.findChild((entity) => {
            return entity.name.search("trigger") == 0;
          })!!;
          triggerEntity.addComponent(new HotspotTriggerComponent(triggerEntity.transform.localScale.clone()));

          triggers.push(triggerEntity);
          let textbox = hotspot.findChild((entity) => {
            return entity.name.search("Card") == 0;
          });
          if (textbox) {
            textBoxes.push(textbox);
            textbox.addComponent(new BillboardComponent());
          }

          triggerEntity.addComponent(
            new OnTapComponent((tapWorldPosition) => {
              if (currentHotspot != hotspot) {
                moveToHotspot(camera, pivotEntity!!.transform.worldPosition, cameraPos, hotspot, triggerEntity, textbox);
              }
            })
          );
        }
      });

      textBoxes.forEach((textbox) => {
        textbox.setActive(false);
      });

      if (introPlaying) {
        triggers.forEach((trigger) => {
          trigger.setActive(false);
        });
      }

      // console.log("found " + triggers.length + " hotspots");
    }
  },
  () => {
    triggers.length = 0;
    textBoxes.length = 0;
    currentHotspot = null;
    currentIntroAnimation = 0;
    introPlaying = false;
    cameraAnimations.length = 0;
  }
);
