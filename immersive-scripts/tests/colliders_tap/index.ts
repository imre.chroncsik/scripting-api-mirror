import { BoxColliderComponent, Entity, experience, MeshRenderableComponent, OnTapComponent, Quaternion, System, Vector3 } from "arsdk";
import { assert, assertEqual, assertNotNull, assertNull, assertThrows } from "../../../lib/assert";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/CfsZD
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/ios/index.json
 * Web: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/web/index.json
 */
//uses animations test experience
assertEqual(experience.uid, "fa3f5772-6522-475f-9f90-fbaf20a6a5cb", "Wrong experience uid");

const tests = new Map<string, Boolean>();
const totalNumberOfTests = 4;
function testComplete(key: string) {
  if (tests.get(key)) {
    return; //already completed
  }
  tests.set(key, true);
  let completedCount = 0;
  tests.forEach((element) => {
    if (element) {
      completedCount++;
    }
  });
  console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
  if (completedCount == totalNumberOfTests) {
    console.log("All tests succeeded! :)");
  }
}

let totalTime = 0;

class TestSystem extends System {
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    experience.forEachComponent(MeshRenderableComponent, (meshRenderable) => {
      meshRenderable.material.color.set(
        meshRenderable.material.color.r - 1 * dt,
        meshRenderable.material.color.g - 1 * dt,
        meshRenderable.material.color.b - 1 * dt,
        1
      );
    });

    if (this.entity) {
      const plane = this.entity.findDescendantInHierarchy((entity) => {
        return entity.name == "Plane";
      })!;

      totalTime += dt;

      plane.transform.worldRotation.multiply(new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), 10 * dt));
      plane.transform.worldPosition.x = Math.sin(totalTime * 0.5) * 1;
    }
  }
}

let testSystem = new TestSystem();

//test destroying an entity while traversing the onTap chain
function destroyTest(rootEntity: Entity) {
  const cube0 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cube";
  })!;

  const cube1 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cube.001";
  })!;

  const cube2 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cube.002";
  })!;
  const cube3 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cube.003";
  })!;
  cube1.parent = cube0;
  cube2.parent = cube0;
  cube3.parent = cube0;

  let cube1Tapped = false;
  let cube2Tapped = false;
  let cube3Tapped = false;

  cube0.addComponent(
    new OnTapComponent(() => {
      assert(!cube2Tapped, "This onTap component should not trigger if a child was tapped");
      cube1Tapped = false;
      testComplete("cube0 directly triggered");
    }, false)
  );

  cube0.addComponent(
    new OnTapComponent(() => {
      // console.log("cube0 onTap triggered");
      assert(!cube1Tapped, "cube1 was already destroyed, it should not have triggered onTapComponent");
      assert(!cube3Tapped, "cube0 was already destroyed, it should not have triggered onTapComponent");

      if (cube2Tapped) {
        cube2Tapped = false;
        testComplete("cube2 tapped, triggered parent");
      }
    }, true)
  );

  cube1.addComponent(
    new OnTapComponent(() => {
      // console.log("cube1 onTap triggered");
      cube1Tapped = true;
      cube1.destroy();
      testComplete("cube1 tapped");
    }, true)
  );

  cube2.addComponent(
    new OnTapComponent(() => {
      // console.log("cube2 onTap triggered");
      cube1Tapped = false;
      cube2Tapped = true;
    }, true)
  );

  cube3.addComponent(
    new OnTapComponent(() => {
      // console.log("cube3 onTap triggered");
      cube1Tapped = false;
      cube3Tapped = true;
      cube0.destroy();
      testComplete("cube3 tapped. All cubes destroyed.");
    }, true)
  );
  cube0.transform.worldPosition.y += 1.0;

  let i = 0;
  cube0.forEachChild((child) => {
    child.transform.localPosition.set(i * 3, -2.2, 0);
    i++;
  });
  //destroy all others
  rootEntity.getChild(0)!.forEachChild((c) => {
    if (c != cube0) {
      c.destroy();
    }
  });
}

let test1 = false;
//9 cubes
experience.addImmersiveObjectLifecycleListeners(
  "9b793829-cd68-4f2d-8f0e-85dddb009ff6",
  (rootEntity) => {
    test1 = !test1;
    //would be nicer to create experience with a separate object
    if (!test1) {
      destroyTest(rootEntity);
      console.log("Test 2 initialized: Tap all cubes");
      return;
    }
    console.log("Test 1 initialized: Tap all objects");

    experience.addSystem(testSystem);

    var colliderCount = 0;
    rootEntity.parent!.forEachDescendantInHierarchy((child) => {
      let collider = child.getComponent(BoxColliderComponent);
      if (collider) {
        colliderCount++;
      }
    });
    assertEqual(colliderCount, 10, "Invalid collider count");

    testSystem.setEntity(rootEntity);

    let entity = new Entity();
    assertThrows(() => {
      //This should throw an exception, as there is no way this OnTap component is ever triggered.
      //In a later version of the API when we allow adding custom colliders later, then we can remove the exception.
      entity.addComponent(new OnTapComponent(() => {}));
    }, "Should not be able to attach onTap Component to Entity that does not have a collider");
    entity.destroy();

    rootEntity.addComponent(
      new OnTapComponent(() => {
        rootEntity.forEachDescendantInHierarchy((entity) => {
          let renderable = entity.getComponent(MeshRenderableComponent);
          if (renderable) {
            assert(tapTriggeredCount >= 1, "Child OnTap should have been called before root onTap");
            renderable.material.color.b = 1;
          }
        });
      }, true)
    );

    let lastTriggeredTapEntity: Entity | null = null;
    //checking if OnTap components are triggered in correct order. Correct order is from child to parent in the order that the components were attached
    var tapTriggeredCount = 0;

    rootEntity.getChild(0)!.forEachChild((cube) => {
      if (cube.getComponent(BoxColliderComponent) != null) {
        assertNotNull(cube.getComponent(BoxColliderComponent), "Entity should have a box collider");
        cube.addComponent(
          new OnTapComponent((worldPosition) => {
            // testing  tap order
            assertNull(lastTriggeredTapEntity, "Was the other onTap component not triggered?");
            lastTriggeredTapEntity = cube;
            console.log("cube " + cube + " tapped #1");
            tapTriggeredCount = 0;
          })
        );

        //testing multiple onTapComponents on the same Entity
        cube.addComponent(
          new OnTapComponent((worldPosition) => {
            assertEqual(cube, lastTriggeredTapEntity, "The other OnTap component should have been triggered before this");
            console.log("cube " + cube + " tapped #2");
            let renderable = cube.getComponent(MeshRenderableComponent);
            if (renderable) {
              renderable.material.color.g = 1;
            }
            lastTriggeredTapEntity = null;
            tapTriggeredCount++;
          })
        );
      }
    });

    const cube0 = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube";
    })!;

    const cube1 = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.001";
    })!;

    const cube2 = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.002";
    })!;

    cube1.parent = cube0;
    cube2.parent = cube0;

    cube0.addComponent(
      new OnTapComponent(() => {
        let renderable = cube0.getComponent(MeshRenderableComponent);
        if (renderable) {
          renderable.material.color.r = 1;
        }
        cube0.forEachDescendantInHierarchy((entity) => {
          let renderable = entity.getComponent(MeshRenderableComponent);
          if (renderable) {
            renderable.material.color.r = 1;
          }
        });
        assertEqual(tapTriggeredCount, 1, "OnTap on child cubes should have been called before this");
        tapTriggeredCount++;
      }, true)
    );
    cube0.transform.worldPosition.x = -1.5;
    cube1.transform.localPosition.y -= 0.5;
    cube2.transform.localPosition.y -= 0.5;
  },
  () => {
    testSystem.setEntity(null);
    experience.removeSystem(testSystem);
  }
);
