
/*
  temporary, to be removed later.
  slightly modified version of `transform_maths/index.ts`, optimized for iOS.
  differences:
  * when running on iOS, it turns on a debug flag that affects the order
    in which nodes are visited in findDescendantInHierarchy().
  * TestSystem.setEntity() finds the "Stuff" node first,
    then uses that as the root for searching some others.
  * updates of the small cubes can be disabled.
*/

import { Entity, experience, System, Vector3 } from "arsdk";
import { assert, assertVectorsAlmostEqual } from "../../../lib/assert";
import { CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

if (experience.platform === "iOS") {
  console.log("ios platform version: ", experience.platformVersion)
  ;(globalThis as any)["arsdk_ios_findDescendantChecksChildrenFirst"] = true
}
const disableCubesUpdates = true

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

//tests for Transform.lookat, localToWorldPoint, worldToLocalPoint, localToWorldDirection, worldToLocalDirection
//see https://www.youtube.com/watch?v=p2kyTka0uXc

class TestSystem extends System {
  entity: Entity | null = null;
  cube: Entity | null = null;
  cube2: Entity | null = null;
  sphere: Entity | null = null;
  cubes: Entity | null = null;
  setEntity(entity: Entity | null) {
    this.entity = entity;

    if (entity) {

      const stuff = entity.findDescendantInHierarchy(e => e.name == "Stuff")!

      this.cube = stuff.findDescendantInHierarchy((entity) => {
        return entity.name == "BigCube1";
      })!;
      this.cube.transform.localScale.set(0.2, 0.5, 2);

      this.cube2 = stuff.findDescendantInHierarchy((entity) => {
        return entity.name == "BigCube2";
      })!;
      this.cube2.transform.localScale.set(0.2, 0.5, 2);

      this.sphere = stuff.findDescendantInHierarchy((entity) => {
        return entity.name == "Sphere";
      })!;
      stuff.findDescendantInHierarchy((entity) => {
        return entity.name == "Plane";
      })!.transform.localPosition.x += 1000;
      this.sphere.transform.localScale.set(0.5, 0.5, 0.5);

      if (disableCubesUpdates !== true) {
        this.cubes = entity.findDescendantInHierarchy((entity) => {
          return entity.name == "Cubes";
        })!;
        this.cubes!.forEachChild((cube) => {
          cube.transform.localScale.set(0.2, 0.5, 2);
        });
      }
    } else {
      this.cube = null;
    }
  }
  time: number = 0;
  update(dt: number): void {
    this.time += dt;

    if (!experience.currentMode.isAR) {
      if (this.cube) {
        this.sphere!.transform.localPosition.set(2 * Math.sin(this.time * 0.4), Math.sin(2 * this.time * 0.4), 2 * Math.cos(this.time * 0.4));
        this.cube.transform.lookAt(this.sphere!.transform.worldPosition, new Vector3(1, 1, 0).normalize());

        this.cube2!.transform.lookAt(this.sphere!.transform.worldPosition);

        if (disableCubesUpdates !== true) {
          this.cubes!.forEachChild((cube) => {
            cube.transform.lookAt(this.sphere!.transform.worldPosition);
          });
        }

        const v1 = this.cube.transform.localToWorldDirection(new Vector3(1, 0, 0))
        const v2 = new Vector3(1, 0, 0).applyQuaternion(this.cube.transform.worldRotation)
        assertVectorsAlmostEqual(
          this.cube.transform.localToWorldDirection(new Vector3(1, 0, 0)),
          new Vector3(1, 0, 0).applyQuaternion(this.cube.transform.worldRotation)
        );
        assertVectorsAlmostEqual(
          this.cube.transform.worldToLocalDirection(new Vector3(1, 0, 0)),
          new Vector3(1, 0, 0).applyQuaternion(this.cube.transform.worldRotation.clone().invert())
        );
        assertVectorsAlmostEqual(this.sphere!.transform.localToWorldPoint(new Vector3()), this.sphere!.transform.worldPosition);
        assertVectorsAlmostEqual(this.sphere!.transform.worldToLocalPoint(this.sphere!.transform.worldPosition.clone()), new Vector3());
      }
    }
  }
}
let testSystem = new TestSystem();

experience.addSystem(testSystem);
experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    testSystem.setEntity(rootEntity);
  },
  (rootEntity) => {
    testSystem.setEntity(null);
  }
);
