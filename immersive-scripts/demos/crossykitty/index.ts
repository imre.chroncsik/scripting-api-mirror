import {
  Animation,
  AudioSourceComponent,
  Component,
  Entity,
  experience,
  Material,
  MeshRenderableComponent,
  ModelAnimatorComponent,
  OnTapComponent,
  OrbitCameraControllerComponent,
  Quaternion,
  System,
  Vector3,
} from "arsdk";
import { assertEqual } from "../../../lib/assert";
import { SoundId, ObjectId, experienceUid } from "./enums";

/**
 * https://short.staging.immersive.yahoo.com/Tkrvv
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/accd0222-6904-4f21-b6d5-bcdfe0e92ea4/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/accd0222-6904-4f21-b6d5-bcdfe0e92ea4/v1/ios/index.json
 * Web: TODO. Can't generate currently because of Composer bug.
 */
assertEqual(experience.uid, experienceUid, "Wrong experience uid");

const OBJECT_UID = ObjectId.Object1;

let inGame = false;
let wonGame = false;
const catTargetPosition = new Vector3();
const spawnPoint = new Vector3();

class CarComponent extends Component {
  speed: number = 1;
  material: Material | null = null;
  constructor(speed: number) {
    super();
    this.speed = speed;
  }
  changeColor() {
    if (!this.material) {
      let mainMeshRenderable = this.entity!.findChild((entity) => {
        return entity.name.indexOf("Main") !== -1;
      })!.getComponent(MeshRenderableComponent);
      this.material = mainMeshRenderable!.material.clone();
      mainMeshRenderable!.material = this.material;
    }
    let color = { r: Math.round(Math.random()), g: Math.round(Math.random()), b: Math.round(Math.random()) };
    if (color.r == 1 && color.g == 1 && color.b == 1) {
      color = { r: 0.7, g: 0.7, b: 0.7 };
    }
    this.material.color.set(color.r, color.g, color.b, 1);
  }
}

class CarSystem extends System {
  rootEntity: Entity | null = null;

  setRootEntity(rootEntity: Entity | null) {
    this.rootEntity = rootEntity;

    rootEntity!
      .findDescendantInHierarchy((entity) => {
        return entity.name == "CarsLeft";
      })!
      .forEachChild((car) => {
        car.addComponent(new CarComponent(2));
      });

    rootEntity!
      .findDescendantInHierarchy((entity) => {
        return entity.name == "CarsRight";
      })!
      .forEachChild((car) => {
        car.addComponent(new CarComponent(-1));
      });

    rootEntity!
      .findDescendantInHierarchy((entity) => {
        return entity.name == "CarsLeft.001";
      })!
      .forEachChild((car) => {
        car.addComponent(new CarComponent(5));
      });

    rootEntity!
      .findDescendantInHierarchy((entity) => {
        return entity.name == "CarsRight.001";
      })!
      .forEachChild((car) => {
        car.addComponent(new CarComponent(-3));
      });
  }

  update(dt: number): void {
    if (!inGame) return;
    experience.forEachComponent(CarComponent, (component) => {
      if (component.entity!.transform.localPosition.z > 8) {
        component.entity!.transform.localPosition.z -= 16;
        component.changeColor();
      }

      if (component.entity!.transform.localPosition.z < -8) {
        component.entity!.transform.localPosition.z += 16;
        component.changeColor();
      }
      component.entity!.transform.localPosition.z += component.speed * dt;
    });
  }
}

const cameraOffset = new Vector3(-4, 9, 3);

class CameraSystem extends System {
  camera: Entity | null = null;
  cat: Entity | null = null;
  rootEntity: Entity | null = null;
  cameraTarget: Vector3 = new Vector3();
  tmpVector: Vector3 = new Vector3();
  orbitCameraController: OrbitCameraControllerComponent | null = null;

  initialize(rootEntity: Entity, camera: Entity, cat: Entity) {
    this.rootEntity = rootEntity;
    this.camera = camera;
    this.cat = cat;
    this.orbitCameraController = camera.removeComponent(camera.getComponent(OrbitCameraControllerComponent)!);
  }

  update(dt: number): void {
    if (!inGame || wonGame) return;
    if (this.camera) {
      //let the camera follow the cat
      let camPos = this.tmpVector!.addVectors(this.cat!.transform.localPosition, cameraOffset);
      this.rootEntity!.transform.localToWorldPoint(camPos);

      this.camera.transform.worldPosition.copy(camPos);
      this.camera.transform.lookAt(this.cat!.transform.worldPosition);
    }
  }

  playWinAnimation() {
    if (this.camera) {
      this.camera.addComponent(this.orbitCameraController!);
      this.camera.getComponent(OrbitCameraControllerComponent)!.pivotPosition.copy(this.cat!.transform.worldPosition);

      let vectorFromCat = this.cat!.transform.localToWorldDirection(new Vector3(-0.08, 0.1, 0.2).multiplyScalar(0.7)).add(this.cat!.transform.worldPosition);
      let target = this.rootEntity!.transform.localToWorldPoint(new Vector3(0, 0.3, 0).add(this.cat!.transform.localPosition));
      this.camera.getComponent(OrbitCameraControllerComponent)!.animateTo(target, vectorFromCat, 5);
    }
  }
}
const cameraSystem = new CameraSystem();

class CatSystem extends System {
  rootEntity: Entity | null = null;
  cat: Entity | null = null;
  targetCircle: Entity | null = null;
  catAnimator: ModelAnimatorComponent | null = null;
  walking = false;
  tmpVector: Vector3 = new Vector3();
  tmpQuaternion: Quaternion = new Quaternion();
  yRotation180 = new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), 180);
  catWalkingAnimation: Animation | null = null;
  catIdleAnimation: Animation | null = null;
  catSitDownAnimation: Animation | null = null;
  catIdleSitAnimation: Animation | null = null;

  initialize(rootEntity: Entity, cat: Entity, animator: ModelAnimatorComponent, targetCircle: Entity) {
    this.rootEntity = rootEntity;
    this.cat = cat;
    this.catWalkingAnimation = animator.getAnimation("WalkCycle");
    this.catWalkingAnimation.loop = true;
    this.catIdleAnimation = animator.getAnimation("IdleTailSwoosh");
    this.catIdleAnimation.loop = true;
    this.catSitDownAnimation = animator.getAnimation("SitDown");
    this.catIdleSitAnimation = animator.getAnimation("IdleSit");
    this.catIdleSitAnimation.loop = true;
    this.targetCircle = targetCircle;
  }

  update(dt: number): void {
    if (wonGame && !this.catSitDownAnimation!.isPlaying() && !this.catIdleSitAnimation!.isPlaying()) {
      this.catIdleSitAnimation!.play();
    }
    if (!inGame || wonGame) return;
    if (this.cat) {
      experience.forEachComponent(CarComponent, (component) => {
        let carLocalPos = this.rootEntity!.transform.worldToLocalPoint(component.entity!.transform.worldPosition.clone());
        if (carLocalPos.distanceTo(this.cat!.transform.localPosition) < 0.75) {
          //cat died, respawn
          console.log("cat died");
          this.cat!.transform.localPosition.copy(spawnPoint);
          catTargetPosition.copy(spawnPoint);
          this.targetCircle!.setActive(false);
          return;
        }
      });

      if (this.cat!.transform.localPosition.x > 4.6) {
        //won the game
        console.log("Won!!!");
        this.targetCircle!.setActive(false);
        this.catSitDownAnimation!.time = 0;
        this.catSitDownAnimation!.play();
        wonGame = true;
        if (!experience.currentMode.isAR) {
          cameraSystem.playWinAnimation();
        }
        return;
      }

      let distanceToTarget = this.cat.transform.localPosition.distanceTo(catTargetPosition);
      if (distanceToTarget < 0.1) {
        this.walking = false;
        this.targetCircle!.setActive(false);
        this.catIdleAnimation!.time = 0;
        this.catIdleAnimation!.play();
        return;
      }
      if (!this.walking) {
        this.catWalkingAnimation!.time = 0;
        this.catWalkingAnimation!.play();
      }
      this.walking = true;

      //rotate cat slowly to face target
      let previousRot = this.tmpQuaternion.copy(this.cat.transform.localRotation);
      let catTargetWorldSpace = this.rootEntity!.transform.localToWorldPoint(catTargetPosition.clone());
      this.cat.transform.lookAt(catTargetWorldSpace);
      let targetRot = this.cat.transform.localRotation.clone();
      targetRot.multiply(this.yRotation180);
      previousRot.rotateTowards(targetRot, dt * 1000);
      this.cat.transform.localRotation.copy(previousRot);

      let walkDir = this.tmpVector.subVectors(catTargetPosition, this.cat.transform.localPosition).normalize();
      walkDir.multiplyScalar(dt * 2);
      this.cat.transform.localPosition.add(walkDir);
    }
  }
}

const carSystem = new CarSystem();
experience.addSystem(carSystem);

const catSystem = new CatSystem();
experience.addSystem(catSystem);

experience.addImmersiveObjectLifecycleListeners(
  OBJECT_UID,
  (rootEntity) => {
    inGame = true;
    wonGame = false;

    let backgroundAudio = new AudioSourceComponent();
    backgroundAudio.clip = experience.getAudioClip(SoundId.Interactive1);
    backgroundAudio.loop = true;
    rootEntity.addComponent(backgroundAudio);
    backgroundAudio.play();

    carSystem.setRootEntity(rootEntity);

    const cat = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cat";
    })!;

    const targetCircle = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Circle";
    })!;
    targetCircle.setActive(false);

    if (!experience.currentMode.isAR) {
      cameraSystem.initialize(rootEntity, experience.currentMode.camera, cat);
      experience.addSystem(cameraSystem);
    }

    catSystem.initialize(rootEntity, cat, rootEntity.getComponent(ModelAnimatorComponent)!, targetCircle);
    catTargetPosition.copy(cat.transform.localPosition);
    spawnPoint.copy(cat!.transform.localPosition);

    rootEntity
      .findDescendantInHierarchy((entity) => {
        return entity.name == "GroundPlane";
      })!
      .addComponent(
        new OnTapComponent((worldPosition) => {
          if (wonGame) {
            return;
          }
          let tapLocalPosition = rootEntity.transform.worldToLocalPoint(worldPosition.clone());
          //clamp play area
          tapLocalPosition.z = Math.max(-5.3, Math.min(tapLocalPosition.z, 5.1));
          tapLocalPosition.x = Math.max(-7.4, Math.min(tapLocalPosition.x, 7.5));
          catTargetPosition.copy(tapLocalPosition);

          targetCircle.transform.localPosition.copy(tapLocalPosition);
          targetCircle.setActive(true);
        })
      );
  },
  () => {
    inGame = false;
    if (!experience.currentMode.isAR) {
      experience.removeSystem(cameraSystem);
    }
  }
);
