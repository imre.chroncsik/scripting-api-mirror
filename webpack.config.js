const path = require("path");
const fs = require("fs");
const webpack = require("webpack");
const glob = require("glob");
const { merge } = require("webpack-merge");
const os = require("os");
const ios = require("./lib/ios/webpack/ios-webpack-tools");

module.exports = (env, args) => {
  const SUPPORTED_TARGET_PLATFORMS = ["android", "ios", "web"];

  const platform = env.SCRIPTING_TARGET;
  if (platform === undefined) {
    throw new Error("'SCRIPTING_TARGET' environment variable is not specified");
  }
  if (!SUPPORTED_TARGET_PLATFORMS.includes(platform)) {
    throw new Error(`'${platform}' is not one of supported platforms: ${SUPPORTED_TARGET_PLATFORMS.join(", ")}`);
  }

  const configs = [
    {
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: [
              {
                loader: "ts-loader",
                options: {
                  configFile: path.resolve(__dirname, `webpack.tsconfig.${platform}.json`),
                  compilerOptions: {
                    ...(platform === "android" && {
                      target: "es5",
                    }),
                  },
                },
              },
            ],
          },
          {
            test: /\.js$/,
            enforce: "pre",
            use: ["source-map-loader"],
          },
        ],
      },
      resolve: {
        roots: [__dirname],
        modules: [path.resolve(__dirname, "node_modules"), "node_modules"],
        extensions: [".tsx", ".ts", ".js"],
        alias: {
          arsdk$: path.resolve(__dirname, `lib/${platform}/arsdk-${platform}.ts`),
          "arsdk/tween$": path.resolve(__dirname, "lib/tween.ts"),
        },
      },
      resolveLoader: {
        roots: [__dirname],
        modules: [path.resolve(__dirname, "node_modules"), "node_modules"],
      },
      mode: args.mode || "production",
      devtool: "inline-source-map",
    },
  ];

  const platformImport = {
    android: [
      "core-js/es/promise",
      "core-js/es/map",
      "core-js/es/set",
      "core-js/es/weak-set",
      "core-js/es/weak-map",
      "core-js/es/global-this",
      "core-js/es/symbol",
    ],
    ios: [],
    web: [],
  };

  //  build list of scripts with input and output paths.
  const immersiveScriptsPath = path.resolve(__dirname, "immersive-scripts");
  let inputFilePath = env.SCRIPTING_INPUT;
  let scripts = [];
  if (inputFilePath !== undefined) {
    inputFilePath = path.resolve(path.resolve(), inputFilePath);
    const relativeOutputFilePath = `${platform}-bundle.js`;
    scripts.push({ inputFilePath, relativeOutputFilePath });
  } else {
    glob.sync(path.join(immersiveScriptsPath, "**/index.ts")).map((inputFilePath) => {
      const dirPath = path.dirname(inputFilePath);
      const relPath = path.relative(immersiveScriptsPath, dirPath);
      const relativeOutputFilePath = path.join(relPath, `${platform}-bundle.js`);
      scripts.push({ inputFilePath, relativeOutputFilePath });
    });
  }

  if (platform === "ios") {
    //  for ios, wrap entry points into dynamic import() calls.
    const entryWrapperFilePaths = ios.createEntryWrappers({
      entryFilePaths: scripts.map((entry) => entry.inputFilePath),
      wrappersFolderPath: fs.mkdtempSync(path.join(os.tmpdir(), "scripting-api-")),
      arsdkRootFolder: path.resolve(__dirname),
    });
    for (let i = 0; i < scripts.length; ++i) scripts[i].inputFilePath = entryWrapperFilePaths[i];
  }

  let outputFolderPath = env.SCRIPTING_OUTPUT || (inputFilePath ? path.dirname(inputFilePath) : immersiveScriptsPath);

  //  turn the list of scripts into webpack entry points.
  configs.push({
    entry: scripts.reduce((entries, script) => {
      return {
        ...entries,
        [script.inputFilePath]: {
          import: [...platformImport[platform], script.inputFilePath],
          filename: script.relativeOutputFilePath,
        },
      };
    }, {}),
    output: {
      path: path.resolve(__dirname, outputFolderPath),
    },
  });

  const watchMode = !!env.WEBPACK_WATCH;
  if (watchMode) {
    configs.push({
      devServer: {
        https: true,
        port: env.HTTP_PORT || 9000,
        contentBase: outputFolderPath,
        host: "0.0.0.0",
      },
    });
  }

  if (args.mode == "development") {
    configs.push({
      optimization: {
        minimize: false,
      },
      devtool: "source-map",
    });
  }

  switch (platform) {
    case "android":
      break;
    case "ios":
      break;
    case "web":
      break;
  }

  const config = merge(configs);
  return config;
};
