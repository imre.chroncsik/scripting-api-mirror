import * as arsdk from "arsdk";
import * as tween from "arsdk/tween";
import { ObjectId, SoundId } from "./enums";
import {
  Entity,
  MeshRenderableComponent,
  ModelAnimatorComponent,
  Component,
  Animation,
  experience,
  OnTapComponent,
  Vector3,
  OrbitCameraControllerComponent,
} from "arsdk";

let currentQuestionIndex = -1;
let questions = new Array<Question>();
let gltfRoot: Entity | null = null;
let winEntity: Entity | null = null;
let failMessageEntity: Entity | null = null;
let tooSlowMessageEntity: Entity | null = null;
let progressBarEntity: Entity | null = null;
let timerAnimation: Animation | null = null;

let hasLostGame = false;
let hasWonGame = false;

class Question extends Component {
  constructor() {
    super();
  }

  show() {
    this.entity!!.setActive(true);
  }

  hide() {
    this.entity!!.setActive(false);
  }
}

class QuizSystem extends arsdk.System {
  totalTime = 0;

  update(dt: number): void {
    this.totalTime += dt;

    if (timerAnimation) {
      if (!timerAnimation.isPlaying() && !hasLostGame && !hasWonGame) {
        console.log("lost based on time");
        lostGame(true);
      }
    }
  }
}

let quizSystem = new QuizSystem();
experience.addSystem(quizSystem);

function lostGame(timeout: Boolean) {
  timerAnimation?.pause();
  if (timeout) {
    tooSlowMessageEntity?.setActive(true);
  } else {
    failMessageEntity?.setActive(true);
  }

  hasLostGame = true;

  //lunge at player
  if (!experience.currentMode.isAR) {
    let camera = experience.currentMode.camera;

    let demogorgon = gltfRoot!!.findDescendantInHierarchy((entity) => {
      return entity.name == "Demogorgon";
    });
    if (demogorgon) {
      camera
        .getComponent(OrbitCameraControllerComponent)!!
        .animateTo(new Vector3(0, 3.5, 0).add(demogorgon.transform.worldPosition), camera.transform.worldPosition, 5);

      let previousRot = new arsdk.Quaternion().copy(demogorgon.transform.localRotation);
      demogorgon.transform.lookAt(camera.transform.worldPosition);
      let targetRot = demogorgon.transform.localRotation.clone();
      demogorgon.transform.localRotation.copy(previousRot);

      let targetDistanceToCAmera = 0.3;
      let previousPos = demogorgon.transform.worldPosition.clone();
      let targetPos = camera.transform.worldPosition
        .clone()
        .add(
          camera.transform.worldPosition
            .clone()
            .sub(new Vector3(0, 2.7, 0).add(demogorgon.transform.worldPosition))
            .normalize()
            .multiplyScalar(-targetDistanceToCAmera)
        )
        .sub(new Vector3(0, 2.7, 0));

      demogorgon.addComponent(
        new tween.TweenComponent(
          0,
          1,
          3,
          (entity, a) => {
            entity.transform.localRotation.slerpQuaternions(previousRot, targetRot, a);
            entity.transform.worldPosition.lerpVectors(previousPos, targetPos, a);
            // entity.transform.lookAt(camera.entity.transform.worldPosition)
          },
          undefined,
          () => {
            camera.removeComponent(camera.getComponent(OrbitCameraControllerComponent)!);
          }
        )
      );
    }
  }
}
function wonGame() {
  winEntity?.setActive(true);
  timerAnimation?.pause();
  hasWonGame = true;
}

function showNextQuestion() {
  if (currentQuestionIndex > questions.length) {
    //already won
    return;
  }
  if (currentQuestionIndex >= 0) {
    let lastQuestion = questions[currentQuestionIndex];
    lastQuestion.hide();
  }

  currentQuestionIndex++;

  if (currentQuestionIndex >= questions.length) {
    wonGame();
    return;
  }
  let nextQuestion = questions[currentQuestionIndex];
  nextQuestion.show();

  timerAnimation?.play();
}

experience.addImmersiveObjectLifecycleListeners(
  ObjectId.Object1,
  (rootEntity) => {
    gltfRoot = rootEntity;
    timerAnimation = gltfRoot!!.getComponent(ModelAnimatorComponent)!!.getAnimations()[0];
    if (!experience.currentMode.isAR) {
      let camera = experience.currentMode.camera;
      winEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "winmessage";
      });
      if (winEntity == null) {
        console.log("error: could not find winmessage entity");
      } else {
        winEntity.setActive(false);
      }
      failMessageEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "failmessage";
      });
      if (failMessageEntity == null) {
        console.log("error: could not find failmessage entity");
      } else {
        failMessageEntity.setActive(false);
      }

      tooSlowMessageEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "tooslowmessage";
      });
      if (tooSlowMessageEntity == null) {
        console.log("error: could not find tooslowmessage entity");
      } else {
        tooSlowMessageEntity.setActive(false);
      }

      progressBarEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "progressbar";
      });
      if (progressBarEntity == null) {
        console.log("error: could not find tooslowmessage entity");
      }

      let questionsEntity = rootEntity.findDescendantInHierarchy((entity) => {
        return entity.name == "questions";
      });

      if (questionsEntity == null) {
        console.log("error: could not find questions entity");
        return;
      }

      questionsEntity.forEachChild((question) => {
        let correctAnswerEntity = question.findChild((entity) => {
          return entity.name.search("correct") == 0;
        });

        if (correctAnswerEntity == null) {
          console.log("could not find correct answer");
          return;
        }

        correctAnswerEntity.addComponent(
          new OnTapComponent(() => {
            if (!hasWonGame && !hasLostGame) {
              correctAnswerEntity!!.getComponent(MeshRenderableComponent)!!.materials[1].color.set(0, 1, 0);
              showNextQuestion();
            }
          })
        );

        question.forEachChild((incorrectAnswer) => {
          if (incorrectAnswer.name.search("incorrect") == 0) {
            incorrectAnswer.addComponent(
              new OnTapComponent(() => {
                if (!hasWonGame && !hasLostGame) {
                  console.log("materials " + incorrectAnswer!!.getComponent(MeshRenderableComponent)!!.materials.length);
                  incorrectAnswer!!.getComponent(MeshRenderableComponent)!!.materials[1].color.set(1, 0, 0);
                  lostGame(false);
                }
              })
            );
          }
        });

        let questionComponent = new Question();

        question.addComponent(questionComponent);
        questions.push(questionComponent);
      });

      console.log("found " + questions.length + " questions");
      questions.forEach((q) => {
        q.hide();
      });

      showNextQuestion();
    }
  },
  () => {
    currentQuestionIndex = -1;
    questions.length = 0;
    gltfRoot = null;
    winEntity = null;
    progressBarEntity = null;
    tooSlowMessageEntity = null;
    failMessageEntity = null;
    timerAnimation = null;
    hasLostGame = false;
    hasWonGame = false;
  }
);
