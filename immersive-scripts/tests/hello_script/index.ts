import { Component, Entity, experience, MeshRenderableComponent, TransformComponent, Vector3 } from "arsdk";
import { CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

console.log("Hello from script ", experience.uid);
console.log("Platform ", experience.platform);
console.log("PlatformVersion ", experience.platformVersion);

class CustomComponent extends Component {
  test = 5;
}

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    const ball = rootEntity.getComponent(TransformComponent);

    let t = new TransformComponent();
    console.log("ball" + ball);

    rootEntity.addComponent(new CustomComponent());
    let renderable = rootEntity.getComponent(MeshRenderableComponent);
    let entity = new Entity();
  },
  () => {
    console.log("Object destroyed");
  }
);
