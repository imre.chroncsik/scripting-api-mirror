import { AudioClip, AudioSourceComponent, Entity, experience, OnTapComponent, System } from "arsdk";
import { assert, assertEqual, assertThrows, assertFloatAlmostEqual } from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

const ICQ_SOUND_ID = "2fd468f4-8e6a-4757-af1d-d07d5b9e1e57";
const FAITH_SOUND_ID = "eeeb5063-1339-4123-8326-2a94e75fab23";
const ICQ_DURATION = 1.019;
const FAITH_DURATION = 3.563999891281128;

//this could be generated by composer to make available AudioClips typed. Then the user does not have to call getAudioClip
class AudioClips {
  static readonly ICQ: AudioClip = experience.getAudioClip(ICQ_SOUND_ID);
  static readonly Faith: AudioClip = experience.getAudioClip(FAITH_SOUND_ID);
}

assertEqual(AudioClips.Faith.name, FAITH_SOUND_ID, "AudioClip name incorrect");
assertEqual(AudioClips.ICQ.name, ICQ_SOUND_ID, "AudioClip name incorrect");

const tests = new Map<string, Boolean>();
const totalNumberOfTests = 8;
function testComplete(key: string) {
  if (tests.get(key)) {
    return; //already completed
  }
  tests.set(key, true);
  let completedCount = 0;
  tests.forEach((element) => {
    if (element) {
      completedCount++;
    }
  });
  console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
  if (completedCount == totalNumberOfTests) {
    console.log("All tests succeeded! :)");
  }
}

function testPlayWithoutClip(rootEntity: Entity) {
  let audioSource2 = new AudioSourceComponent();
  rootEntity.addComponent(audioSource2);
  assertThrows(() => {
    audioSource2.play();
  }, "No clip set");
  rootEntity.removeComponent(audioSource2);
}

//cube scene
experience.addImmersiveObjectLifecycleListeners(CUBE_SCENE_OBJECT_UID, (rootEntity) => {
  let audiosource = new AudioSourceComponent(AudioClips.Faith);
  //attach to camera. AudioSources attached to the camera are never attenuated by distance.
  experience.currentMode.camera.addComponent(audiosource);
  rootEntity.addComponent(
    new OnTapComponent(() => {
      if (audiosource.isPlaying()) {
        audiosource.stop();
        testComplete("stop");
      } else {
        audiosource.play();
      }
    }, true)
  );
});

class TestSystem extends System {
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    if (!this.entity) {
      return;
    }
    let audiosource = this.entity!.getComponent(AudioSourceComponent)!;
    // assert(audiosource.getCurrentProgress() >= 0 && audiosource.getCurrentProgress() <= 1);
    assert(audiosource.time >= 0 && audiosource.time <= FAITH_DURATION);
    assertFloatAlmostEqual(audiosource.clip!!.duration, FAITH_DURATION);

    // if (audiosource.getCurrentProgress() > 0.9) {
    //   testComplete("getCurrentProgress");
    // }
    if (audiosource.time > 0.9 * FAITH_DURATION) {
      testComplete("getCurrentTime");
    }
  }
}

const testSystem = new TestSystem();
experience.addSystem(testSystem);

//duck 1
experience.addImmersiveObjectLifecycleListeners("085c661a-040f-4385-974d-90dfab565906", (rootEntity) => {
  testPlayWithoutClip(rootEntity);
  assertThrows(() => {
    let audioComponent = new AudioSourceComponent();
    rootEntity.addComponent(audioComponent);
    audioComponent.play();
  }, "No clip set");
  let audiosource = new AudioSourceComponent();
  assertEqual(audiosource.volume, 1);
  assertEqual(audiosource.loop, false);
  assertEqual(audiosource.isPlaying(), false);
  audiosource.clip = AudioClips.ICQ;

  assertThrows(() => {
    new AudioSourceComponent().play();
  }, "Not attached to entity");
  assertFloatAlmostEqual(audiosource.clip.duration, ICQ_DURATION, "ICQ duration incorrect");
  // assertEqual(audiosource.getCurrentProgress(), 0);
  assertEqual(audiosource.time, 0);

  rootEntity.addComponent(audiosource);
  audiosource.play();

  rootEntity.getChild(0)!.addComponent(
    new OnTapComponent(() => {
      audiosource.clip = AudioClips.Faith;
      audiosource.play();
      assertEqual(audiosource.isPlaying(), true);
      rootEntity.removeComponent(audiosource);
      assertEqual(audiosource.isPlaying(), false, "Removing component should stop audio");
      rootEntity.addComponent(audiosource);
      assertEqual(audiosource.isPlaying(), false, "Audio should not automatically resume when attached");
      testComplete("change clip, remove component");
    }, true)
  );
  testComplete("test 1");

  //TODO attach to camera
});

//duck 2
experience.addImmersiveObjectLifecycleListeners(
  "292b7f18-8873-4132-ae66-90539e31260c",
  (rootEntity) => {
    testSystem.setEntity(rootEntity);
    let audioSource = new AudioSourceComponent();
    audioSource.clip = AudioClips.Faith;
    rootEntity.addComponent(audioSource);

    let wasPaused = false;
    rootEntity.addComponent(
      new OnTapComponent(() => {
        if (audioSource.isPlaying()) {
          audioSource.pause();
          audioSource.loop = true;
          wasPaused = true;
          testComplete("looping sound");
        } else {
          if (audioSource.volume == 1.0) {
            audioSource.volume = 0.3;
            assertEqual(audioSource.volume, 0.3);
            testComplete("volume 0.3");
          } else {
            audioSource.volume = 1.0;
            assertEqual(audioSource.volume, 1.0);
            testComplete("volume 1.0");
          }
          audioSource.play();
          if (wasPaused) testComplete("resume sound");
        }
      }, true)
    );
  },
  () => {
    testSystem.setEntity(null);
  }
);
