import {
  BoxColliderComponent,
  CameraComponent,
  Component,
  Entity,
  experience,
  MeshRenderableComponent,
  ModelAnimatorComponent,
  OnTapComponent,
  OrbitCameraControllerComponent,
  TransformComponent,
  Vector3,
} from "arsdk";
import { assertEqual, assertNotNull, assertNull, assertThrows, assertNotStrictEqual, assertStrictEqual, assertVectorsEqual } from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

class CustomComponent extends Component {
  test: number = 0;
}

let testableComponents = [TransformComponent, MeshRenderableComponent, ModelAnimatorComponent, OnTapComponent, BoxColliderComponent, CameraComponent];

experience.addImmersiveObjectLifecycleListeners(CUBE_SCENE_OBJECT_UID, (rootEntity) => {
  assertEqual(rootEntity.getComponents(TransformComponent).length, 1, "Entity must have one Transform");
  assertNull(rootEntity.removeComponent(rootEntity.getComponent(TransformComponent)!), "Removing the Transform component is not allowed");
  assertNull(rootEntity.addComponent(new TransformComponent()), "Adding two Transform components is not allowed");
  assertStrictEqual(rootEntity.transform, rootEntity.getComponent(TransformComponent));
  assertNull(rootEntity.getComponent(BoxColliderComponent), "The root entity has a collider");
  assertNull(rootEntity.getComponent(BoxColliderComponent), "The gltf node has a collider but it should not have one");

  const animator = rootEntity.getComponent(ModelAnimatorComponent);
  assertNotNull(animator, "Object root should have object animator");
  assertNull(rootEntity.removeComponent(animator!), "ModelAnimator component can not be removed");

  assertNotNull(rootEntity.removeComponent(rootEntity.addComponent(new OnTapComponent(() => {}, true))!), "OnTap component should be able to be removed");

  const sphere = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Sphere";
  })!;
  const collider = sphere.getComponent(BoxColliderComponent);
  assertNotNull(collider, "sphere should have BoxCollider");

  assertNotNull(sphere.removeComponent(collider!), "BoxCollider component should be able to be removed");

  const meshRenderable = sphere.getComponent(MeshRenderableComponent);
  assertNotNull(meshRenderable, "Sphere should have a MeshRenderable");
  assertNull(sphere.removeComponent(meshRenderable!), "MeshRenderable component should not be able to be removed");

  let cameraEntity: Entity | null = null;
  cameraEntity = experience.currentMode.camera;
  const cameraComponent = cameraEntity.getComponent(CameraComponent);
  assertNotNull(cameraComponent, "cameraEntity should have a Camera component");
  assertNull(cameraEntity.removeComponent(cameraComponent!), "Camera component should not be able to be removed");

  assertThrows(() => {
    rootEntity.getComponent(TransformComponent)!.parent = new TransformComponent();
  }, "Can not set parent to a transform that is not assigned to an entity");

  //check if "entity" access is possible for each component
  testableComponents.forEach((componentClass) => {
    experience.forEachComponent(componentClass, (component) => {
      assertNotNull(component.entity, "component.entity is null");
    });
  });

  let customComponent = new CustomComponent();
  rootEntity.addComponent(customComponent);
  assertNotNull(rootEntity.getComponent(CustomComponent), "Custom component was not added");

  assertNull(rootEntity.getChild(0)!.addComponent(customComponent), "Component is already attached to an entity, attaching it to another entity should fail");
  assertEqual(customComponent.entity, rootEntity);

  //two components of the same type can be added to an entity if the component allows it
  assertNotNull(rootEntity.addComponent(new CustomComponent()));
  const customComponents = rootEntity.getComponents(CustomComponent);
  assertEqual(customComponents.length, 2, "Two components of the same type can be added to an entity if the component allows it");
  assertNotStrictEqual(customComponents[0], customComponents[1]);
  assertNotStrictEqual(
    rootEntity.getComponents(CustomComponent),
    rootEntity.getComponents(CustomComponent),
    "getComponents should return a copy, not a reference"
  );
  assertNotNull(rootEntity.removeComponent(customComponents[0]));
  assertNull(customComponents[0].entity);
  assertEqual(rootEntity.getComponents(CustomComponent).length, 1, "Invalid amounts of components");
  assertStrictEqual(rootEntity.getComponent(CustomComponent), customComponents[1]);
  assertNotNull(rootEntity.removeComponent(customComponents[1]));
  assertNull(rootEntity.getComponent(CustomComponent));

  //check creating a new entity from script
  let entity = new Entity();
  assertEqual(entity.getComponents(TransformComponent).length, 1, "Entity must have one Transform");
  assertEqual(entity.getComponents(MeshRenderableComponent).length, 0, "Entity should not have MeshRenderable Component");
  assertEqual(entity.getComponents(OnTapComponent).length, 0, "Entity should not have MeshRenderable Component");
  assertEqual(entity.getComponents(ModelAnimatorComponent).length, 0, "Entity should not have ModelAnimator Component");
  assertEqual(entity.name, "Entity");

  //TODO what happens if a base class is passed to getComponent?
  entity.getComponents(Component);

  let entityTransform = entity.transform;
  let customComponent2 = new CustomComponent();
  entity.addComponent(customComponent2);
  //entity.destroy() will remove all components from the entity
  entity.destroy();
  assertNull(customComponent2.entity, "Entity was destroyed, so the customComponent2 should have been removed");
  //components can still be accessed after the entity was destroyed
  entityTransform.localPosition.x = 5;
  customComponent2.test = 1;
  assertEqual(customComponent2.test, 1);
  assertVectorsEqual(entityTransform.localPosition, new Vector3(5, 0, 0));

  assertNotNull(rootEntity.addComponent(customComponent2), "customComponent2 should be able to be attached to another Entity");

  let entity2 = new Entity();
  let sphereRenderable = sphere.getComponent(MeshRenderableComponent)!;
  sphere.destroy();

  assertEqual(sphereRenderable?.materials.length, 1);
  assertNull(sphereRenderable.entity, "Entity was destroyed, so the sphereRenderable component should have been removed");

  assertNull(entity2.addComponent(sphereRenderable), "MeshRenderable should not be able to be added");

  let userDefinedOrbitCameraController = new OrbitCameraControllerComponent();
  assertNull(userDefinedOrbitCameraController.entity);
  assertNotNull(entity2.addComponent(userDefinedOrbitCameraController), "OrbitCameraController should not be able to be added");

  //manual instantiation of system components.
  let userDefinedMeshRenderable = new MeshRenderableComponent();
  assertNull(userDefinedMeshRenderable.entity);

  let userDefinedModelAnimator = new ModelAnimatorComponent();
  assertNull(userDefinedModelAnimator.entity);
  let userDefinedCamera = new CameraComponent();
  assertNull(userDefinedModelAnimator.entity);
  //adding system components should fail
  assertNull(entity2.addComponent(userDefinedMeshRenderable), "MeshRenderable should not be able to be added");
  assertNull(entity2.addComponent(userDefinedCamera), "Camera should not be able to be added");
  assertNull(entity2.addComponent(userDefinedModelAnimator), "ModelAnimator should not be able to be added");

  //accessing user-created system components should fail
  assertThrows(() => {
    let mat = userDefinedMeshRenderable!.material;
  }, "User-created system components should throw exceptions when accessed");

  assertThrows(() => {
    let anims = userDefinedModelAnimator.getAnimations();
  }, "User-created system components should throw exceptions when accessed");

  console.log("All tests succeeded! :)");
});
