
TODO this is outdated, should update

design goals for how the ios-specific parts of the repo are set up.

*   support development of immersive scripts in full IDEs, eg. VSCode.
    *   module resolution should work outside of webpack too.
*   prefer being as close as possible to a "standard" js project.
    *   => easier integration with dev tools,
        eg. ide, non-webpack bundler, nodejs (when remote debugging), etc.
*   use ES modules.
    like, _really_ use ES modules, as opposed to saying so in the tsconfig,
    and then hacking around it in the webpack setup.
*   minimize reliance on webpack.
*   support entry wrapping as required by the ios implementation.
    *   the main immersive script must not start immediately on loading the bundle,
        but needs to wait for the app's "start" signal.
        implemented by dynamic import()-ing the main module.
*   use cases to support:
    *   bundle input script for on-device execution,
        input script is specified using `--env SCRIPTING_INPUT` on the webpack cmdline.
    *   bundle all test / sample scripts in immersive-scripts.
    *   script development with prebuilt arsdk.
        *   immersive script in the repo root.
        *   arsdk prebuilt and installed to <repo>/node_modules/arsdk.
            no changes in arsdk are expected.
        *   tsc should be able to build the immersive script.
            *   => tsc module resolution needs to find arsdk under node_modules.
    *   script development with on-demand recompiled arsdk.
        *   repo contains sources for both the sdk (<repo>/arsdk)
            and the immersive script (<repo>/experience).
        *   there's also a <repo>/arsdk-dev folder, with project/build related files
            (eg. tsconfig) that we only use in this setup, but do not want to
            commit into the main scripting-api repo.
        *   a master tsconfig in the repo root references
            exp/tsconfig and arsdk-dev/tsconfig.
            this makes sure that arsdk sources are also watched and recompiled on change.
        *   arsdk build results are produced to exp/node_modules/arsdk.
            => module resolution is the same as in the prebuilt-arsdk case.
        *   note at the moment a manual copy step
            arsdk/types/arsdk/index.d.ts -> exp/node_modules/arsdk/index.d.ts
            is necessary, should be automated later.
