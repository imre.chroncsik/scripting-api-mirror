
/*
    This is where we implement the cross-platform API
    (as defined in arsdk/index.d.ts)
    on top of what arsdk-ios-raw provides
    (in other words, on top of what the iOS SDK exposes).

        --- Implementation Notes ---

        Subclass constructors and arsdk-ios-raw superclass constructors

    Someone needs to handle object replication. Ideally only one of either the superclass
    or the subclass, though theoretically it isn't an issue if they try to both handle it.

    In most cases if we have a superclass in arsdk-ios-raw,
    and a subclass of it in arsdk-ios,
    then the subclass still uses the same native-side class
    (eg. `Entity extends raw.ECSEntity` => Entity still replicates as ECSEntity on the native side).
    This means that we can leave replication to the base class.
    To do so, we simply need to pass along the replicationInfo parameter to the super constructor.

    If a subclass wanted to replicate as a native-side class other than the one used
    by the superclass, then the subclass would need to call super(arDoNotReplicateConstructorTag),
    and handle replication itself (by calling replicateNewObject()).

    Component constructors are a special case, see below.

        Component constructors

    iOS native-side ECSComponent and its subclasses require an owner Entity
    as a mandatory constructor parameter (at least now; may change later).
    On the other hand, on the script side we want to allow the creation of Components
    with no owner Entity, eg. to enable the `entity.addComponent(new MyComponent())` pattern.
    We resolve this interface incompatibility as follows:
    *   The script-side constructor takes an optional NewObjectReplicationInfo parameter.
    *   If replication info is provided (probably happens when an object was created
        on the native side, now being replicated to script), then just passes it along
        to the superclass constructor.
    *   If replication info is not provided, then the creation of the new Component instance
        was initiated by script.
        We don't yet have an owner Entity, so we can't yet replicate the new Component to native.
        Instead, we call the super-constructor passing `arDoNotReplicate`.
        This prevents the super-constructor from trying to call the native-side constructor
        (which would fail without passing an owner Entity).
        Instead, we handle replication later, when setEntity() gets called.
    *   A consequence of the above is that some functions in Component classes may not work
        until the Component is added to an Entity.
        This is because before adding the Component to an Entity, the Component doesn't have a
        native-side counterpart, and some functions (those implemented on the native side)
        can't work without that.
*/

import { NewObjectReplicationInfo, classesByName, ryot_nullify, arDoNotReplicateConstructorTag } from "./arsdk-ios-tools"
import * as raw from "./arsdk-ios-raw"
import { Vector3, arsdk_createReplicatedVector } from "./vector3-ios"
export { Vector3 }
import { Quaternion, arsdk_createReplicatedQuaternion } from "./quaternion-ios"
export { Quaternion }
import { Color, arsdk_createReplicatedColor } from "./color-ios"
export { Color }

//  https://git.vzbuilders.com/immersive/scripting-api/blob/master/types/arsdk/index.d.ts



// function replicate(object: any, ...params: any[]) {
//     let cls = object.constructor
//     if (!isComponentClass(cls))
//         throw "!isComponentClass: " + cls
//     let nativeClassName = raw.nativeClassNameForReplicatedClass(cls)
//     if (nativeClassName === undefined)
//         throw `nativeClassNameForReplicatedClass(${cls}) === undefined`
//     raw.replicateNewObject(object, nativeClassName, ...params)
// }

function notNull<T>(value: T | null): value is T { return value !== null }



export type OnModeChangeListener = (newCurrentMode: ARMode | PreviewMode) => void

export class ARExperience extends raw.ECSExperience {
  constructor(replicationInfo?: NewObjectReplicationInfo) {
    super(replicationInfo)
    this.onModeChangeListeners = []

    this.internalCurrentMode =
      (this.raw_getComponent(raw.ECSPreviewMode) === null)
        ? new ARMode()
        : new PreviewMode()

    this.startObservingObjectPreviewModeStateChanged()
  }

  get uid(): string { return this.experienceDefinitionUid }
  get platform(): string { return "iOS" }
  get platformVersion(): string { return this.arsdkVersion }

  addImmersiveObjectLifecycleListeners(
    objectUid: string,
    onObjectInstantiated: (root: Entity) => void,
    onObjectDestroyed?: (root: Entity) => void) {

    const didCreateObject =
      (newECSEntity: raw.ECSEntity) => {
        if (!(newECSEntity instanceof Entity))
          throw "!(newECSEntity instanceof Entity)"
        let newEntity = newECSEntity as Entity
        onObjectInstantiated(newEntity)
      }
    const didDestroyObject =
      (destroyedECSEntity: raw.ECSEntity) => {
        if (onObjectDestroyed === undefined)
          return
        if (!(destroyedECSEntity instanceof Entity))
          throw "!(destroyedECSEntity instanceof Entity)"
        let destroyedEntity = destroyedECSEntity as Entity
        onObjectDestroyed(destroyedEntity)
      }
    super.raw_addObjectScript(objectUid, didCreateObject, didDestroyObject)
  }

  get currentMode(): ARMode | PreviewMode { return this.internalCurrentMode }

  addOnModeChangedListener(onModeChanged: (currentMode: ARMode | PreviewMode) => void): void {
    this.onModeChangeListeners.push(onModeChanged)
  }

  addSystem(system: System): void {
    // this.raw_addComponent(system)
    this.raw_addComponent(system.arsdk_onFrameComponent)
  }

  removeSystem(system: System): void {
    // this.raw_removeComponent(system)
    this.raw_removeComponent(system.arsdk_onFrameComponent)
  }

  forEachComponent<T extends Component>(
    classDef: ComponentClassDefinition<T>,
    action: (component: T) => void): void {

    let entityRegistry = this.raw_getComponent(raw.ECSEntityRegistry)
    if (entityRegistry === null)
      return

    let nativeClassName = raw.nativeClassNameForReplicatedClass(classDef)
    if (nativeClassName === undefined)
      throw `nativeClassNameForReplicatedClass(${classDef}) === undefined`
    const ecsComponents: raw.ECSComponent[] =
      entityRegistry.getComponentsByClassName(nativeClassName)
    const maybeComponents: (T | null)[] = ecsComponents.map(ecsComponent => {
      if (!isComponentClass(ecsComponent.constructor))
        return null
      if (!(ecsComponent instanceof classDef))
        return null
      return ecsComponent as T
    })
    const components: T[] = maybeComponents.filter(notNull)
    components.forEach(action)
  }

  startObservingObjectPreviewModeStateChanged() {
    let onMediatorMessageComponent = this.raw_getComponent(raw.ECSOnMediatorMessage)
    if (onMediatorMessageComponent !== null) {
      onMediatorMessageComponent.onObjectPreviewModeStateChangedAction =
        (isNewActive) => this.onObjectPreviewModeStateChanged(isNewActive)
    }
  }

  onObjectPreviewModeStateChanged(isNewActive: boolean) {
    let inPreviewMode = isNewActive
    if (inPreviewMode && this.currentMode instanceof ARMode)
      this.internalCurrentMode = new PreviewMode()
    if (!inPreviewMode && this.currentMode instanceof PreviewMode)
      this.internalCurrentMode = new ARMode()

    for (let onModeChangeListener of this.onModeChangeListeners)
      onModeChangeListener(this.currentMode)
  }

  //  module-internal

  internalCurrentMode: ARMode | PreviewMode
  onModeChangeListeners: OnModeChangeListener[]

  get cameraEntity(): Entity {
    // const cameraECSEntity: raw.ECSEntity | null | undefined =
    //     this.raw_getComponent(raw.ECSEntityGetter, "cameraEntity")?.value
    // if (cameraECSEntity === null)
    //     throw "cameraECSEntity === null"
    // if (cameraECSEntity === undefined)
    //     throw "cameraECSEntity === undefined"
    // if (!(cameraECSEntity instanceof Entity))
    //     throw "!(cameraECSEntity instanceof Entity)"
    // return cameraECSEntity as Entity

    const presentationMode = ryot_nullify(this.raw_getComponent(raw.ECSPresentationMode))
    if (presentationMode === null)
      throw "presentationMode === null"
    const cameraECSEntity = presentationMode.cameraEntity
    if (!(cameraECSEntity instanceof Entity))
      throw "!(cameraECSEntity instanceof Entity)"
    return cameraECSEntity as Entity
  }
}
classesByName["ECSExperience"] = ARExperience

export class ARMode {
  public readonly isAR = true
  public get camera(): Entity {
    if (!experience)
      throw "!experience"
    return experience.cameraEntity
  }
  constructor() {
    this.camera.transform.isReadOnly = true
  }
}

class PreviewMode {
  public readonly isAR = false
  public readonly camera: PreviewCamera = new PreviewCamera()
}

class PreviewCamera {
  public get entity(): Entity { return experience!.cameraEntity }
  public get transform(): TransformComponent { return this.entity.transform }
  public get orbitCameraController(): OrbitCameraControllerComponent | null {
    let orbitCameraControllerComponent = this.entity.getComponent(OrbitCameraControllerComponent)!
    return orbitCameraControllerComponent
  }
}


interface AnyComponentClassDefinition {
  isComponentClass: boolean
  nativeClassName: string
  allowMultipleInstancesPerEntity: boolean
  new(...args: any[]): Component
}
function isComponentClass(value: any): value is AnyComponentClassDefinition {
  return 'isComponentClass' in value
}

interface ComponentClassDefinition<T extends Component>
/*
    it's important that ComponentClassDefinition does _not_ extend AnyComponentClassDefinition.
    i'm not sure why, but doing so seems to confuse the typescript type checker.
    in that case, `let component = entity.getComponent(SomeComponent)`
    would infer the type of `component` as `Component`, not `SomeComponent`.
    (interestingly, if we explicitly type `component`, as in
    `let component: SomeComponent = entity.getComponent(SomeComponent)`, that still works.)
*/
// extends AnyComponentClassDefinition
{

  isComponentClass: boolean
  nativeClassName: string
  allowMultipleInstancesPerEntity: boolean
  new(...args: any[]): T
}


export class Entity extends raw.ECSEntity {
  constructor()
  constructor(replicationInfo: NewObjectReplicationInfo)
  constructor(replicationInfo?: NewObjectReplicationInfo) {
    super(replicationInfo)
    if (replicationInfo === undefined) {
      //  Entity created by script, add default components
      this.addComponent(new TransformComponent())
    }
  }

  get name(): string { return this.raw_name ?? "" }
  set name(newValue: string) { this.raw_name = newValue }
  get activeInHierarchy(): boolean { return this.raw_isActive }

  get transform(): TransformComponent {
    const transform = this.getComponent(TransformComponent)
    return transform!
  }

  get parent(): Entity | null {
    return ryot_nullify(this.transform.parent?.entity)
  }

  set parent(parentEntity: Entity | null) {
    this.transform.parent = ryot_nullify(parentEntity?.transform)
  }

  get children(): ReadonlyArray<Entity> {
    let maybeChildEntities = this.transform.children.map(tr => tr.entity)
    let childEntities: ReadonlyArray<Entity> = maybeChildEntities.filter(notNull)
    return childEntities
  }

  childCount(): number { return this.transform.childCount() }

  getChild(index: number): Entity | null {
    if (index < 0 || index >= this.transform.getChildCount())
      return null
    const childTransform = this.transform.getChild(index)
    return ryot_nullify(childTransform?.entity)
  }

  forEachDescendantInHierarchy(action: (entity: Entity) => void): void {
    action(this)
    const childEntites = this.children
    childEntites.forEach(childEntity => childEntity.forEachDescendantInHierarchy(action))
  }
  findDescendantInHierarchy(predicate: (entity: Entity) => Boolean): Entity | null {
    if (predicate(this))
      return this
    const childEntites = this.children

    if ((globalThis as any)["arsdk_ios_findDescendantChecksChildrenFirst"] === true) {
      //  strictly speaking this isn't proper depth-first search,
      //  but with our usual search patterns this can be faster,
      //  and findDescendantInHierarchy() is horribly slow on iOS.
      for (let childEntity of childEntites) {
        if (predicate(childEntity))
          return childEntity
      }
    }

    for (let childEntity of childEntites) {
      const foundInChildEntity = childEntity.findDescendantInHierarchy(predicate)
      if (foundInChildEntity !== null)
        return foundInChildEntity
    }
    return null
  }

  forEachChild(action: (child: Entity) => void): void {
    const childEntites = this.children
    childEntites.forEach(action)
  }
  findChild(predicate: (child: Entity) => Boolean): Entity | null {
    const childEntites = this.children
    for (const childEntity of childEntites) {
      if (predicate(childEntity))
        return childEntity
    }
    return null
  }

  addComponent<T extends Component>(component: T): T | null {
    if (component.entity !== null)
      return null

    const componentClass = component.constructor
    if (!isComponentClass(componentClass))
      throw "!isComponentClassDefinition: " + componentClass

    if (!componentClass.allowMultipleInstancesPerEntity) {
      const existingComponent = this.getComponent(componentClass)
      if (existingComponent !== null)
        return null
    }

    // component.setEntity(this)
    super.raw_addComponent(component)
    return component
  }

  removeComponent<T extends Component>(component: T): T | null {
    //  TODO read an isRemovable field from the class def instead of hardwiring
    if (component.constructor === TransformComponent)
      return null
    if (component.constructor === MeshRenderableComponent)
      return null
    // if (component.constructor === ModelAnimatorComponent)
    //     return null
    if (component.constructor === BoxColliderComponent)
      return null
    this.raw_removeComponent(component)
    return component
  }

  getComponent<T extends Component>(classDef: ComponentClassDefinition<T>): T | null {
    // if (!isIOSRawComponentClass(classDef))
    //     return null
    return this.raw_getComponent(classDef)
  }

  getComponents<T extends Component>(classDef: ComponentClassDefinition<T>): Array<T> {
    // if (!isIOSRawComponentClass(classDef))
    //     return []
    return this.raw_getComponents(classDef)
  }

  setActive(newIsActive: boolean) {
    //  TODO use getComponent(CameraComponent) instead of raw_getComponent(raw.ECSCameraComponent)
    if (!newIsActive && (this.raw_getComponent(raw.ECSCameraComponent) !== null))
      throw "can't deactivate a camera Entity"
    this.isDisabled = !newIsActive
  }

  destroy() {
    if (this.raw_getComponent(raw.ECSCameraComponent) !== null)
      throw "can't destroy() a camera Entity"
    this.raw_destroy()
  }

  //  private

}
classesByName["ECSEntity"] = Entity
classesByName["ECSObjectInstance"] = Entity

/*
export abstract class System extends raw.ECSOnFrame {
    abstract update(dt: number): void
    constructor() {
        if (experience === null)
            throw "experience === null"
        super(dt => this.update(dt))
        experience.raw_addComponent(this)
    }
}
*/

export abstract class System {
  abstract update(dt: number): void
  constructor() {
    if (experience === null)
      throw "experience === null"
    this.arsdk_onFrameComponent = new raw.ECSOnFrame()
    this.arsdk_onFrameComponent.onFrameAction = (dt: number) => { this.update(dt) }
    // this.arsdk_onFrameComponent.onFrameAction = this.update
  }

  readonly arsdk_onFrameComponent: raw.ECSOnFrame
}

export abstract class EntityUpdaterSystem extends System {
  update(dt: number) {
    for (let entity of this.entities) {
      if (!entity.activeInHierarchy)
        continue
      this.updateEntity(entity, dt)
    }
  }

  abstract updateEntity(entity: Entity, dt: number): void

  addEntity(entity: Entity) { this.entities.push(entity) }

  private entities: Entity[] = []
}

export class Component extends raw.ECSComponent {
  static nonInheritableNativeClassName = "ECSComponent"

  static allowMultipleInstancesPerEntity = true

  // constructor()
  // constructor(replicationInfo: NewObjectReplicationInfo)
  // constructor(replicationInfo?: NewObjectReplicationInfo) {
  //     //  see comments at the top ("Component constructors")
  //     super(replicationInfo ?? arDoNotReplicateConstructorTag)
  // }

  get entity(): Entity | null {
    // if (this.objectId === "arsdk_uninitialized_objectId")
    //     return null
    // return this.getEntity() as Entity
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }

  // /* fileprivate */ setEntity(entity: Entity): void {
  //     if (this.entity !== null)
  //         throw "this.entity !== null"
  //     replicate(this, entity)
  // }
}
classesByName["ECSComponent"] = Component

export class TransformComponent extends raw.ECSTransform implements Component {
  readonly worldPosition: Vector3
  readonly worldRotation: Quaternion
  readonly worldScale: Vector3
  readonly localPosition: Vector3
  readonly localRotation: Quaternion
  readonly localScale: Vector3

  constructor(replicationInfo?: NewObjectReplicationInfo) {
    super(replicationInfo)

    this.worldPosition = arsdk_createReplicatedVector(
      () => this.raw_worldPosition,
      v => this.raw_worldPosition = v)
    this.worldRotation = arsdk_createReplicatedQuaternion(
      () => this.raw_worldOrientation,
      q => this.raw_worldOrientation = q)
    this.worldScale = arsdk_createReplicatedVector(
      () => this.raw_worldScale,
      v => this.raw_worldScale = v)

    this.localPosition = arsdk_createReplicatedVector(
      () => this.raw_localPosition,
      v => this.raw_localPosition = v)
    this.localRotation = arsdk_createReplicatedQuaternion(
      () => this.raw_localOrientation,
      q => this.raw_localOrientation = q)
    this.localScale = arsdk_createReplicatedVector(
      () => this.raw_localScale,
      v => this.raw_localScale = v)
  }

  get parent(): TransformComponent | null {
    let rawParentTransform = this.raw_parent
    if (rawParentTransform === null)
      return null
    return rawParentTransform as TransformComponent
  }

  set parent(newParentTransform: TransformComponent | null) {
    if (newParentTransform !== null && newParentTransform.entity === null)
      throw "newParentTransform.entity === null"
    if (this.entity?.raw_getComponent(raw.ECSCameraComponent) !== null)
      throw "can't set parent of a camera entity"
    this.raw_parent = newParentTransform
  }

  // constructor()
  // constructor(replicationInfo: NewObjectReplicationInfo)
  // constructor(replicationInfo?: NewObjectReplicationInfo) {
  //     super(replicationInfo ?? arDoNotReplicateConstructorTag)
  // }

  //  TODO impl array references, make iterator functions work with array refs

  forEachDescendantInHierarchy(action: (transform: TransformComponent) => void): void {
    action(this)
    const childTransforms = this.children
    childTransforms.forEach(childTransform => childTransform.forEachDescendantInHierarchy(action))
  }

  findDescendantInHierarchy(predicate: (transform: TransformComponent) => Boolean): TransformComponent | null {
    if (predicate(this))
      return this
    const childTransforms = this.children
    for (const childTransform of childTransforms) {
      const foundInChild = childTransform.findDescendantInHierarchy(predicate)
      if (foundInChild !== null)
        return foundInChild
    }
    return null
  }

  forEachChild(action: (child: TransformComponent) => void): void {
    const childTransforms = this.children
    childTransforms.forEach(action)
  }
  findChild(predicate: (child: TransformComponent) => Boolean): TransformComponent | null {
    const childTransforms = this.children
    for (const childTransform of childTransforms) {
      if (predicate(childTransform))
        return childTransform
    }
    return null
  }

  get children(): ReadonlyArray<TransformComponent> {
    return this.raw_children.map(ch => ch as TransformComponent)
  }

  //  ---

  //  TODO instead of re-implementing entity() and setEntity() in each Component subclass, use mixins.

  static allowMultipleInstancesPerEntity = false

  get entity(): Entity | null {
    // if (this.objectId === "arsdk_uninitialized_objectId")
    //     return null
    // return this.getEntity() as Entity
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }

  // /* fileprivate */ setEntity(entity: Entity): void {
  //     if (this.entity !== null)
  //         throw "this.entity !== null"
  //     replicate(this, entity)
  // }

  private _isReadOnly: boolean = false
  get isReadOnly(): boolean { return this._isReadOnly }
  set isReadOnly(newisReadOnly: boolean) {
    this._isReadOnly = newisReadOnly
    this.worldPosition.isReadOnly = newisReadOnly
    this.worldRotation.isReadOnly = newisReadOnly
    this.worldScale.isReadOnly = newisReadOnly
    this.localPosition.isReadOnly = newisReadOnly
    this.localRotation.isReadOnly = newisReadOnly
    this.localScale.isReadOnly = newisReadOnly
  }

  getChild(index: number): TransformComponent | null {
    const child = super.getChild(index)
    if (child === null)
      return null
    if (!(child instanceof TransformComponent))
      return null
    return child as TransformComponent
  }
  childCount(): number { return super.getChildCount() }

  lookAt(target: Vector3): void;
  lookAt(target: Vector3, worldUp: Vector3): void;
  lookAt(target: Vector3, worldUp?: Vector3): void {
    this.raw_lookAt(target.asArray(), worldUp?.asArray())
  }


  localToWorldPoint(point: Vector3): Vector3 { return Vector3.fromArray(this.worldPointFromLocalPoint(point.asArray())) }
  worldToLocalPoint(point: Vector3): Vector3 { return Vector3.fromArray(this.localPointFromWorldPoint(point.asArray())) }
  localToWorldDirection(direction: Vector3): Vector3 { return Vector3.fromArray(this.worldDirectionFromLocalDirection(direction.asArray())) }
  worldToLocalDirection(direction: Vector3): Vector3 { return Vector3.fromArray(this.localDirectionFromWorldDirection(direction.asArray())) }
}
classesByName["ECSTransform"] = TransformComponent

export namespace OnTapComponent {
  export type OnTapCallback = (hitPointWorld: Vector3) => void
}
export class OnTapComponent extends raw.ECSOnTap implements Component {
  constructor(onTapCallback: OnTapComponent.OnTapCallback)
  constructor(
    onTapCallbackOrReplicationInfo: OnTapComponent.OnTapCallback | NewObjectReplicationInfo,
    triggerOnChildColliders?: boolean) {

    if (onTapCallbackOrReplicationInfo instanceof NewObjectReplicationInfo)
      throw "OnTapComponent must not be replicated native-to-script"
    let onTap = onTapCallbackOrReplicationInfo
    super(
      (worldPosition: number[]) => {
        const hitPointWorld = Vector3.fromArray(worldPosition)
        if (hitPointWorld === null) return
        onTap(hitPointWorld)
      })
    this.handlesTapsOnChildEntities = triggerOnChildColliders ?? true
  }

  //  TODO instead of re-implementing entity() and setEntity() in each Component subclass, use mixins.
  static allowMultipleInstancesPerEntity = true

  get entity(): Entity | null {
    // if (this.objectId === "arsdk_uninitialized_objectId")
    //     return null
    // return this.getEntity() as Entity
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }

  // /* fileprivate */ setEntity(entity: Entity): void {
  //     if (this.entity !== null)
  //         throw "this.entity !== null"
  //     replicate(
  //         this,
  //         entity,
  //         (worldPosition: number[]) => {
  //             const hitPointWorld = Vector3.fromArray(worldPosition)
  //             if (hitPointWorld === null) return
  //             this.onTap(hitPointWorld)
  //         })
  // }

  // private onTap: (hitPointWorld: Vector3) => void
}
classesByName["ECSOnTap"] = OnTapComponent

export class MeshRenderableComponent extends raw.ECSMesh {
  //  castShadows, receiveShadows, material, materials, getPrimitiveCount, setMaterialAt inherited from ECSMesh

  get material(): Material { return super.material as Material }
  get materials(): Material[] { return super.materials as Material[] }
}
classesByName["ECSMesh"] = MeshRenderableComponent

export class Material extends raw.ECSMaterial {
  //  opacity, metalness, roughness, emissive inherited from ECSMaterial
  readonly color: Color

  constructor(replicationInfo?: NewObjectReplicationInfo) {
    super(replicationInfo)

    this.color = arsdk_createReplicatedColor(() => this.raw_color, c => this.raw_color = c)
  }

  clone(): Material {
    let copy = new Material()
    copy.color.copy(this.color)
    copy.opacity = this.opacity
    copy.metalness = this.metalness
    copy.roughness = this.roughness
    copy.emissive = this.emissive
    return copy
  }

  setFade(opacity: number) {
    //  TODO useFadeBlendingMode()
    this.opacity = opacity
  }
}
classesByName["ECSMaterial"] = Material

export class OrbitCameraControllerComponent extends raw.ECSOrbitCameraController implements Component {
  static allowMultipleInstancesPerEntity = false

  readonly pivotPosition_: Vector3
  get pivotPosition(): Vector3 {
    this.checkEntity()
    return this.pivotPosition_
  }

  constructor(replicationInfo?: NewObjectReplicationInfo) {
    super(replicationInfo)

    this.pivotPosition_ = arsdk_createReplicatedVector(
      () => this.raw_pivotPosition,
      v => this.raw_pivotPosition = v)
  }

  get entity(): Entity | null {
    // if (this.objectId === "arsdk_uninitialized_objectId")
    //     return null
    // return this.getEntity() as Entity
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }

  // /* fileprivate */ setEntity(entity: Entity): void {
  //     if (this.entity !== null)
  //         throw "this.entity !== null"
  //     replicate(this, entity)
  // }

  animateTo(pivotPosition: Vector3, cameraPosition: Vector3, duration: number) {
    this.checkEntity()
    this.raw_animateTo(pivotPosition.asArray(), cameraPosition.asArray(), duration)
  }

  stopAnimating() {
    this.checkEntity()
    this.raw_stopAnimating()
  }

  private checkEntity() {
    if (this.entity === null)
      throw "OrbitCameraControllerComponent.entity === null"
  }
}
classesByName[raw.ECSOrbitCameraController.nativeClassName] = OrbitCameraControllerComponent

export class ModelAnimatorComponent extends raw.ECSObjectAnimator implements Component {
  get entity(): Entity | null {
    // if (this.objectId === "arsdk_uninitialized_objectId")
    //     return null
    // return this.getEntity() as Entity
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }

  // /* fileprivate */ setEntity(entity: Entity): void {
  //     if (this.entity !== null)
  //         throw "this.entity !== null"
  //     replicate(this, entity)
  // }

  getAnimation(animationName: string): Animation {
    let rawAnimation = this.getOrCreateAnimationInstanceNamed(animationName)
    if (rawAnimation === null)
      throw "animation not found by name: " + animationName
    return rawAnimation as Animation
  }

  getAnimations(): ReadonlyArray<Animation> {
    let rawAnimations = this.getOrCreateAnimationInstancesForAllTemplates()
    let animations = rawAnimations.map(rawAnimation => { return rawAnimation as Animation })
    return animations
  }
}
classesByName[raw.ECSObjectAnimator.nativeClassName] = ModelAnimatorComponent

export class Animation extends raw.ECSObjectAnimationInstance {
  isRunning(): boolean { return this.raw_isRunning }
  getDuration(): number { return this.duration }
  getCurrentProgress(): number { return this.progress }
  start(): this { this.raw_start(); return this }
  pause(): this { this.raw_pause(); return this }
  resume(): this { this.raw_resume(); return this }
}
classesByName[raw.ECSObjectAnimationInstance.nativeClassName] = Animation

export class AudioSourceComponent extends raw.ECSAudioSource implements Component {
  get entity(): Entity | null {
    // if (this.objectId === "arsdk_uninitialized_objectId")
    //     return null
    // return this.getEntity() as Entity
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }

  // /* fileprivate */ setEntity(entity: Entity): void {
  //     if (this.entity !== null)
  //         throw "this.entity !== null"
  //     if (experience === null)
  //         throw "experience === null"
  //     replicate(this, entity, experience)
  // }

  // constructor()
  // constructor(replicationInfo: NewObjectReplicationInfo)
  // constructor(replicationInfo?: NewObjectReplicationInfo) {
  //     super(replicationInfo ?? arDoNotReplicateConstructorTag)
  // }
  constructor(replicationInfo?: NewObjectReplicationInfo) {
    if (replicationInfo === undefined) {
      if (experience === null)
        throw "experience === null"
      super(experience)
    } else
      super(replicationInfo)
  }

  get volume(): number { return this.firstClip?.volume ?? 1 }
  set volume(newValue: number) { if (this.firstClip !== null) this.firstClip.volume = newValue }
  get loop(): boolean { return this.firstClip?.loops ?? false }
  set loop(newValue: boolean) { if (this.firstClip !== null) this.firstClip.loops = newValue }

  setClip(clip: string): void {
    this.firstClip?.stop()
    this.getOrCreateAudioClipInstanceNamed(clip)
  }
  play(): void {
    this.checkFirstClip()
    //  TODO should call resume() instead of start() if paused
    this.firstClip?.start()
  }
  pause(): void { this.checkFirstClip(); this.firstClip?.pause() }
  stop(): void { this.checkFirstClip(); this.firstClip?.stop() }

  isPlaying(): boolean { return this.firstClip?.isRunning ?? false }
  getDuration(): number { this.checkFirstClip(); return this.firstClip?.duration ?? 0 }
  getCurrentTime(): number { return this.getCurrentProgress() * this.getDuration() }
  getCurrentProgress(): number { return this.firstClip?.progress ?? 0 }

  private checkFirstClip() {
    if (this.firstClip === null)
      throw "AudioSourceComponent.play(): no clip"
  }
}
classesByName["ECSAudioSource"] = AudioSourceComponent



export abstract class ColliderComponent extends Component { }
export class BoxColliderComponent extends ColliderComponent { }

export class CameraComponent extends raw.ECSCameraComponent implements Component {
  get entity(): Entity | null {
    let ecsEntity = super.entity
    if (ecsEntity === null)
      return null
    return ecsEntity as Entity
  }
}
classesByName[CameraComponent.nativeClassName] = CameraComponent

export let experience: ARExperience | null = null

//  TODO remove
// export let legacyExperience: raw.ScriptExperience

export function setGlobal(name: string, value: any) {
  if (name === "experience")
    experience = value
}
