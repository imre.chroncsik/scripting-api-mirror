//@ts-ignore
nativeExperience.Component = /**  @class */ (function () {
  function Component() {
    //@ts-ignore
    this.scriptableComponent = new nativeExperience.ScriptableComponent(this);
  }

  Object.defineProperty(Component.prototype, "entity", {
    get() {
      return this.scriptableComponent.entity;
    },
  });

  return Component;
})();

//@ts-ignore
nativeExperience.System = /** @class */ (function () {
  function System() {
    //@ts-ignore
    this.scriptableSystem = new nativeExperience.ScriptableSystem(this);
  }

  return System;
})();

// @ts-ignore
const ni = nativeExperience;
export const experience = ni.experience;
export const System = ni.System;
export const Entity = ni.Entity;
export const Component = ni.Component;
export const TransformComponent = ni.TransformComponent;
export const MeshRenderableComponent = ni.MeshRenderableComponent;
export const Material = ni.Material;
export const Texture = ni.Texture;
export const ModelAnimatorComponent = ni.ModelAnimatorComponent;
export const Animation = ni.Animation;
export const Vector3 = ni.Vector3;
export const Quaternion = ni.Quaternion;
export const Color = ni.Color;
export const OnTapComponent = ni.OnTapComponent;
export const ColliderComponent = ni.ColliderComponent;
export const BoxColliderComponent = ni.BoxColliderComponent;
export const AudioSourceComponent = ni.AudioSourceComponent;
export const OrbitCameraControllerComponent = ni.OrbitCameraControllerComponent;
export const CameraComponent = ni.CameraComponent;
export const AudioClip = ni.AudioClip;
