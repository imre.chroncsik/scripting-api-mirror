import {
  BoxColliderComponent,
  Component,
  ComponentClassDefinition,
  Entity,
  experience,
  MeshRenderableComponent,
  ModelAnimatorComponent,
  OnTapComponent,
  Quaternion,
  TransformComponent,
  Vector3,
} from "arsdk";
import {
  assert,
  assertEqual,
  assertNotNull,
  assertNull,
  assertThrows,
  assertVectorsEqual,
  assertQuaternionsEqual,
  assertVectorsAlmostEqual,
  assertEntityIsDestroyed,
  assertEntityNotDestroyed,
  assertQuaternionsAlmostEqual,
} from "../../../lib/assert";
import { CUBE_SCENE_OBJECT_UID, CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_DUCK1_UID, CUBE_SCENE_DUCK2_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

const AR_CAMERA_NAME = "AR Camera";
const PREVIEW_MODE_CAMERA_NAME = "Preview Mode Camera";
const OBJECT_ROOT_NAME = "Immersive Object Root";
const ANCHOR_ENTITY_NAME = "Plane Anchor";
const DEFAULT_ENTITY_NAME = "Entity";

const tests = new Map<string, Boolean>();
const totalNumberOfTests = 12;
function testComplete(key: string) {
  if (tests.get(key)) {
    return; //already completed
  }
  tests.set(key, true);
  let completedCount = 0;
  tests.forEach((element) => {
    if (element) {
      completedCount++;
    }
  });
  console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
  if (completedCount == totalNumberOfTests) {
    console.log("All tests succeeded! :)");
  }
}

function countComponentsInChildren(entity: Entity, component: ComponentClassDefinition<Component>) {
  let totalCount = entity.getComponents(component).length;
  entity.forEachDescendantInHierarchy((child) => {
    totalCount += child.getComponents(component).length;
  });

  return totalCount;
}

function entityCreateAndDestroyTests(rootEntity: Entity) {
  let entity = new Entity();
  assertEqual(entity.name, DEFAULT_ENTITY_NAME);

  assertNull(entity.parent, "The default parent should be null (scene root)");
  entity.transform.setParent(rootEntity.transform, false);
  //check default transforms
  assertVectorsEqual(entity.transform.localPosition, new Vector3(0, 0, 0));
  assertVectorsEqual(entity.transform.localScale, new Vector3(1, 1, 1));
  assertQuaternionsEqual(entity.transform.localRotation, new Quaternion(0, 0, 0, 1).identity());
  assertVectorsAlmostEqual(entity.transform.worldPosition, rootEntity.transform.worldPosition);

  let child = new Entity();
  let child2 = new Entity();
  child.parent = entity;
  child2.parent = entity;
  child2.destroy();
  assertEqual(entity.childCount(), 1, "Entity should be removed from parent when it is destroyed");
  let transform = entity.getComponent(TransformComponent)!;
  entity.destroy();
  assertEntityIsDestroyed(entity);
  assertThrows(() => {
    child.parent = null;
  }, "Child was not destroyed");

  assertNull(transform.entity, "All components should be removed by entity.destroy()");

  //TODO: Should the component still be able to be accessed?
  transform.localPosition.x += 1;

  testComplete("entityCreateAndDestroyTests");
}

function testSetActive(rootEntity: Entity) {
  const cube1 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "BigCube1";
  })!;

  const cube2 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "BigCube2";
  })!;

  const cubes = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cubes";
  })!;

  let active = true;
  cube1.addComponent(
    new OnTapComponent(() => {
      active = !active;
      cube2.setActive(active);
      assertEqual(cube2.activeInHierarchy, active);
      testComplete("testSetActive1");
    })
  );

  let active2 = true;
  cube2.addComponent(
    new OnTapComponent(() => {
      active2 = !active2;
      cubes.setActive(active2);
      assertEqual(cubes.parent!.activeInHierarchy, true, "Parent activeInHierarchy wrong");
      assertEqual(cubes.activeInHierarchy, active2, "Entity activeInHierarchy wrong");
      assertEqual(cubes.getChild(0)!.activeInHierarchy, active2, "Child activeInHierarchy wrong");
      testComplete("testSetActive2");
    })
  );
}

function cyclicGraphDetectionTest() {
  //build cyclic graph
  let e1 = new Entity();
  let e2 = new Entity();
  let e3 = new Entity();

  e1.transform.parent = e2.transform;
  e2.transform.parent = e3.transform;
  assertThrows(() => {
    e3.transform.parent = e1.transform;
  }, "Cyclic hierarchy not detected");
  assertThrows(() => {
    e3.parent = e1;
  }, "Cyclic hierarchy not detected");
  assertThrows(() => {
    e2.transform.parent = e1.transform;
  }, "Cyclic hierarchy not detected");

  e1.destroy();
  e2.destroy();
  e3.destroy();
  testComplete("cyclicGraphDetectionTest");
}

function testReparenting() {
  let parent = new Entity();
  let child = new Entity();
  child.parent = parent;

  let childLocalPos = new Vector3(1, 2, 3);
  let childLocalRot = new Quaternion(1, 2, 3, 4).normalize();
  let childLocalScale = new Vector3(1, 2, 3);

  child.transform.localPosition.copy(childLocalPos);
  child.transform.localScale.copy(childLocalScale);
  child.transform.localRotation.copy(childLocalRot);

  assertVectorsEqual(child.transform.localPosition, childLocalPos, "localPosition incorrect");
  assertVectorsEqual(child.transform.localScale, childLocalScale, "localScale incorrect");
  assertQuaternionsEqual(child.transform.localRotation, childLocalRot), "localRotation incorrect";

  parent.transform.localPosition.set(5, 2, 1);
  assertVectorsAlmostEqual(child.transform.worldPosition, new Vector3().addVectors(parent.transform.localPosition, childLocalPos), "worldPosition incorrect");
  assertVectorsAlmostEqual(child.transform.worldScale, childLocalScale, "worldScale incorrect");
  assertQuaternionsAlmostEqual(child.transform.worldRotation, childLocalRot, "worldRotation incorrect");

  child.parent = null;
  assertVectorsAlmostEqual(
    child.transform.worldPosition,
    new Vector3().addVectors(parent.transform.localPosition, childLocalPos),
    "worldPosition incorrect. .parent should keep the world transform"
  );

  child.transform.setParent(parent.transform);

  assertVectorsAlmostEqual(
    child.transform.worldPosition,
    new Vector3().addVectors(parent.transform.localPosition, childLocalPos),
    "worldPosition incorrect. .setParent with keepWorldTransform==true should keep the world transform"
  );

  parent.transform.localRotation.set(2, 3, 1, 2).normalize();
  parent.transform.localScale.set(3, 2, 1);
  //re-parenting with keepWorldTransform == false does not change the local transform, but it changes the world transform
  child.setParent(null, false);

  assertVectorsAlmostEqual(child.transform.localPosition, childLocalPos, "localPosition incorrect. after keepWorldTransform == false");
  assertVectorsAlmostEqual(child.transform.localScale, childLocalScale, "localScale incorrect");
  assertQuaternionsAlmostEqual(child.transform.localRotation, childLocalRot, "localRotation incorrect");

  assertVectorsAlmostEqual(child.transform.worldPosition, childLocalPos, "worldPosition incorrect");
  assertVectorsAlmostEqual(child.transform.worldScale, childLocalScale, "worldScale incorrect");
  assertQuaternionsAlmostEqual(child.transform.worldRotation, childLocalRot, "worldRotation incorrect");

  child.destroy();
  parent.destroy();
}

function testCameras() {
  if (experience.currentMode.isAR) {
    let arCamera = experience.currentMode.camera;
    assertEqual(arCamera.name, AR_CAMERA_NAME);
  } else {
    let previewCamera = experience.currentMode.camera;
    assertEqual(previewCamera.name, PREVIEW_MODE_CAMERA_NAME); //TODO agree on default node name
  }
}

let sphereEntity: Entity | null = null;
let rootEntity1: Entity | null = null;
let anchorEntity1: Entity | null = null;
let anchorChild: Entity | null = null;
let anchorChildChild: Entity | null = null;

experience.addOnModeChangedListener((currentMode) => {
  aliveDuckRoots.forEach((entity) => {
    assertEntityIsDestroyed(entity, "Entity not destroyed after mode change");
  });
  aliveDuckRoots.clear();
  if (sphereEntity) {
    //after a mode switch all entities must be destroyed
    assertEntityIsDestroyed(sphereEntity, "Entity not destroyed after mode change");
    sphereEntity = null;
    testComplete("Mode change test");
  }
});

experience.addOnResetListener(() => {
  console.log("on reset");
  sphereEntity = null;
  let transformComponents = 0;
  experience.forEachComponent(TransformComponent, (comp) => {
    transformComponents++;
  });
  console.log("TransformComponents " + transformComponents);
});

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    let transformComponents = 0;
    experience.forEachComponent(TransformComponent, (comp) => {
      transformComponents++;
    });
    console.log("TransformComponents " + transformComponents);
    assertNull(rootEntity1, "onObjectDestroyed was not called");

    let anchorEntity = rootEntity.parent;
    assert(anchorEntity!.transform.isReadOnly);
    assertThrows(() => {
      anchorEntity!.transform.localPosition.x += 0.1;
    }, "Transform is readonly");

    assertThrows(() => {
      anchorEntity!.transform.parent = anchorEntity!.parent!.transform;
    }, "Transform is readonly");
    assertEqual(anchorEntity!.name, ANCHOR_ENTITY_NAME);

    anchorEntity1 = anchorEntity;
    if (anchorChild) {
      assertEntityIsDestroyed(anchorChild, "Anchor child should be destroyed in onObjectInstantiated");
      assertEntityIsDestroyed(anchorChildChild!, "anchorChildChild should be destroyed in onObjectInstantiated");
      testComplete("Anchor children destroyed");
    }
    anchorChild = new Entity();
    anchorChild.name = "anchorChild";
    anchorChild.parent = anchorEntity1;
    rootEntity1 = rootEntity;

    assertEqual(countComponentsInChildren(rootEntity, TransformComponent), 290, "Incorrect number of Transform components in hierarchy");
    assertEqual(countComponentsInChildren(rootEntity, MeshRenderableComponent), 283, "Incorrect number of MeshRenderable components in hierarchy");
    assertEqual(countComponentsInChildren(rootEntity, BoxColliderComponent), 283, "Incorrect number of BoxCollider components in hierarchy");
    assertEqual(countComponentsInChildren(rootEntity, ModelAnimatorComponent), 1, "Incorrect number of ModelAnimator components in hierarchy");

    if (!experience.currentMode.isAR) {
      assertVectorsEqual(rootEntity.transform.worldPosition, new Vector3(0, 0, 0), "worldPosition incorrect");
      assertQuaternionsEqual(rootEntity.transform.worldRotation, new Quaternion(0, 0, 0, 1), "worldRotation incorrect");
    }
    assertVectorsAlmostEqual(rootEntity.transform.worldScale, new Vector3(1, 1, 1), "worldScale incorrect");
    assertVectorsEqual(rootEntity.transform.localPosition, new Vector3(0, 0, 0), "localPosition incorrect");
    assertVectorsEqual(rootEntity.transform.localScale, new Vector3(1, 1, 1), "localScale incorrect");
    assertQuaternionsEqual(rootEntity.transform.localRotation, new Quaternion(0, 0, 0, 1), "localRotation incorrect");

    assertEqual(rootEntity.childCount(), 1, "Root entity should have only one child");
    assertNull(rootEntity.getChild(1)), "rootEntity.getChild(1) should return null";
    assertEqual(rootEntity.name, OBJECT_ROOT_NAME);

    //TODO: Composer currently inserts one extra node. Should be removed, see: XR-6098
    let gltfNode = rootEntity.getChild(0)!;

    if (sphereEntity) {
      sphereEntity.destroy();
    }

    sphereEntity = gltfNode.getComponent(TransformComponent)!.getChild(1)!.getChild(4)!.entity!;
    assertEqual(sphereEntity.name, "Sphere");

    //test findChild
    assertEqual(
      sphereEntity.parent!.findChild((child) => {
        return child.name == "Sphere";
      })!.name,
      "Sphere"
    );

    testReparenting();

    //Set parent to scene root. It should still be rendered and visible in the scene.
    //It should still be in the scene if the user switches to the next carousel item.
    sphereEntity.parent = null;

    testSetActive(rootEntity);

    rootEntity.transform.worldPosition.x += 1;

    entityCreateAndDestroyTests(rootEntity);
    cyclicGraphDetectionTest();
    testCameras();

    if (experience.currentMode.isAR) {
      testComplete("Tests for object 1 spawned AR mode");
    } else {
      testComplete("Tests for object 1 spawned 3D mode");
    }
  },
  (rootEntity) => {
    assertEqual(rootEntity, rootEntity1, "onObjectDestroyed should pass the same rootEntity that onObjectInstantiated has passed");
    assertNotNull(rootEntity1);
    assertEntityIsDestroyed(rootEntity1!);
    rootEntity1 = null;
    assertEntityIsDestroyed(anchorEntity1!);

    //The anchor child will be destroyed right after this callback.
    assertEntityNotDestroyed(anchorChild!, "anchor child should not yet be destroyed in onObjectDestroyed");
    anchorChildChild = new Entity();
    anchorChildChild.name = "anchorChildChild";
    anchorChildChild.parent = anchorChild;

    //sphere should still be accessible
    assertNotNull(sphereEntity!.getComponent(MeshRenderableComponent));
    sphereEntity!.getComponent(MeshRenderableComponent)!.material.color.set(1, 0, 0, 1);
    sphereEntity!.transform.worldPosition.set(1, 1, 1);
    sphereEntity!.transform.worldScale.set(0.5, 0.5, 0.5);
    if (experience.currentMode.isAR) {
      testComplete("Tests for object 1 destroyed AR mode");
    } else {
      testComplete("Tests for object 1 destroyed 3D mode");
    }
  }
);

let destroyCalled = false;
let rootEntityDuck1: Entity | null = null;
let anchorDuck1: Entity | null = null;
let childEntityDuck1: Entity | null = null;
let anchorChildDuck1: Entity | null = null;
experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_DUCK1_UID,
  (rootEntity) => {
    rootEntityDuck1 = rootEntity;
    anchorDuck1 = rootEntity.parent;
    let childEntity = new Entity();
    childEntity.name = "childEntityDuck1";
    childEntityDuck1 = childEntity;
    anchorChildDuck1 = new Entity();
    anchorChildDuck1.name = "anchorChild";
    anchorChildDuck1.parent = anchorDuck1;
    childEntity.parent = rootEntity;
    destroyCalled = false;
    //manually destroy the anchor entity
    rootEntity.parent!.destroy();
    assert(destroyCalled, "onDestroyed should have been called");
    testComplete("Anchor destroyed");
  },
  (rootEntity) => {
    assertEqual(rootEntity, rootEntityDuck1, "onObjectDestroyed should pass the same rootEntity that onObjectInstantiated has passed");
    //this should be called when the root object entity is destroyed. Destroying the anchor will also destroy all its children.
    assertEntityIsDestroyed(rootEntityDuck1!, "Root entity was not destroyed");
    assertEntityIsDestroyed(anchorDuck1!, "Anchor entity was not destroyed");
    assertEntityIsDestroyed(childEntityDuck1!, "Child entity was not destroyed. onObjectDestroyed should be called after the entire subgraph was destroyed");

    assertEntityNotDestroyed(anchorChildDuck1!, "anchor child should not be destroyed here if .destroy is called directly on the root Entity");

    anchorChildDuck1!.destroy();
    anchorChildDuck1 = null;
    rootEntityDuck1 = null;
    anchorDuck1 = null;
    childEntityDuck1 = null;
    destroyCalled = true;
  }
);

let aliveDuckRoots = new Set<Entity>();
let createdDucksCount = 0;

//add a new Duck, whenever the carousel item is selected.
//destroy each Duck whenever it is tapped.
experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_DUCK2_UID,
  (rootEntity) => {
    rootEntity.name = "Duck root " + createdDucksCount++;
    rootEntity.parent = null;
    rootEntity.transform.worldPosition.x = createdDucksCount * 1;
    aliveDuckRoots.add(rootEntity);
    console.log("Created ducks, ducks alive: " + aliveDuckRoots.size);

    rootEntity.addComponent(
      new OnTapComponent(() => {
        rootEntity.destroy();
        testComplete("Destroyed duck2");
      }, true)
    );
  },
  (rootEntity) => {
    aliveDuckRoots.delete(rootEntity);
    console.log("Duck destroyed, ducks alive: " + aliveDuckRoots.size);
  }
);
