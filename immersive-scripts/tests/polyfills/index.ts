import { assert, assertEqual, assertThrows } from "../../../lib/assert";

//Promise
async function hello() {
  return await Promise.resolve("Hello");
}
hello().then(function (value) {
  assertEqual(value, "Hello");
  console.log("All tests succeeded! :)");
});

//Collections
const map = new Map();
map.set("a", "b");
assertEqual(map.get("a"), "b");

const set = new Set();
set.add("a");

const weakSet = new WeakSet<String>();
weakSet.add(new String("b"));
assertThrows(() => {
  weakSet.add("b");
}, "Should throw b is not an object");

const weakMap = new WeakMap<Number>();
weakMap.set(new Number(1), new String("b"));

const array = new Array(1, 2, 3, 4);
assertEqual(
  array.find((v) => {
    return v == 2;
  }),
  2,
  "Error in array.find()"
);
assertEqual(
  array.findIndex((v) => {
    return v == 2;
  }),
  1,
  "Error in array.findIndex()"
);

const obj = {
  prop: 42,
};
Object.freeze(obj);

assert(Object.isFrozen(obj));

//there seems to be a bug in Rhino where this doesn't throw properly when assertThrows is used
var thrown2 = false;
try {
  obj.prop = 31;
} catch (e) {
  thrown2 = true;
}
assertEqual(thrown2, true, "Should not be able to add property to frozen object");
assertThrows(() => {
  Object.freeze(1);
}, "This should throw under ES5");

class T {
  prop? = 42;
  prop2? = 43;
}
let object2 = new T();
delete object2.prop2;
Object.seal(object2);
assert(Object.isSealed(object2));

object2.prop = 33;

//there seems to be a bug in Rhino where this doesn't throw properly when assertThrows is used
let thrown = false;
try {
  delete object2.prop; // cannot delete when sealed
} catch (e) {
  thrown = true;
}

assertEqual(thrown, true, "delete property on sealed object did not thrown");

assertEqual(object2.prop, 33);
assertEqual(object2.prop2, undefined);

//regex
var re = new RegExp("d(b+)d");
assertEqual(re.test("cdbbdbsbz"), true);
var myRe = /d(b+)d/g;
var myArray = myRe.exec("cdbbdbsbz");
assertEqual(myArray![0], "dbbd");
assertEqual(myArray![1], "bb");

new String().toLowerCase();
let d2 = Date.UTC(2312, 12);

assertEqual(parseFloat("12.12"), 12.12);
decodeURI("test");
escape("bla ");
Function().call(this);
let b = new EvalError("bla");

//JSON
const json = '{"result":true, "count":42}';
const parsedObj = JSON.parse(json);
assertEqual(parsedObj.count, 42);

//Symbol
let s = Symbol();

//test globalThis
globalThis.Set;
globalThis.escape("test");

"asdf".match("test");

"test".split("e");
