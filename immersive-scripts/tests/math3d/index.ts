import { Quaternion, Vector3 } from "arsdk";
import {
  assertColorsAlmostEqual,
  assertEqual,
  assertIsNaN,
  assertQuaternionsAlmostEqual,
  assertQuaternionsEqual,
  assertVectorsAlmostEqual,
  assertVectorsEqual,
  assertFloatAlmostEqual,
} from "../../../lib/assert";

assertVectorsEqual(new Vector3(), new Vector3(0, 0, 0), "Incorrect default values for Vector3");
assertVectorsEqual(new Vector3(1), new Vector3(1, 0, 0), "Incorrect default values for Vector3");
assertVectorsEqual(new Vector3(1, 2), new Vector3(1, 2, 0), "Incorrect default values for Vector3");
assertQuaternionsEqual(new Quaternion(), new Quaternion(0, 0, 0, 1), "Incorrect default values for Quaternion");
assertQuaternionsEqual(new Quaternion(1), new Quaternion(1, 0, 0, 1), "Incorrect default values for Quaternion");
assertQuaternionsEqual(new Quaternion(1, 2), new Quaternion(1, 2, 0, 1), "Incorrect default values for Quaternion");
assertQuaternionsEqual(new Quaternion(1, 2, 3), new Quaternion(1, 2, 3, 1), "Incorrect default values for Quaternion");

let v1 = new Vector3(2, Number.NaN);
assertEqual(v1.x, 2);
assertIsNaN(v1.y);
assertEqual(v1.z, 0);

let q2 = new Quaternion(Number.NaN, 2, Number.NaN);
assertIsNaN(q2.x);
assertEqual(q2.y, 2);
assertIsNaN(q2.z);
assertEqual(q2.w, 1, "Incorrect default values for Quaternion");

let q = new Quaternion(1, 2, 3, 4);
//check that quaternion is not normalized automatically
assertEqual(q.x, 1);
assertEqual(q.y, 2);
assertEqual(q.z, 3);
assertEqual(q.w, 4);

assertQuaternionsAlmostEqual(q.normalize(), new Quaternion(0.18257418, 0.36514837, 0.5477226, 0.73029673));
q.set(2, 3, 4, 5);
assertQuaternionsEqual(q, new Quaternion(2, 3, 4, 5), "Quaternion.set error");

q.x = Number.NaN;
q.normalize();
assertIsNaN(q.x);
assertIsNaN(q.y);
assertIsNaN(q.z);
assertIsNaN(q.w);

assertQuaternionsEqual(new Quaternion(5).identity(), new Quaternion(0, 0, 0, 1), "Quaternion.identity");

//check all functions to take degrees not radians
let axisAngle = new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), 90);
assertQuaternionsAlmostEqual(axisAngle, new Quaternion(0.0, 0.70710677, 0.0, 0.70710677));
assertFloatAlmostEqual(new Quaternion().angleTo(axisAngle), 90);
assertVectorsAlmostEqual(new Vector3(1, 0, 0).applyAxisAngle(new Vector3(0, 1, 0), 90), new Vector3(0, 0, -1));

let rotatedQuaternion = new Quaternion().rotateTowards(axisAngle, 45);
assertQuaternionsAlmostEqual(rotatedQuaternion, new Quaternion(0.0, 0.38268343, 0.0, 0.9238795));
assertFloatAlmostEqual(rotatedQuaternion.angleTo(axisAngle), 45);

//check euler angles are applied in correct order
assertQuaternionsAlmostEqual(
  new Quaternion().setEulerAngles(new Vector3(10, 20, 30)),
  new Quaternion(0.1276794, 0.1448781, 0.2685358, 0.9437144),
  "setEulerAngles"
);

const x = 2;
const y = 3;
const z = 4;
const w = 5;

//add
var a = new Vector3(x, y, z);
var b = new Vector3(-x, -y, -z);
a.add(b);
assertEqual(a.x, 0);
assertEqual(a.y, 0);
assertEqual(a.z, 0);
var c = new Vector3().addVectors(b, b);
assertEqual(c.x, -2 * x);
assertEqual(c.y, -2 * y);
assertEqual(c.z, -2 * z);

//addScalar
var a = new Vector3(x, y, z);
a.addScalar(2);
assertEqual(a.x, x + 2);
assertEqual(a.y, y + 2);
assertEqual(a.z, z + 2);

//addScaledVector
var a = new Vector3(x, y, z);
a.addScaledVector(new Vector3(x, y, z), -1);
assertEqual(a.x, 0);
assertEqual(a.y, 0);
assertEqual(a.z, 0);

//addVectors
var a = new Vector3(0, 0, 0);
a.addVectors(new Vector3(x, y, z), new Vector3(x, y, z));
assertEqual(a.x, 2 * x);
assertEqual(a.y, 2 * y);
assertEqual(a.z, 2 * z);

//angleTo
var a = new Vector3(1, 0, 0);
assertEqual(a.angleTo(new Vector3(0, 1, 0)), 90);

//clone
var a = new Vector3(x, y, z);
var b = a.clone();
b.z += 2;
assertEqual(b.x, a.x);
assertEqual(b.y, a.y);
assertEqual(b.z, a.z + 2);
assertEqual(a.z, z);

//copy
var a = new Vector3(x, y, z);
a.copy(new Vector3(1, 2, 42));
assertEqual(a.x, 1);
assertEqual(a.y, 2);
assertEqual(a.z, 42);

//cross
var a = new Vector3(1, 2, 3);
var b = new Vector3(2, 3, 4);
var crossed = new Vector3(-1, 2, -1);
var result = a.cross(b);
assertVectorsAlmostEqual(result, crossed);

//crossVectors
var a = new Vector3(1, 2, 3);
var b = new Vector3(2, 3, 4);
var crossed = new Vector3(-1, 2, -1);
var result = new Vector3().crossVectors(a, b);
assertVectorsAlmostEqual(result, crossed);

//distanceTo
var a = new Vector3(1, 1, 1);
assertFloatAlmostEqual(a.distanceTo(new Vector3()), 1.732050807568877);
assertFloatAlmostEqual(a.distanceToSquared(new Vector3()), 3);

//dot
var a = new Vector3(1, 2, 3);
var b = new Vector3(1, 5, 7);
assertEqual(a.dot(b), 32);

//equals
assertEqual(new Vector3(1, 2, 3).equals(new Vector3(1, 2, 3)), true);
assertEqual(a.equals(a), true);
assertEqual(new Vector3(1, 2, 3).equals(new Vector3(2, 2, 3)), false);

//length
var a = new Vector3(1, 1, 1);
assertFloatAlmostEqual(a.length(), 1.732050807568877);
//lengthSq
assertFloatAlmostEqual(a.lengthSq(), 3);

//lerp
var a = new Vector3(1, 1, 1);
var b = new Vector3(0, 2, 3);
a.lerp(b, 0.5);
assertVectorsAlmostEqual(a, new Vector3(0.5, 1.5, 2));

//lerpVectors
var a = new Vector3(1, 1, 1);
var b = new Vector3(0, 2, 3);
assertVectorsAlmostEqual(new Vector3().lerpVectors(a, b, 0.5), new Vector3(0.5, 1.5, 2));

//max
var a = new Vector3(1, 1, 1);
var b = new Vector3(0, 2, 3);
a.max(b);
assertVectorsAlmostEqual(a, new Vector3(1, 2, 3));

//min
var a = new Vector3(1, 1, 1);
var b = new Vector3(0, 2, 3);
a.min(b);
assertVectorsAlmostEqual(a, new Vector3(0, 1, 1));

//multiply
var a = new Vector3(x, y, z);
var b = new Vector3(0, 2, 3);
a.multiply(b);
assertVectorsAlmostEqual(a, new Vector3(0, y * 2, z * 3));

//multiplyScalar
var a = new Vector3(x, y, z);
a.multiplyScalar(2);
assertVectorsAlmostEqual(a, new Vector3(x * 2, y * 2, z * 2));

//multiplyVectors
var a = new Vector3(x, y, z);
var b = new Vector3(0, 2, 3);
assertVectorsAlmostEqual(new Vector3().multiplyVectors(a, b), new Vector3(0, y * 2, z * 3));

//negate
var a = new Vector3(x, y, z);
a.negate();
assertVectorsAlmostEqual(a, new Vector3(-x, -y, -z));

//normalize
var a = new Vector3(x, y, z);
a.normalize();
assertFloatAlmostEqual(a.length(), 1);

//projectOnPlane
assertVectorsEqual(new Vector3(1, 0, 0).projectOnPlane(new Vector3(1, 0, 0)), new Vector3(0, 0, 0));
assertVectorsEqual(new Vector3(0, 1, 0).projectOnPlane(new Vector3(1, 0, 0)), new Vector3(0, 1, 0));
assertVectorsEqual(new Vector3(0, 0, -1).projectOnPlane(new Vector3(1, 0, 0)), new Vector3(0, 0, -1));
assertVectorsEqual(new Vector3(-1, 0, 0).projectOnPlane(new Vector3(1, 0, 0)), new Vector3(0, 0, 0));

//projectOnVector
assertVectorsEqual(new Vector3(1, 0, 0).projectOnVector(new Vector3(10, 0, 0)), new Vector3(1, 0, 0));
assertVectorsEqual(new Vector3(0, 1, 0).projectOnVector(new Vector3(10, 0, 0)), new Vector3(0, 0, 0));
assertVectorsEqual(new Vector3(0, 0, -1).projectOnVector(new Vector3(10, 0, 0)), new Vector3(0, 0, 0));
assertVectorsEqual(new Vector3(-1, 0, 0).projectOnVector(new Vector3(10, 0, 0)), new Vector3(-1, 0, 0));

//reflect
assertVectorsEqual(new Vector3(0, -1, 0).reflect(new Vector3(0, 1, 0)), new Vector3(0, 1, 0));
assertVectorsEqual(new Vector3(1, -1, 0).reflect(new Vector3(0, 1, 0)), new Vector3(1, 1, 0));
assertVectorsEqual(new Vector3(1, -1, 0).reflect(new Vector3(0, -1, 0)), new Vector3(1, 1, 0));

//set
var a = new Vector3(0, 1, 2);
a.set(x, y, z);
assertVectorsEqual(a, new Vector3(x, y, z));

//setLength
var a = new Vector3(0, 0, 1);
a.setLength(2);
assertVectorsEqual(a, new Vector3(0, 0, 2));
a.set(1, 2, 3);
a.setLength(512);
assertFloatAlmostEqual(a.length(), 512);

//sub
var a = new Vector3(x, y, z);
var b = new Vector3(-x, -y, -z);
a.sub(b);
assertEqual(a.x, 2 * x);
assertEqual(a.y, 2 * y);
assertEqual(a.z, 2 * z);

//subVectors
var c = new Vector3().subVectors(b, b);
assertEqual(c.x, 0);
assertEqual(c.y, 0);
assertEqual(c.z, 0);

// ---- Quaternion -----
//clone
var qa = new Quaternion(x, y, z, w);
var qb = qa.clone();
qb.z += 2;
assertEqual(qb.x, qa.x);
assertEqual(qb.y, qa.y);
assertEqual(qb.z, qa.z + 2);
assertEqual(qa.z, z);
assertEqual(qa.w, w);

var qa = new Quaternion(x, y, z, w);
qa.conjugate();
assertQuaternionsAlmostEqual(qa, new Quaternion(-x, -y, -z, w));

//copy
var qa = new Quaternion(x, y, z, w);
qa.copy(new Quaternion(1, 2, 42, 5));
assertEqual(qa.x, 1);
assertEqual(qa.y, 2);
assertEqual(qa.z, 42);
assertEqual(qa.w, 5);
//equals
qa.equals(new Quaternion(1, 2, 42, 5));

//dot
var qa = new Quaternion(x, y, z, w);
var qb = new Quaternion(5, 6, 7, 8);
assertEqual(qa.dot(qb), qa.x * qb.x + qa.y * qb.y + qa.z * qb.z + qa.w * qb.w);
assertEqual(qa.dot(qa), qa.lengthSq());

//identity
var qa = new Quaternion(x, y, z, w);
qa.identity();
assertQuaternionsEqual(qa, new Quaternion(0, 0, 0, 1));

//invert
var qa = new Quaternion(x, y, z, w);
assertQuaternionsEqual(qa.clone().invert(), qa.clone().conjugate());

//length
var qa = new Quaternion(1, 1, 1, 1);
assertFloatAlmostEqual(qa.length(), 2);

//lengthq
var qa = new Quaternion(1, 1, 1, 1);
assertEqual(qa.lengthSq(), 4);

//multiply
var qa = new Quaternion(1, 2, 3, 4);
var qb = new Quaternion(1, 2, 3, 4);
var expected = new Quaternion(8, 16, 24, 2);
qa.multiply(qb);
assertQuaternionsAlmostEqual(qa, expected);

//multiplyQuaternions
var qa = new Quaternion(1, 2, 3, 4);
var qb = new Quaternion(2, 3, 4, 5);
var expected = new Quaternion(12, 24, 30, 0);
assertQuaternionsAlmostEqual(new Quaternion().multiplyQuaternions(qa, qb), expected);

//premultiply
var qa = new Quaternion(1, 2, 3, 4);
var qb = new Quaternion(2, 3, 4, 5);
var expected = new Quaternion(14, 20, 32, 0);
qa.premultiply(qb);
assertQuaternionsAlmostEqual(qa, expected);

//slerp
var qa = new Quaternion(x, y, z, w).normalize();
var qb = new Quaternion(0.23, 0.712, 0.12, 0.11).normalize();

var qc = qa.clone().slerp(qb, 0);
var qd = qa.clone().slerp(qb, 1);
//Note: Slerp can sometimes slerp to the negated quaternion
//This is not a bug, but can happen if the "shorted path" is the other way around.
//The quaternion and its negated version represent the same rotation.
assertQuaternionsEqual(qc, qa, "Error in slerp");
assertQuaternionsEqual(qd, qb, "Error in slerp");

var D = Math.SQRT1_2;

var qe = new Quaternion(1, 0, 0, 0);
var qf = new Quaternion(0, 0, 1, 0);
var expected = new Quaternion(D, 0, D, 0);
var qresult = qe.clone().slerp(qf, 0.5);
assertQuaternionsAlmostEqual(qresult, expected, "Error in slerp");

var qg = new Quaternion(0, D, 0, D);
var qh = new Quaternion(0, -D, 0, D);
expected = new Quaternion(0, 0, 0, 1);
qresult = qg.clone().slerp(qh, 0.5);
assertQuaternionsAlmostEqual(qresult, expected, "Error in slerp");

//slerpQuaternions
var qe = new Quaternion(1, 0, 0, 0);
var qf = new Quaternion(0, 0, 1, 0);
var expected = new Quaternion(Math.SQRT1_2, 0, Math.SQRT1_2, 0);
var qa = new Quaternion();
qa.slerpQuaternions(qe, qf, 0.5);
assertQuaternionsAlmostEqual(qa, expected, "Error in slerpQuaternions");

//set
var qa = new Quaternion();
qa.set(1, 2, 3, 4);
assertQuaternionsEqual(qa, new Quaternion(1, 2, 3, 4));

//setRotationBetweenVectors
var a = new Vector3(1, 2, 3);
var b = new Vector3(5, 4, 3);
var qa = new Quaternion();
qa.setRotationBetweenVectors(a, b);
assertQuaternionsAlmostEqual(qa, new Quaternion(-0.21982299, 0.43964598, -0.21982299, 0.84265476), "Error in setRotationBetweenVectors");

console.log("All tests completed!");
