import { BoxColliderComponent, experience, OnTapComponent, OrbitCameraControllerComponent, Vector3 } from "arsdk";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/oyooS
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/371a3bab-960e-4338-ad08-fb6b4cbecf67/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/371a3bab-960e-4338-ad08-fb6b4cbecf67/v1/ios/index.json
 * Web: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/371a3bab-960e-4338-ad08-fb6b4cbecf67/v1/web/index.json
 */

class HotSpot {
  cameraPosition: Vector3;
  cameraPivot: Vector3;
  constructor(cameraPosition: Vector3, cameraPivot: Vector3) {
    this.cameraPosition = cameraPosition;
    this.cameraPivot = cameraPivot;
  }
}

let hotspots = new Map<string, HotSpot>();
hotspots.set("Male.000", new HotSpot(new Vector3(-1.7195129, 2.4863563, 3.2902381), new Vector3(-1.41646, 1.1445888, 0.12405467)));
hotspots.set("Male.002", new HotSpot(new Vector3(-1.6812689, 1.7015278, 2.7260118), new Vector3(-0.09604859, 1.5553483, 0.13113365)));
hotspots.set("Female", new HotSpot(new Vector3(-1.0400598, 2.0596352, 2.0269148), new Vector3(1.1257155, 1.1548738, 0.14293206)));
hotspots.set("bb8", new HotSpot(new Vector3(3.2993674, 1.2583094, 1.3781419), new Vector3(2.338517, 0.35661447, 0.0060970783)));
let currentHotspot: string | null = null;

experience.addImmersiveObjectLifecycleListeners("006144b5-c085-4cba-b0b3-1adb9436a8b9", (rootEntity) => {
  if (!experience.currentMode.isAR) {
    let camera = experience.currentMode.camera;

    rootEntity.forEachDescendantInHierarchy((childEntity) => {
      let hotspot = hotspots.get(childEntity.name);
      if (hotspot != null && childEntity.getComponent(BoxColliderComponent) != null) {
        childEntity.addComponent(
          new OnTapComponent((tapWorldPosition) => {
            if (currentHotspot != childEntity.name) {
              camera.getComponent(OrbitCameraControllerComponent)!.animateTo(hotspot!.cameraPivot, hotspot!.cameraPosition, 1);
              currentHotspot = childEntity.name;
            }
          })
        );
      }
    });
  }
});

// //to determine positions
// class TestSystem extends System {
//   update(dt: number): void {
//     if (!experience.currentMode.isAR) {
//       let camera = experience.currentMode.camera;
//       let occ = camera.orbitCameraController;
//       //@ts-ignore
//       console.log("cameraPosition " + occ.cameraPosition);
//       //@ts-ignore
//       console.log("pivotPosition " + occ.pivotPosition);
//     }
//   }
// }
// experience.addSystem(new TestSystem());
