
//  helpers used in both arsdk-ios and arsdk-ios-raw

export class NewObjectReplicationInfo { 
    nativeObjectId?: string
    doNotReplicateToNative?: boolean
    constructor(nativeObjectId?: string, doNotReplicateToNative?: boolean) { 
        this.nativeObjectId = nativeObjectId
        this.doNotReplicateToNative = doNotReplicateToNative
    }
}

export const arDoNotReplicateConstructorTag: NewObjectReplicationInfo = new NewObjectReplicationInfo(undefined, true)

//  TODO rename?
export var classesByName: { [name: string]: any } = []

export function ryot_nullify<T>(value: T | null | undefined): T | null { 
    return (value === undefined) ? null : value
}
