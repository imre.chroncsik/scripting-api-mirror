import { ARMode, AudioSourceComponent, Entity, experience, PreviewMode, System, TransformComponent, Vector3 } from "arsdk";
import { MoveTransformComponent, TweenDirection } from "arsdk/tween";
import { assertEqual, assertEntityIsDestroyed, assertEntityNotDestroyed, assertNotStrictEqual, assert, assertStrictEqual } from "../../../lib/assert";
import { CUBE_SCENE_OBJECT_UID, CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_DUCK1_UID, CUBE_SCENE_DUCK2_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");
const FAITH_SOUND_ID = "eeeb5063-1339-4123-8326-2a94e75fab23";

let lastMode = experience.currentMode;
let cameraEntity: Entity | null = getCameraEntity(experience.currentMode);
let testEntity: Entity | null = null;
let sphere: Entity | null = null;
let destroyCalled = false;

console.log("Test pressing reset button on different carousel items in AR and 3D mode.");

function getCameraEntity(currentMode: ARMode | PreviewMode): Entity {
  return currentMode.camera;
}

class TestSystem extends System {
  totalTime = 0;
  resetPressedTime = 0;
  objectInstaniated = -1;
  lastInstantiatedObjectWhenResetWasPressed = -1;
  checkObjectInstantiated = true;
  update(dt: number): void {
    this.totalTime += dt;

    const delayCheck = 2;
    //Note: This test can fail, if the tester switches carousel item quickly after pressing the reset button
    //or if the asset takes longer than delayCheck to load.
    if (this.checkObjectInstantiated && this.totalTime - this.resetPressedTime > delayCheck && this.lastInstantiatedObjectWhenResetWasPressed != -1) {
      assert(
        this.objectInstaniated == this.lastInstantiatedObjectWhenResetWasPressed,
        "onObjectInstantiated for object index " + this.lastInstantiatedObjectWhenResetWasPressed + " was not called after reset"
      );
      this.checkObjectInstantiated = false;
    }
  }

  resetPressed() {
    this.resetPressedTime = this.totalTime;
    this.lastInstantiatedObjectWhenResetWasPressed = this.objectInstaniated;
    this.objectInstaniated = -1;
    this.checkObjectInstantiated = true;
  }

  onObjectInstantiated(index: number) {
    this.objectInstaniated = index;
  }
}
let testSystem = new TestSystem();
experience.addSystem(testSystem);

experience.addOnModeChangedListener((mode) => {
  lastMode = mode;
});

experience.addOnResetListener(() => {
  assertEntityIsDestroyed(cameraEntity!!, "Camera should have been destroyed with reset");
  assertEntityNotDestroyed(getCameraEntity(experience.currentMode), "Camera should have been re-initialized with reset");
  assertNotStrictEqual(cameraEntity!!, getCameraEntity(experience.currentMode));

  assertStrictEqual(experience.currentMode, lastMode, "The .currentMode object should still be the same after reset");

  if (testEntity) {
    assertEntityIsDestroyed(testEntity, "all entities should have been destroyed with reset");
  }
  if (sphere) {
    assertEntityIsDestroyed(sphere, "all entities should have been destroyed with reset");
  }
  assertEqual(destroyCalled, true, "onObjectDestroy was not called");
  let transformComponents = 0;
  experience.forEachComponent(TransformComponent, (comp) => {
    transformComponents++;
  });
  assertEqual(transformComponents, 1, "Only the camera should exist in the scene after reset");

  testSystem.resetPressed();
});

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    destroyCalled = false;
    testEntity = rootEntity;
    let audioSource = new AudioSourceComponent();
    audioSource.clip = experience.getAudioClip(FAITH_SOUND_ID);
    audioSource.loop = true;
    testEntity.addComponent(audioSource);
    audioSource.play();
    testSystem.onObjectInstantiated(0);

    sphere = testEntity.findDescendantInHierarchy((e) => {
      return e.name == "Sphere";
    })!;
    // let worldPos = sphere.transform.worldPosition.clone();
    sphere.parent = null;
    // sphere.transform.worldPosition.copy(worldPos);

    sphere.addComponent(
      new MoveTransformComponent(sphere.transform.worldPosition.clone(), new Vector3(0, 0, 0), 3, null, undefined, TweenDirection.Alternate, true)
    );
  },
  (rootEntity) => {
    destroyCalled = true;
    console.log("onDestroy");
  }
);

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_DUCK1_UID,
  (rootEntity) => {
    testSystem.onObjectInstantiated(1);
  },
  (rootEntity) => {}
);

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_DUCK2_UID,
  (rootEntity) => {
    testSystem.onObjectInstantiated(2);
  },
  (rootEntity) => {}
);
