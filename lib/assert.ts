import { Color, Entity, experience, OnTapComponent, Quaternion, TransformComponent, Vector3 } from "arsdk";

export function assert(condition: Boolean, message?: string): asserts condition {
  if (!condition) throw new Error("assert failed " + (message ?? ""));
}

export function assertNull(actual: any, message?: string) {
  if (actual != null) throw new Error("assertNull failed: Expected null, actual value: " + actual + " " + (message ?? ""));
}

export function assertNotNull(actual: any, message?: string) {
  if (actual == null) throw new Error("assertNotNull failed: Expected not null: " + (message ?? ""));
}

export function assertEqual(actual: any, expected: any, message?: string) {
  if (actual != expected) throw new Error("assertEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertStrictEqual(actual: any, expected: any, message?: string) {
  if (actual !== expected) throw new Error("assertStrictEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertNotEqual(actual: any, notExpected: any, message?: string) {
  if (actual == notExpected) throw new Error("assertNotEqual failed: Did not expect " + actual + " " + (message ?? ""));
}

export function assertNotStrictEqual(actual: any, notExpected: any, message?: string) {
  if (actual === notExpected)
    throw new Error("assertNotStrictEqual failed: Did not expect " + notExpected + " actual value: " + actual + " " + (message ?? ""));
}

const EPSILON = 0.0001;
export function assertFloatAlmostEqual(actual: number, expected: number, message?: string) {
  if (Math.abs(actual - expected) > EPSILON)
    throw new Error("assertFloatAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertVectorsAlmostEqual(actual: Vector3, expected: Vector3, message?: string) {
  if (!vectorsAlmostEqual(actual, expected))
    throw new Error("assertVectorsAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertVectorsEqual(actual: Vector3, expected: Vector3, message?: string) {
  if (!actual.equals(expected)) throw new Error("assertVectorsEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertQuaternionsAlmostEqual(actual: Quaternion, expected: Quaternion, message?: string) {
  if (!quaternionsAlmostEqual(actual, expected))
    throw new Error("assertQuaternionsAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function assertQuaternionsEqual(actual: Quaternion, expected: Quaternion, message?: string) {
  if (!actual.equals(expected)) throw new Error("assertVectorsEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

export function vectorsAlmostEqual(actual: Vector3, expected: Vector3): Boolean {
  return !(Math.abs(actual.x - expected.x) > EPSILON || Math.abs(actual.y - expected.y) > EPSILON || Math.abs(actual.z - expected.z) > EPSILON);
}
export function quaternionsAlmostEqual(actual: Quaternion, expected: Quaternion): Boolean {
  return !(
    Math.abs(actual.x - expected.x) > EPSILON ||
    Math.abs(actual.y - expected.y) > EPSILON ||
    Math.abs(actual.z - expected.z) > EPSILON ||
    Math.abs(actual.w - expected.w) > EPSILON
  );
}
export function assertIsNaN(actual: any, message?: string) {
  if (!isNaN(actual)) throw new Error("assertIsNaN failed: Expected NaN, actual value: " + actual + " " + (message ?? ""));
}

export function assertColorsEqual(actual: Color, expected: Color, message?: string) {
  if (!colorsEqual(actual, expected)) throw new Error("assertColorsEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}
export function assertColorsAlmostEqual(actual: Color, expected: Color, message?: string) {
  if (!colorsAlmostEqual(actual, expected))
    throw new Error("assertColorsAlmostEqual failed: Expected " + expected + " actual value: " + actual + " " + (message ?? ""));
}

function colorsAlmostEqual(actual: Color, expected: Color): Boolean {
  return !(
    Math.abs(actual.r - expected.r) > EPSILON ||
    Math.abs(actual.g - expected.g) > EPSILON ||
    Math.abs(actual.b - expected.b) > EPSILON ||
    Math.abs(actual.a - expected.a) > EPSILON
  );
}

function colorsEqual(actual: Color, expected: Color): Boolean {
  return actual.r == expected.r && actual.g == expected.g && actual.b == expected.b && actual.a == expected.a;
}

export function assertThrows(fun: () => void, message?: string) {
  try {
    fun();
  } catch {
    return;
  }
  throw new Error("assertThrows failed: Function did not throw error: " + message ?? "");
}

export function assertEntityNotDestroyed(entity: Entity, message?: string) {
  try {
    let name = entity.name;
    entity.findChild(() => {
      return true;
    });
  } catch (e) {
    if (message) {
      throw "assertEntityNotDestroyed failed: Entity was destroyed. " + message;
    }
    throw e;
  }
}

export function assertEntityIsDestroyed(entity: Entity, message?: string) {
  assertThrows(() => {
    entity.name;
  }, message ?? "Accessing a destroyed Entity should throw an exception (.name)");

  assertThrows(() => {
    entity.name = "test";
  }, "Accessing a destroyed Entity should throw an exception (setName)");

  assertThrows(() => {
    entity.findChild((c) => {
      return true;
    });
  }, "Accessing a destroyed Entity should throw an exception (findChild)");

  assertThrows(() => {
    entity.findDescendantInHierarchy((c) => {
      return true;
    });
  }, "Accessing a destroyed Entity should throw an exception (findDescendantInHierarchy)");

  assertThrows(() => {
    entity.forEachChild((c) => {
      return true;
    });
  }, "Accessing a destroyed Entity should throw an exception (forEachChild)");

  assertThrows(() => {
    entity.forEachDescendantInHierarchy((c) => {
      return true;
    });
  }, "Accessing a destroyed Entity should throw an exception (forEachDescendantInHierarchy)");

  assertThrows(() => {
    entity.getComponent(TransformComponent);
  }, "Accessing a destroyed Entity should throw an exception (getComponent)");

  assertThrows(() => {
    entity.removeComponent(new OnTapComponent(() => {}));
  }, "Accessing a destroyed Entity should throw an exception (removeComponent)");

  assertThrows(() => {
    entity.activeInHierarchy;
  }, "Accessing a destroyed Entity should throw an exception (activeInHierarchy)");

  assertThrows(() => {
    entity.setActive(true);
  }, "Accessing a destroyed Entity should throw an exception (setActive)");

  assertThrows(() => {
    entity.getChild(0);
  }, "Accessing a destroyed Entity should throw an exception (getChild)");

  assertThrows(() => {
    entity.childCount();
  }, "Accessing a destroyed Entity should throw an exception (childCount)");

  assertThrows(() => {
    entity.destroy();
  }, "Accessing a destroyed Entity should throw an exception (destroy)");

  assertThrows(() => {
    entity.transform;
  }, "Accessing a destroyed Entity should throw an exception (.transform)");

  let testEntity = new Entity();
  testEntity.name = "TestEntity";
  assertThrows(() => {
    entity.parent = testEntity;
  }, "Accessing a destroyed Entity should throw an exception (setParent)");

  assertThrows(() => {
    testEntity.parent = entity.parent;
  }, "Accessing a destroyed Entity should throw an exception (getParent)");
  testEntity.destroy();
}
