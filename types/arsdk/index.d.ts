declare module "arsdk" {
  export class ARExperience {
    private constructor();
    readonly uid: string;
    /**
     * The platform this script is executed on. Android, iOS or Web.
     * @deprecated Platform-specific code is highly discouraged. Use only to workaround implementation bugs.
     */
    readonly platform: Platform;

    /**
     * The runtime version of the platform that executes this script. Android, iOS and Web versions are independent from each other.
     * @deprecated Platform-specific code is highly discouraged. Use only to workaround implementation bugs.
     */
    readonly platformVersion: string;

    /**
     * Adds callbacks for when an Immersive Object specified by "objectUid" is instantiated and destroyed.
     * This triggers in both 3D preview mode and in AR mode.
     */
    addImmersiveObjectLifecycleListeners(objectUid: String, onObjectInstantiated: (root: Entity) => void, onObjectDestroyed?: (root: Entity) => void): void;

    readonly currentMode: ARMode | PreviewMode;
    /**
     * Sets a callback for when the experience has changed between AR and 3D preview mode.
     */
    addOnModeChangedListener(onModeChanged: (currentMode: ARMode | PreviewMode) => void): void;

    /**
     * Sets a callback for when the reset button was pressed.
     * Calls back after all Entities in the experience have been destroyed.
     */
    addOnResetListener(onReset: () => void): void;

    addSystem(system: System): void;
    removeSystem(system: System): void;

    /**
     * Returns a reference to an AudioClip.
     * @param name The name/id of the sound.
     */
    getAudioClip(name: string): AudioClip;

    /**
     * Iterates over all {@link Component}s of type {@link classDef} available at the start of calling this function.
     * Components can be added or removed while iterating which would not affect the callbacks.
     */
    forEachComponent<T extends Component>(classDef: ComponentClassDefinition<T>, callback: (component: T) => void): void;
  }

  /**
   * Android, iOS or Web
   * @deprecated Platform-specific code is highly discouraged. Use only to workaround implementation bugs.
   */
  enum Platform {
    Android = "Android",
    iOS = "iOS",
    Web = "Web",
  }

  export class ARMode {
    private constructor();
    public readonly isAR: true;
    /**
     * The AR camera.
     * Its TransformComponent is defined by the user's device in the real world and is thus read-only. Writing to it will throw an exception.
     */
    public readonly camera: Entity;
  }

  export class PreviewMode {
    private constructor();
    public readonly isAR: false;
    /**
     * The camera in 3D Preview Mode. Has an OrbitCameraController component.
     */
    public readonly camera: Entity;
    //public background: Color;
  }

  /**
   * The constructor of a class extending from Component.
   */
  type ComponentClassDefinition<T extends Component> = new (...args: any[]) => T;

  export class Entity {
    name: string;

    /**
     * Gets or sets the parent of the Entity.
     * Changing the parent keeps the world transform of this Entity the same.
     * Shortcut for entity.transform.parent.entity.
     * If the parent is null, this means this entity is attached to the scene root.
     * * @throws An exception when trying to change the parent of a camera entity.
     */
    parent: Entity | null;

    /**
     * Set the parent of the Entity.
     * This method is the same as setting the parent property except that it also lets the Entity keep the local transform rather than the global transform.
     * This means for example, if the Entity was previously next to its parent, setting keepWorldTransform to false will move the Entity to be positioned next to its new parent in the same way.
     * @param parent The new parent of the Entity.
     * @param keepWorldTransform If true, the local transform is modified such that the world position, rotation and scale is kept the same as it was before. The default value is true.
     */
    setParent(parent: Entity | null, keepWorldTransform?: boolean): void;

    /**
     * This is the same as getComponent(TransformComponent), but can be accessed directly as every Entity has exactly one TransformComponent.
     */
    readonly transform: TransformComponent;

    /**
     * Calls back for every descendant of this Entity in the hierarchy. Does not call back for the caller Entity itself.
     */
    forEachDescendantInHierarchy(callback: (entity: Entity) => void): void;

    /**
     * Returns the first Entity in the descendant hierarchy that fullfills the predicate. Does not return the caller Entity.
     */
    findDescendantInHierarchy(predicate: (entity: Entity) => boolean): Entity | null;

    /**
     * Calls back for every direct child of this Entity in the hierarchy.
     * Children can be added or removed while iterating which would not affect the callbacks.
     */
    forEachChild(callback: (child: Entity) => void): void;

    /**
     * Returns the first direct child Entity that fullfills the predicate.
     */
    findChild(predicate: (child: Entity) => boolean): Entity | null;

    getChild(index: number): Entity | null;
    childCount(): number;

    /**
     * Some component types can only be added once to an entity.
     * If a component could not be added, this function returns null.
     */
    addComponent<T extends Component>(component: T): T | null;
    /**
     * Returns the component that was removed or null if the component could not be removed.
     * TransformComponents can not be removed.
     */
    removeComponent<T extends Component>(component: T): T | null;

    /**
     * Returns the {@link Component} of a specific type if such a Component is attached to this Entity.
     * If multiple Components of the same type are attached, then this function returns the first one.
     * Returns null if no such Component is attached.
     */
    getComponent<T extends Component>(classDef: ComponentClassDefinition<T>): T | null;

    /**
     * Returns all components of a specific type of this Entity. This array is returned as a copy.
     */
    getComponents<T extends Component>(classDef: ComponentClassDefinition<T>): Array<T>;

    /**
     * Sets the active state of this Entity.
     * If this Entity is inactive, then all components of this Entity and all components of its child entities will not be updated.
     * For example, the MeshRenderableComponent will not be rendered anymore, the AudioSourceComponent's and ModelAnimatorComponent will be paused.
     * @throws An exception when setActive(false) is called on a camera entity.
     */
    setActive(active: boolean): void;

    /**
     * Whether this Entity is active in the scene, i.e. when it is active itself and all its parents are set to active.
     */
    readonly activeInHierarchy: boolean;

    /**
     * Destroys this Entity. This will destroy all components and all child entities and their components.
     * @throws An exception when called on a camera entity.
     */
    destroy(): void;
  }

  /**
   * Can be subclassed to create custom components.
   */
  export class Component {
    readonly entity: Entity | null;
  }

  /**
   * An entity has always exactly one Transform.
   */
  export class TransformComponent extends Component {
    /**
     * Gets or sets the parent of the Transform.
     * Changing the parent keeps the world transform the same.
     * If the parent is null, this means this transform is attached to the scene root.
     */
    parent: TransformComponent | null;

    /**
     * Set the parent of the Transform.
     * This method is the same as setting the parent property except that it also keeps the local transform rather than the global transform.
     * This means for example, if the Transform was previously next to its parent, setting keepWorldTransform to false will move the Transform to be positioned next to its new parent in the same way.
     * @param parent The new parent of the Transform.
     * @param keepWorldTransform If true, the local transform is modified such that the world position, rotation and scale is kept the same as it was before. The default value is true.
     */
    setParent(parent: TransformComponent | null, keepWorldTransform?: boolean): void;

    /**
     * Calls back for every descendant of this Transform in the hierarchy. Does not call back for the caller Transform itself.
     */
    forEachDescendantInHierarchy(callback: (transform: TransformComponent) => void): void;

    /**
     * Returns the first Transform in the descendant hierarchy that fullfills the predicate. Does not return the caller Transform.
     */
    findDescendantInHierarchy(predicate: (transform: TransformComponent) => boolean): TransformComponent | null;

    /**
     * Calls back for every direct child of this Transform in the hierarchy.
     * Children can be added or removed while iterating which would not affect the callbacks.
     */
    forEachChild(callback: (child: TransformComponent) => void): void;

    /**
     * Returns the first direct child Transform that fullfills the predicate.
     */
    findChild(predicate: (child: TransformComponent) => boolean): TransformComponent | null;

    getChild(index: number): TransformComponent | null;
    childCount(): number;

    //these are returned as references to avoid memory allocations
    readonly localPosition: Vector3;
    readonly localRotation: Quaternion;
    readonly localScale: Vector3;

    readonly worldPosition: Vector3;
    readonly worldRotation: Quaternion;
    readonly worldScale: Vector3;

    /**
     * Rotates the transform to face "target" in world space. Uses world y-axis as the up vector.
     */
    lookAt(target: Vector3): void;

    /**
     * Rotates the transform to face "target" in world space.
     * Then it rotates the transform to point its up direction vector in the direction of the provided worldUp vector.
     */
    lookAt(target: Vector3, worldUp: Vector3): void;

    /**
     *  Converts the point from this object's local space to world space.
     * @param point A point representing a position in this object's local space.
     */
    localToWorldPoint(point: Vector3): Vector3;
    /**
     * Converts the point from world space to this object's local space.
     * @param point A point representing a position in world space.
     */
    worldToLocalPoint(point: Vector3): Vector3;

    /**
     * Converts the direction from this object's local space to world space.
     * @param direction A direction in this object's local space.
     */
    localToWorldDirection(direction: Vector3): Vector3;

    /**
     * Converts the direction from world space to this object's local space.
     * @param direction A direction in world space.
     */
    worldToLocalDirection(direction: Vector3): Vector3;

    /**
     * Returns if this Transform is readonly.
     * If it is read-only then writing any of its fields will throw an exception.
     */
    readonly isReadOnly: boolean;
  }

  /**
   * Component to handle tap interactions. onTapCallback is called whenever the Collider component of the entity is tapped.
   * When "triggerOnChildColliders" is set to true, the OnTapComponent will be triggered whenever any of its child entities' colliders are tapped.
   * OnTapComponents are triggered in the order of how they were added to the Entity.
   * If the parent hierarchy contains OnTapComponents with "triggerOnChildColliders" set to true,
   * then the OnTapComponents will be triggered bottom-up from children to parents.
   */
  export class OnTapComponent extends Component {
    constructor(onTapCallback: (hitPointWorld: Vector3) => void, triggerOnChildColliders?: boolean);
    //blocksTaps: boolean;
  }

  /**
   * A MeshRenderableComponent can contain mutiple "submeshes/primitives", each having their own material.
   * This matches the GLTF concept of meshes with primitives
   * ,see https://github.com/KhronosGroup/glTF-Tutorials/blob/master/gltfTutorial/gltfTutorial_009_Meshes.md
   */
  export class MeshRenderableComponent extends Component {
    /**
     * This component is automatically created when a GLTF object is loaded. Instantiating it manually is invalid.
     */
    constructor();

    castShadows: boolean;
    receiveShadows: boolean;

    /**
     * This is a shortcut for materials[0]
     */
    material: Material;

    setMaterialAt(primitive: number, material: Material): void;

    /**
     * @returns The number materials of this MeshRenderable. One material per primitive.
     */
    readonly materials: ReadonlyArray<Material>;
  }

  /**
   * Materials will have only the uniforms that are defined in the GLTF.
   *
   * Supported extensions:
   * https://docs.google.com/spreadsheets/d/19m8WCLFmcKZdNHQhAXpHvEynB1MeCSqcvbCqWhj-48k/edit#gid=0
   */
  export class Material {
    private constructor();
    clone(): Material;

    /**
     * Every GLTF defined material has a color factor. This is in linear RGB.
     */
    readonly color: Color;
    /**
     * Setting the opacity of an opaque material has no effect. Use "setFade()" if you want to fade a material.
     */
    opacity: number | null;
    metalness: number | null;
    roughness: number | null;

    /**
     * This is in linear RGB. The alpha value is ignored.
     */
    readonly emissive: Color | null;

    //For fading opaque materials, we would want to have an option to change the transparency mode,
    // however ThreeJS currently does not support "Transparent" mode, only "Fade".
    //see https://google.github.io/filament/Materials.html#materialdefinitions/materialblock/blendingandtransparency:blending
    //https://docs.unity3d.com/Manual/StandardShaderMaterialParameterRenderingMode.html
    //blendingMode : BlendingMode //(Opaque, Transparent, Fade, Masked)

    //Fade a material to a specified opacity. This may internally change the rendering mode of the material.
    //TODO this helper function should be removed and replaced with a BlendingMode setting.
    setFade(opacity: number): void;

    /*
      In the future we would probably want to expose access to arbitrary named uniforms.

      - Unused uniforms can be requested as GLTF extras.
      - We will have to align exposed uniform names between platforms.
      For example "baseColorFactor", "baseColorMap", "metallicFactor", "roughnessFactor", "metallicRoughnessMap"

      getFloat(uniform: string): number | null;
      getFloatArray(uniform: string): Array<number> | null;
      getTexture(uniform: string): Texture | null;
      getColor(uniform: string): Color | null

      setFloat(uniform: string, value: number): void;
      setFloatArray(uniform: string, values: Array<number>): void
      setColor(uniform: string, color: Color): void
      setTexture(uniform: string, texture: Texture): void;
      //For video textures
      //setExternalTexture(uniform: string, texture: ExternalTexture): void;
      //This would be useful for debugging
      //getAvailableMaterialParameters():  Array<string>;
      */
  }

  /**
   * Only one ModelAnimatorComponent can be attached to an entity.
   */
  export class ModelAnimatorComponent extends Component {
    /**
     * This component is automatically created when a GLTF object is loaded. Instantiating it manually is invalid.
     */
    constructor();

    /**
     * Returns a reference to an Animation defined on this ModelAnimatorComponent.
     */
    getAnimation(animationName: string): Animation;
    /**
     * Returns a list of all available Animations.
     */
    getAnimations(): ReadonlyArray<Animation>;
  }

  /**
   * Animation of a set of entities.
   * Animations can animate transforms of entities (including skeletal animation) and animate morph weights.
   */
  export class Animation {
    private constructor();
    readonly name: string;
    /**
     * The current time in seconds of this animation.
     * Setting this value apply the animation to the animations targets to the state at the specified time.
     * If the animation was never played, this will return 0.
     * @throws Setting it throws an exception if the ModelAnimatorComponent is not attached to an entity.
     */
    time: number;
    /**
     * Whether the Animation replays after it finishes. When set to false on a playing Animation, it is stopped at the end of the loop.
     * @throws Setting it throws an exception if the ModelAnimatorComponent is not attached to an entity.
     */
    loop: boolean;
    /**
     * If two animations are in the same layer, only one of them can play at a time.
     * Starting one will stop the other. If two animations exist on separate layers, they can play at the same time.
     * @throws Setting it throws an exception if the ModelAnimatorComponent is not attached to an entity.
     */
    layer: number;

    /**
     * Plays the animation, starting from "time".
     * @throws An exception if an animation is already playing on a different layer with any of the same animation targets.
     * @throws An exception if the ModelAnimatorComponent is not attached to an entity.
     */
    play(): void;

    /**
     * Pauses animation.
     */
    pause(): void;

    /**
     * Returns if the animation is currently playing.
     */
    isPlaying(): boolean;
    /**
     * float, in seconds
     */
    readonly duration: number;
  }

  /**
   * A 3-dimensional vector.
   */
  export class Vector3 {
    /**
     *
     * @param x the x value of this vector. Default is 0.
     * @param y the y value of this vector. Default is 0.
     * @param z the z value of this vector. Default is 0.
     */
    constructor(x?: number, y?: number, z?: number);
    x: number;
    y: number;
    z: number;

    /**
     * Adds v to this vector.
     */
    add(v: Vector3): this;

    /**
     * Adds the scalar value s to this vector's x, y and z values.
     */
    addScalar(s: number): this;

    /**
     * Adds the multiple of v and s to this vector.
     */
    addScaledVector(v: Vector3, s: number): this;

    /**
     * Sets this vector to a + b.
     */
    addVectors(a: Vector3, b: Vector3): this;

    /**
     * axis - A normalized Vector3.
     * angle - An angle in degrees.
     * Applies a rotation specified by an axis and an angle to this vector.
     */
    applyAxisAngle(axis: Vector3, angleDegrees: number): this;

    /**
     *  Applies a Quaternion transform to this vector.
     */
    applyQuaternion(quaternion: Quaternion): this;

    /**
     * Returns the angle between this vector and vector v in degrees.
     */
    angleTo(v: Vector3): number;

    /**
     *  Returns a new vector3 with the same x, y and z values as this one.
     */
    clone(): Vector3;

    /***
     *  Copies the values of the passed vector3's x, y and z properties to this vector3.
     */
    copy(v: Vector3): this;

    /**
     *  Sets this vector to cross product of itself and v.
     */
    cross(v: Vector3): this;

    /**
     * Sets this vector to cross product of a and b.
     */
    crossVectors(a: Vector3, b: Vector3): this;

    /**
     * Computes the distance from this vector to v.
     */
    distanceTo(v: Vector3): number;

    /**
     * Computes the squared distance from this vector to v. If you are just comparing the distance with another distance, you should compare the distance squared instead as it is slightly more efficient to calculate.
     */
    distanceToSquared(v: Vector3): number;

    /**
     * Calculate the dot product of this vector and v.
     */
    dot(v: Vector3): number;

    /**
     * Checks for strict equality of this vector and v.
     */
    equals(v: Vector3): boolean;

    /**
     * Computes the Euclidean length (straight-line length) from (0, 0, 0) to (x, y, z).
     */
    length(): number;

    /**
     * Computes the square of the Euclidean length (straight-line length) from (0, 0, 0) to (x, y, z). If you are comparing the lengths of vectors, you should compare the length squared instead as it is slightly more efficient to calculate.
     */
    lengthSq(): number;

    /**
     * Linearly interpolate between this vector and v, where alpha is the percent distance along the line - alpha = 0 will be this vector, and alpha = 1 will be v.
     * @param v   Vector3 to interpolate towards.
     * @param alpha  interpolation factor, typically in the closed interval [0, 1].
     */
    lerp(v: Vector3, alpha: number): this;

    /**
     * Sets this vector to be the vector linearly interpolated between v1 and v2 where alpha is the percent distance along the line connecting the two vectors - alpha = 0 will be v1, and alpha = 1 will be v2.
     *
     * @param v1 the starting Vector3.
     * @param v2 Vector3 to interpolate towards.
     * @param alpha interpolation factor, typically in the closed interval [0, 1].
     */
    lerpVectors(v1: Vector3, v2: Vector3, alpha: number): this;

    /**
     * If this vector's x, y or z value is less than v's x, y or z value, replace that value with the corresponding max value.
     */
    max(v: Vector3): this;

    /**
     * If this vector's x, y or z value is greater than v's x, y or z value, replace that value with the corresponding min value.
     */
    min(v: Vector3): this;

    /**
     * Multiplies this vector by v.
     */
    multiply(v: Vector3): this;

    /**
     * Multiplies this vector by scalar s.
     */
    multiplyScalar(s: number): this;

    /**
     * Sets this vector equal to a * b, component-wise.
     */
    multiplyVectors(a: Vector3, b: Vector3): this;

    /**
     * Inverts this vector - i.e. sets x = -x, y = -y and z = -z.
     */
    negate(): this;

    /**
     * Convert this vector to a unit vector - that is, sets it equal to a vector with the same direction as this one, but length 1.
     */
    normalize(): this;

    /**
     * Projects this vector onto a plane by subtracting this vector projected onto the plane's normal from this vector.
     * @param planeNormal A vector representing a plane normal.
     */
    projectOnPlane(planeNormal: Vector3): this;

    /**
     * Projects this vector onto v.
     */
    projectOnVector(v: Vector3): this;

    /**
     * Reflect this vector off of plane orthogonal to normal. Normal is assumed to have unit length.
     * @param normal the normal to the reflecting plane
     */
    reflect(normal: Vector3): this;

    /**
     * Sets the x, y and z components of this vector.
     */
    set(x: number, y: number, z: number): this;

    /**
     * Set this vector to a vector with the same direction as this one, but length l.
     */
    setLength(l: number): this;

    /**
     * Subtracts v from this vector.
     */
    sub(v: Vector3): this;

    /**
     * Sets this vector to a - b.
     */
    subVectors(a: Vector3, b: Vector3): this;
  }

  /**
   * A quaternion. Used for representing a 3D-rotation.
   */
  export class Quaternion {
    /**
     *
     * @param x the x value of this vector. Default is 0.
     * @param y the y value of this vector. Default is 0.
     * @param z the z value of this vector. Default is 0.
     * @param w the w value of this vector. Default is 1.
     */
    constructor(x?: number, y?: number, z?: number, w?: number);
    x: number;
    y: number;
    z: number;
    w: number;

    /**
     * Returns the angle between this quaternion and quaternion q in degrees.
     */
    angleTo(q: Quaternion): number;

    /**
     * Creates a new Quaternion with identical x, y, z and w properties to this one.
     */
    clone(): Quaternion;

    /**
     * Returns the rotational conjugate of this quaternion. The conjugate of a quaternion represents the same rotation in the opposite direction about the rotational axis.
     */
    conjugate(): this;

    /**
     * Copies the x, y, z and w properties of q into this quaternion.
     */
    copy(q: Quaternion): this;

    /**
     * Compares the x, y, z and w properties of v to the equivalent properties of this quaternion to determine if they represent the same rotation
     * @param v Quaternion that this quaternion will be compared to.
     */
    equals(v: Quaternion): boolean;

    /**
     * Calculates the dot product of quaternions v and this one.
     */
    dot(v: Quaternion): number;

    /**
     * Sets this quaternion to the identity quaternion; that is, to the quaternion that represents "no rotation".
     */
    identity(): this;

    /**
     * Inverts this quaternion - calculates the conjugate. The quaternion is assumed to have unit length.
     */
    invert(): this;

    /**
     * Computes the Euclidean length (straight-line length) of this quaternion, considered as a 4 dimensional vector.
     */
    length(): number;

    /**
     * Computes the squared Euclidean length (straight-line length) of this quaternion, considered as a 4 dimensional vector. This can be useful if you are comparing the lengths of two quaternions, as this is a slightly more efficient calculation than length().
     */
    lengthSq(): number;

    /**
     * Normalizes this quaternion - that is, calculated the quaternion that performs the same rotation as this one, but has length equal to 1.
     */
    normalize(): this;

    /**
     * Multiplies this quaternion by q.
     */
    multiply(q: Quaternion): this;

    /**
     * Sets this quaternion to a x b.
     * Adapted from the method outlined here.
     */
    multiplyQuaternions(a: Quaternion, b: Quaternion): this;

    /**
     * Pre-multiplies this quaternion by q.
     */
    premultiply(q: Quaternion): this;

    /**
     * Rotates this quaternion by a given angular step to the defined quaternion q. The method ensures that the final quaternion will not overshoot q.
     * @param q The target quaternion.
     * @param step The angular step in degrees.
     */
    rotateTowards(q: Quaternion, stepDegrees: number): this;

    /**
     * Handles the spherical linear interpolation between quaternions. t represents the amount of rotation between this quaternion (where t is 0) and qb (where t is 1). This quaternion is set to the result. Also see the static version of the slerp below.
     * @param qb The other quaternion rotation
     * @param t interpolation factor in the closed interval [0, 1].
     */
    slerp(qb: Quaternion, t: number): this;

    /**
     * Performs a spherical linear interpolation between the given quaternions and stores the result in this quaternion.
     * @param t interpolation factor in the closed interval [0, 1].
     */
    slerpQuaternions(qa: Quaternion, qb: Quaternion, t: number): this;

    /**
     * Sets x, y, z, w properties of this quaternion.
     */
    set(x: number, y: number, z: number, w: number): this;

    /**
     * Sets this quaternion from rotation specified by axis and angle.
     * Axis is assumed to be normalized, angle is in degrees.
     */
    setFromAxisAngle(axis: Vector3, angleDegrees: number): this;

    /**
     * Sets this quaternion to the rotation required to rotate direction vector vFrom to direction vector vTo.
     * Adapted from the method here. http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm
     * vFrom and vTo are assumed to be normalized.
     */
    setRotationBetweenVectors(vFrom: Vector3, vTo: Vector3): this;

    /**
     * Sets this quaternion from the rotation specified by Euler angles. Applying the rotations in XYZ order.
     * @param eulerAngles - the angles in degrees.
     */
    setEulerAngles(eulerAngles: Vector3): this;
  }

  /**
   * An RGBA color.
   */
  export class Color {
    /**
     * The red value.
     */
    r: number;
    /**
     * The green value.
     */
    g: number;
    /**
     * The blue value.
     */
    b: number;
    /**
     * The alpha value.
     */
    a: number;
    /**
     *
     * @param red float [0;1]
     * @param green float [0;1]
     * @param blue float [0;1]
     * @param alpha float [0;1], default: 1
     */
    constructor(red: number, green: number, blue: number, alpha?: number);

    /**
     * Creates a new color from a hex value.
     * @param hex Hex color can either be a string in format "#RRGGBB", "#RRGGBBAA", "RRGGBB", "RRGGBBAA".
     * Lower case is also allowed.
     * Make sure you know the input color space and convert to linear RGB if necessary using {@link Color.convertSRGBToLinear}.
     */
    static fromHex(hex: string): Color;

    /**
     * Sets this colors r,g,b,a values from a hex string.
     * @param hex Hex color can either be a string in format "#RRGGBB", "#RRGGBBAA", "RRGGBB", "RRGGBBAA".
     * Lower case is also allowed.
     * Make sure you know the input color space and convert to linear RGB if necessary using {@link Color.convertSRGBToLinear}.
     */
    setHex(hex: string): this;

    /**
     * Creates a new Color with identical r, g, b and a properties to this one.
     */
    clone(): Color;

    /**
     * Copies the r, g, b and a properties of c into this Color.
     */
    copy(c: Color): this;

    /**
     * Converts this color from linear space to sRGB space.
     */
    convertLinearToSRGB(): this;

    /**
     * Converts this color from sRGB space to linear space.
     */
    convertSRGBToLinear(): this;

    /**
     * Sets r, g, b, a properties of this color. If the parameter a is omitted the property remains unchanged.
     */
    set(r: number, g: number, b: number, a?: number): this;
  }

  /**
   * A System receives a callback to its update method once per frame.
   * It typically performs global actions on every Entity that possesses a specific Component or Components that match that Systems query.
   */
  export abstract class System {
    /**
     * Called once per frame.
     * @param dt The time since the last callback to update(). In seconds.
     */
    abstract update(dt: number): void;
  }

  export class AudioClip {
    readonly name: string;
    /**
     * float, in seconds
     */
    readonly duration: number;
  }

  /**
   * An AudioSourceComponent can only play audio, when it is attached to an entity and references an AudioClip.
   */
  export class AudioSourceComponent extends Component {
    /**
     * @param clip The audio file "clip" that should be played.
     */
    constructor(clip?: AudioClip | null);

    /**
     * The current time in seconds.
     */
    readonly time: number;

    /**
     * float [0;1]
     */
    volume: number;

    /**
     * Whether the audio clip replays after it finishes. When set to false on a playing AudioSourceComponent the sound is stopped at the end of the current loop.
     */
    loop: boolean;

    /**
     * Sets or gets the audio file "clip" that should be played.
     * After a new clip is set, play() must be called again.
     */
    clip: AudioClip | null;

    /**
     * Starts or resumes playback. If playback had previously been paused,
     * playback will continue from where it was paused. If playback had
     * been stopped, or never started before, playback will start at the
     * beginning.
     * @throws An exception when the audio source is not attached to an Entity.
     */
    play(): void;

    /**
     * Pauses playback. Call play() to resume.
     */
    pause(): void;

    /**
     * Stops playback after playback has been started or paused.
     */
    stop(): void;

    /**
     * Returns whether the audio clip is currently playing.
     */
    isPlaying(): boolean;
  }

  /**
   * As long as the OrbitCameraControllerComponent is attached to the Camera entity
   * it is overriding the camera rotation at the end of every frame so the camera looks at pivotPosition.
   * OrbitCameraControllerComponent only works on the PreviewCamera.
   */
  export class OrbitCameraControllerComponent extends Component {
    readonly pivotPosition: Vector3;

    /**
     * Convenience function for tweening the camera to the new location. Using an easeInOut interpolator.
     * This animation is automatically stopped, when the experience switches to another object.
     * This animation is overriding any other changes to the camera's transform while it is playing.
     * @param pivotPosition The target pivot position.
     * @param cameraPosition The target camera position.
     * @param duration The duration in seconds.
     */
    animateTo(pivotPosition: Vector3, cameraPosition: Vector3, duration: number): void;
    /**
     * Stops the camera transform animation.
     */
    stopAnimating(): void;
  }

  /**
   * Sealed class.
   */
  export abstract class ColliderComponent extends Component {}
  export class BoxColliderComponent extends ColliderComponent {}

  /**
   * Preview mode camera and AR camera have a CameraComponent. The AR camera has a read-only transform.
   */
  export class CameraComponent extends Component {}

  /**
   * Global object representing the entire AR experience.
   */
  export const experience: ARExperience;
}
