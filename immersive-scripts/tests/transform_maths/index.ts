import { Entity, experience, System, Vector3 } from "arsdk";
import { assert, assertVectorsAlmostEqual } from "../../../lib/assert";
import { CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

//tests for Transform.lookat, localToWorldPoint, worldToLocalPoint, localToWorldDirection, worldToLocalDirection
//see https://www.youtube.com/watch?v=p2kyTka0uXc

class TestSystem extends System {
  entity: Entity | null = null;
  cube: Entity | null = null;
  cube2: Entity | null = null;
  sphere: Entity | null = null;
  cubes: Entity | null = null;
  setEntity(entity: Entity | null) {
    this.entity = entity;

    if (entity) {
      this.cube = entity.findDescendantInHierarchy((entity) => {
        return entity.name == "BigCube1";
      })!;
      this.cube.transform.localScale.set(0.2, 0.5, 2);

      this.cube2 = entity.findDescendantInHierarchy((entity) => {
        return entity.name == "BigCube2";
      })!;
      this.cube2.transform.localScale.set(0.2, 0.5, 2);

      this.sphere = entity.findDescendantInHierarchy((entity) => {
        return entity.name == "Sphere";
      })!;
      entity.findDescendantInHierarchy((entity) => {
        return entity.name == "Plane";
      })!.transform.localPosition.x += 1000;
      this.sphere.transform.localScale.set(0.5, 0.5, 0.5);

      this.cubes = entity.findDescendantInHierarchy((entity) => {
        return entity.name == "Cubes";
      })!;
      this.cubes!.forEachChild((cube) => {
        cube.transform.localScale.set(0.2, 0.5, 2);
      });
    } else {
      this.cube = null;
    }
  }
  time: number = 0;
  update(dt: number): void {
    this.time += dt;

    if (!experience.currentMode.isAR) {
      if (this.cube) {
        this.sphere!.transform.localPosition.set(2 * Math.sin(this.time * 0.4), Math.sin(2 * this.time * 0.4), 2 * Math.cos(this.time * 0.4));
        this.cube.transform.lookAt(this.sphere!.transform.worldPosition, new Vector3(1, 1, 0).normalize());

        this.cube2!.transform.lookAt(this.sphere!.transform.worldPosition);

        this.cubes!.forEachChild((cube) => {
          cube.transform.lookAt(this.sphere!.transform.worldPosition);
        });

        assertVectorsAlmostEqual(
          this.cube.transform.localToWorldDirection(new Vector3(1, 0, 0)),
          new Vector3(1, 0, 0).applyQuaternion(this.cube.transform.worldRotation)
        );
        assertVectorsAlmostEqual(
          this.cube.transform.worldToLocalDirection(new Vector3(1, 0, 0)),
          new Vector3(1, 0, 0).applyQuaternion(this.cube.transform.worldRotation.clone().invert())
        );
        assertVectorsAlmostEqual(this.sphere!.transform.localToWorldPoint(new Vector3()), this.sphere!.transform.worldPosition);
        assertVectorsAlmostEqual(this.sphere!.transform.worldToLocalPoint(this.sphere!.transform.worldPosition.clone()), new Vector3());
      }
    }
  }
}
let testSystem = new TestSystem();

experience.addSystem(testSystem);
experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    testSystem.setEntity(rootEntity);
  },
  () => {
    testSystem.setEntity(null);
  }
);
