/*
    RemoteApp
    *   Represents an ARScript-enabled app running on a remote device.
    *   Handles communication with the remote app,
        with the exception of actual network traffic
        (that's delegated to ScriptConnectionWorker).
    *   Implements
        executeNative_str(callString: string): RemoteMessage | undefined
        (so it can be used to provide the same-named requirement for ARScript).
    *   Requires:
        *   executeCallback(callbackJsonString: string): string
            Take a string that contains a JSON-encoded object that describes a native -> script callback,
            find and execute the callback referenced by that object,
            return a string JSON-encoding an object with either a `value` or an `error` field.
        *   replicateNativeToScript(nativeObject: { className: string, objectId: string}, name: string | undefined): any
            Given `nativeObject`, construct a corresponding script-side object that is a replication of
            the native object. `nativeObject` is an object that contains `className` and `objectId` fields.
            Return the newly constructed script object.
            If `name` is defined, then also make it available as a global with that name.
            (Used when exporting native-created objects to script.)
        *   These requirements are typically provided by an ARScript instance.
    *   Note executeNative_str can block the main thread. This is intentional.
        Script -> native calls are normally (when running on device) synchronous.
        We want remote script execution to work pretty much exactly like on-device execution
        (obviously with the exception of network delay),
        so even when doing networked remote script execution, script -> native calls are still sync.
        The way this is implemented is as follows:
        *   RemoteApp launches a worker thread.
            The worker is responsible for sending and receiving messages to/from the remote app.
        *   The worker can receive messages even when the main thread is blocked in a script -> native call
            (waiting for a result from the native side, see getCallResultForQueryMessageId()).
        *   When receiving a message, the worker puts it into a shared buffer,
            and notifies RemoteApp on the main thread.
        *   The worker actually sends two notifications (of different kinds),
            because the main thread, depending on its state, can only react to one of them
            at any time. The two kinds of notifications are:
            1)  A message sent using parentPort.postMessage,
                handled in RemoteApp using worker.on('message', ...).
                RemoteApp can only receive such messages when _not_ blocked in getCallResultForQueryMessageId().
            2)  Using Atomics.notify() on a shared messageLengthArray.
                This is received by Atomics.wait() in getCallResultForQueryMessageId().
        *   When receiving either of the two notifications, RemoteApp reads the remote message
            from a shared buffer and processes it.
        *   getCallResultForQueryMessageId() doesn't return until the result of the specified call is received.
        *   If some other message (eg. a native -> script callback) is received
            while waiting in getCallResultForQueryMessageId(), then getCallResultForQueryMessageId() takes care
            of dispatching it.
            Note this may result in re-entering getCallResultForQueryMessageId() if the script-side function
            just called from the native side calls back to native.
        *   Once getCallResultForQueryMessageId() has a result, it returns.
            If this was the top-level getCallResultForQueryMessageId(), then normal execution is resumed
            (as opposed to running a remote-message loop inside getCallResultForQueryMessageId()).
            After resuming normal execution, newly received remote messages are handled using
            didReceiveWorkerMessage().
*/

import { Worker } from "worker_threads"
import * as http from "http"
import { exec } from "child_process"
import fs from 'fs'
import qrcode from "qrcode-generator"
import { assert } from "console"


export interface RemoteAppRequirements {
  executeCallback(callbackJsonString: string): string | Promise<string>
  replicateNativeToScript(nativeObject: { className: string, objectId: string }, name: string | undefined): any
}

interface RemoteMessage {
  messageId?: number
  respondingToMessageId?: number
  error?: string
  value?: string
  scriptCallback?: string
  exportObjectId?: string
  exportObjectClassName?: string
  exportObjectGlobalVarName?: string
  nativeCall?: string
}

export class RemoteApp {
  constructor(requirements: RemoteAppRequirements) {
    this.requirements = requirements

    this.workerMessageLengthBuffer = new SharedArrayBuffer(4)
    this.workerMessageLengthArray = new Int32Array(this.workerMessageLengthBuffer)
    this.workerMessageBuffer = new SharedArrayBuffer(256 * 1024)
    this.workerMessageArray = new Uint8Array(this.workerMessageBuffer)

    //  note we only use this hardwired path in remote dev/debug mode,
    //  not in production (when running the script on-device).
    const workerPath = './node_modules/arsdk/lib/ios/remote/script-connection-worker.js'
    if (!fs.existsSync(workerPath))
      throw "worker not found: " + workerPath
    this.worker = new Worker(
      workerPath,
      {
        workerData: {
          messageLengthBuffer: this.workerMessageLengthBuffer,
          messageBuffer: this.workerMessageBuffer
        }
      })
    this.worker.on('message', (message) => { this.didReceiveWorkerMessage(message) })

    this.withUrlDo(url => console.log("worker started with url: ", url))

    this.launchQRServer()
  }

  executeNative_str(callString: string): string {
    let messageId: number = this.sendNativeCall(callString)
    let resultString: string = this.getCallResultForQueryMessageId(messageId)
    return resultString
  }

  //  private

  private requirements: RemoteAppRequirements

  private worker: Worker
  private workerMessageLengthBuffer: SharedArrayBuffer
  private workerMessageLengthArray: Int32Array
  private workerMessageBuffer: SharedArrayBuffer
  private workerMessageArray: Uint8Array

  private nextMessageId = 1
  private pendingValueMessageStrings: string[] = []

  private launchQRServer() {
    this.withUrlDo(url => this.launchQRServerWithUrl(url))
  }

  private withUrlDo(action: ((url: string) => void)) {
    //  TODO shouldn't just assume that we're running on Gitpod,
    //  should use some more generic way of getting the url + port.
    exec(
      "gp url 2897",
      (_error, stdout, _stderr) => {
        let url = stdout.trim()
        action(url)
      })
  }

  private launchQRServerWithUrl(url: string) {
    let qr = qrcode(4, 'L')
    qr.addData(url)
    qr.make()
    let qrHtml = qr.createImgTag(8)
    let html = `
            <div id="qr"></div>
            <script type="text/javascript">
            document.getElementById('qr').innerHTML = '${qrHtml}'
            </script> `

    let server = http.createServer(
      (_request: http.IncomingMessage, response: http.ServerResponse) => {
        response.end(html)
      })
    server.on('error', (error) => { console.log("qr server error: ", error) })
    server.listen(2898)
  }

  private sendNativeCall(callString: string): number {
    return this.sendMessage({ nativeCall: callString })
  }

  private sendResponse(value: any, queryMessageId: number) {
    value = (value === undefined) ? "nil" : value
    this.sendMessage({ value: value, respondingToMessageId: queryMessageId })
  }

  private sendError(error: string, queryMessageId: number) {
    console.log(`sendError: queryMessageId = ${queryMessageId}, error = ${error}`)
    this.sendMessage({ error: error, respondingToMessageId: queryMessageId })
  }

  private sendMessage(messageObject: RemoteMessage) {
    const messageId = this.nextMessageId++
    messageObject.messageId = messageId
    const messageString = JSON.stringify(messageObject) + "###arsdk-message-separator###"
    // console.log("sending native call string: ", messageString)
    this.sendMessageString(messageString)
    return messageId
  }

  private sendMessageString(messageString: string) {
    this.worker.postMessage(messageString)
  }

  private getCallResultForQueryMessageId(queryMessageId: number): string {
    /*
        wait for a response message from the native side,
        but also handle cases when we receive a message other than a response
        (eg. a native -> script call).
        only return once we receive a value message that holds the result
        of the script -> native call identified by `queryMessageId`.
        once received a result message, return the original JSON-string
        form of that result message.
    */

    while (true) {
      for (let i = 0; i < this.pendingValueMessageStrings.length; ++i) {
        let pendingValueMessageString = this.pendingValueMessageStrings[i]
        let pendingValueMessage = this.handleRemoteMessageString(pendingValueMessageString)
        if (pendingValueMessage === null)
          throw "pendingValueMessage === null"
        if (pendingValueMessage.respondingToMessageId === queryMessageId) {
          this.pendingValueMessageStrings.splice(i, 1)
          return pendingValueMessageString
        }
      }

      let waitResult = Atomics.wait(this.workerMessageLengthArray, 0, 0)
      if (waitResult === "timed-out")
        throw "waitResult === timed-out"
      //  note if waitResult === "not-equal", that just means that a message was put in the buffer
      //  _before_ we called wait() here. not a problem, just proceed with reading it.

      let workerMessageString = this.readWorkerMessageFromSharedBuffer()
      if (workerMessageString === undefined)
        throw Error("getCallResultForQueryMessageId: message === undefined")

      var maybeMessage: RemoteMessage | null =
        this.handleWorkerMessage(workerMessageString)
      if (maybeMessage !== null) {
        //  a message is only returned from handleWorkerMessage() if
        //  handleWorkerMessage() itself couldn't handle it
        //  -- and the only such case is if it's a value message,
        //  that is, a value we received in response to a query.
        if (maybeMessage.respondingToMessageId === queryMessageId) {
          if (maybeMessage.value === undefined)
            throw "maybeMessage.value === undefined"
          let resultValueMessageString = workerMessageString
          return resultValueMessageString
        } else {
          /*
              we've received a response to a different message,
              not the one we're blocked waiting a response to.
              this is a rare edge case, but we need to handle it.
              may happen when a script-to-native call is started roughly the
              same time as a native-to-script callback is started.
              what may happen is something like this:
              *   script calls native, starts waiting for a response.
              *   native sends a callback (before receiving the call from script).
              *   native receives the call from script, executes it, sends a response.
              *   while waiting for a response to the original script-to-native call,
                  script receives a callback, exeutes it.
              *   the script-side callback handler does a second script-to-native call,
                  starts waiting for a response to this second call.
              *   at this point script receives the response to the first to-native call
                  (while blocked waiting a response for the second call).
              in such cases we push the response to a pending queue.
              later on we should receive a response to the second (inner) s2n call,
              then we'll return from the inner getCallResultForQueryMessageId(),
              then the outer getCallResultForQueryMessageId() will check the pending queue
              and find the response it's waiting for in the queue.
          */
          this.pendingValueMessageStrings.push(workerMessageString)
        }
      }
    }
  }

  private readWorkerMessageFromSharedBuffer(): string | undefined {
    if (this.workerMessageLengthArray[0] <= 0) {
      /*
          looks like the message has already been read out from the buffer.
          this can happen if readWorkerMessage() is called from didReceiveWorkerMessage,
          but the worker -> main delivery of the "didReceiveWorkerMessage" notification was delayed
          while the main thread was blocked in getCallResultForQueryMessageId().
          in this case by the time "didReceiveWorkerMessage" gets delivered,
          and in response to that, readWorkerMessage() gets called,
          getCallResultForQueryMessageId() has already read and handled the message,
          there's nothing to do here, all is fine.
          (if we get to this branch when called from getCallResultForQueryMessageId(),
          that's an error; handled in getCallResultForQueryMessageId().)
      */
      return undefined
    }
    let newWorkerMessageArray = this.workerMessageArray.slice(0, this.workerMessageLengthArray[0])
    let newWorkerMessageString = (new TextDecoder("utf8")).decode(newWorkerMessageArray)
    this.workerMessageLengthArray[0] = 0
    Atomics.notify(this.workerMessageLengthArray, 0, 1)
    return newWorkerMessageString
  }

  private handleWorkerMessage(workerMessageString: string): RemoteMessage | null {
    if (workerMessageString.indexOf("error: ") == 0) {
      //  not really a remote message, but rather an error message
      //  from the remote receiver worker.
      throw new Error("worker " + workerMessageString)
    }
    if (workerMessageString.indexOf("log: ") == 0) {
      console.log("worker ", workerMessageString)
      return null
    }

    //  if the worker message does not have any of the known prefixes,
    //  then it is a remote message (received from the native side),
    //  a RemoteMessage encoded into a JSON string.
    let remoteMessageString = workerMessageString
    return this.handleRemoteMessageString(remoteMessageString)
  }

  private handleRemoteMessageString(remoteMessageString: string): RemoteMessage | null {
    try {
      let message: RemoteMessage = JSON.parse(remoteMessageString)
      if (message.error !== undefined)
        throw message.error
      else if (message.value !== undefined)
        //  we should only receive value messages when waiting for a
        //  script -> native call result in getCallResultForQueryMessageId().
        // throw new Error("unexpected value message: " + JSON.stringify(message))
        return message
      else if (message.scriptCallback !== undefined)
        this.handleCallbackMessage(message)
      else if (message.exportObjectId !== undefined)
        this.handleExportObjectMessage(message)
      else
        throw new Error("unrecognized message: " + JSON.stringify(message))
    } catch (x) {
      console.log("exception in handleRemoteMessageString: ")
      console.log(x)
      throw x
    }
    return null
  }

  private handleCallbackMessage(message: RemoteMessage) {
    if (this.requirements === undefined)
      throw "this.requirements === undefined"
    if (message.scriptCallback === undefined)
      throw "message.scriptCallback === undefined"
    if (message.messageId === undefined)
      throw "message.messageId === undefined"

    const queryMessageId: number = message.messageId

    let resultStringOrPromise: string | Promise<string> =
      this.requirements.executeCallback(message.scriptCallback)
    if (typeof resultStringOrPromise === "string") {
      this.processCallbackResultString(resultStringOrPromise, queryMessageId)
    } else {
      //  the callback result is a promise; see comments in executeCallback().
      resultStringOrPromise.then(resultString =>
        this.processCallbackResultString(resultString, queryMessageId))
    }
  }

  private processCallbackResultString(resultString: string, queryMessageId: number) {
    var result = JSON.parse(resultString)
    if (result.error !== undefined) {
      this.sendError(result.error, queryMessageId)
    } else {
      this.sendResponse(result.value, queryMessageId)
    }
  }

  private handleExportObjectMessage(message: RemoteMessage) {
    if (this.requirements === undefined) return
    if (message.exportObjectClassName === undefined || message.exportObjectId === undefined) return
    if (message.messageId === undefined) return
    const nativeObject = { className: message.exportObjectClassName, objectId: message.exportObjectId }
    this.requirements.replicateNativeToScript(nativeObject, message.exportObjectGlobalVarName)
    this.sendResponse("ack", message.messageId)
  }

  private didReceiveWorkerMessage(workerPortMessage: string) {
    if (workerPortMessage !== "didReceiveWorkerMessage") {
      console.log("unexpected port message from worker: ")
      console.log(workerPortMessage)
      assert(false)
    }

    let workerBufferMessage = this.readWorkerMessageFromSharedBuffer()
    if (workerBufferMessage !== undefined)
      this.handleWorkerMessage(workerBufferMessage)
  }
}
