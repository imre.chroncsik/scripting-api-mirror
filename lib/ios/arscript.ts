
/*
    Implements some scripting-related helpers
    (agnostic of ARSDK, could be reused in other projects for scripting support),
    related to dispatching calls to the native side,
    conversions between JS types and our RPC string format,
    object replication between the native and script sides, etc.

    Its most important usage is to provide functionality required by
    other classes / modules;
    eg. ios-bootstrap resolves arsdk_ios_raw's requirements by passing
    an ARScript instance to arsdk_ios_raw.init().

    The implemented helpers are:
    *   executeNative(objectId, methodName, params: any[]) => result: any
        Built on top of the executeNative_str(callString) requirement.
        Execute a call on the native side:
        convert parameters to the RPC string format,
        build a JSON object describing the function call,
        dump the call JSON into a string, pass that to executeNative_str().
        (This function is still untyped (`params: any[]`),
        but works with JS values instead of strings.)
    *   executeCallback(callbackJsonString) => result: string
        Take a string received from the native side,
        that describes a native-to-script callback, and execute it.
        Parse the string into a JSON object with a callbackId and parameters,
        convert the parameters from the RPC string format to JS values,
        look up the callback function by callbackId and call it,
        convert the result to the RPC string format, return the result string.
    *   replicateNewObject(newObject: any, className: string, ...params: any[])
        Called from script-side class constructors,
        either when the constructor is directly called from script (`new MyClass(...)`),
        or called during replicating an object from the native side.
        Make sure newObject exists on both sides (native and script),
        and the two representations are connected by a shared objectId.

    Requirements of ARScript:
    *   executeNative_str(callString) => ExecuteNativeStrResult | undefined
        Execute `callString` on the native side,
        return string results: either a `value` or an `error` inside an ExecuteNativeStrResult.
*/

//  TODO remove ios-specific
import {
    NewObjectReplicationInfo,
    classesByName
}
    from "./arsdk-ios-tools"

export interface ExecuteNativeStrResult {
    value?: string
    error?: string
}

export class ARScript {
    executeNative_str?: ((callString: string) => string) = undefined
    setGlobal?: (name: string, value: any) => void = undefined

    isBatchingNativeCalls = false
    pendingBatchedNativeCallStrings: string[] = []

    //  TODO should be private
    callbacks: { [callbackId: string]: any } = {}

    startBatchingNativeCalls() {
        this.isBatchingNativeCalls = true
    }

    endBatchingNativeCalls() {
        this.isBatchingNativeCalls = false
        if (this.executeNative_str === undefined)
            throw "this.executeNative_str === undefined"

        const callString = JSON.stringify(this.pendingBatchedNativeCallStrings)
        var resultString: string = this.executeNative_str(callString)
        const resultValue = this.scriptValueFromNativeCallResultJsonString(resultString, false)
    }

    executeNative(objectId: string, methodName: string, ...params: any[]): any {
        // if (methodName === "getComponentsByClassScriptName") {
        //     // console.log(params[0])
        //     // if (params[0] == "MeshRenderable")
        //     //     console.log("MeshRenderable 1")
        //     if (typeof params[0] === "string" && params[0] == "MeshRenderable") {
        //         console.log("MeshRenderable 2")
        //         console.trace()
        //     }
        // }

        const callString = this.createCallString(objectId, methodName, ...params)

        if (this.isBatchingNativeCalls) {
            //  TODO check that the method's return type is void
            this.pendingBatchedNativeCallStrings.push(callString)
            return
        }

        if (this.executeNative_str === undefined)
            throw "this.executeNative_str === undefined"
        var resultJsonString: string = this.executeNative_str(callString)

        //  TODO instead of hardwiring ctor names, we should have something like
        //  executeNative(execParams: ExecParams, ...params: any[]): any { ... }
        //  interface ExecParams { objectId: string, methodName: string, isConstructor?: boolean }
        //  then treat isConstructor == undefined as isConstructor == false,
        //  and explicitly pass isConstructor = true when calling execNative during replication
        //  (when called from replicateNewObject()).
        //  (possibly also rename isConstructor to isReplicatingScriptToNative)
        const isConstructor = methodName === "new" || methodName === "script_construct"

        const resultValue = this.scriptValueFromNativeCallResultJsonString(resultJsonString, isConstructor)
        return resultValue
    }

    createCallString(objectId: string, methodName: string, ...params: any[]): string {
        if (objectId === "arsdk_uninitialized_objectId")
            throw "objectId === arsdk_uninitialized_objectId"

        //  this removed trailing `undefined`s, but why?
        //  actually, i think that can cause param count mismatches.
        //  commented out for now.
        //  TODO either fully remove this, or add comments.
        // while (params.length > 0 && params[params.length - 1] === undefined)
        //     params.pop()

        params = params.map(p => this.scriptValueToNativeString(p))

        const callString = JSON.stringify({
            "objectId": objectId,
            "methodName": methodName,
            "parameters": params
        })
        return callString
    }

    scriptValueFromNativeCallResultJsonString(resultString: string, isConstructor: boolean): any {
        var result: ExecuteNativeStrResult = JSON.parse(resultString)
        if (result.error !== undefined)
            throw result.error
        if (result.value === undefined)
            throw new Error("result.value === undefined")

        const resultValue = this.nativeValueStringToScriptValue(result.value, isConstructor)
        return resultValue
    }

    executeCallback(callbackJsonString: string): string | Promise<string> {
        try {
            const callbackObject = JSON.parse(callbackJsonString)
            const callback = this.callbacks[callbackObject.callbackId]
            if (callback === undefined)
                throw "callback not found by id: " + callbackObject.callbackId
            const params = callbackObject.params.map(
                (p: any) => this.nativeValueStringToScriptValue(p, false))
            const callbackResult = callback(...params)

            let callbackResultIsPromise = typeof callbackResult?.then === "function"
            if (callbackResultIsPromise) {
                /*
                    this is a rare edge case, at the moment the only reason why we support it
                    is about handling the "start" callback, when using ES modules.
                    the "start" handler loads the main user script as a module.
                    this would be trivial with CommonJS modules and require(),
                    but we're using ES modules.
                    with normal `import`, ES modules are loaded in advance
                    (all referenced modules loaded before executing the importing module),
                    we can't wait until the "start" callback arrives and only load then.
                    with ES2017 dynamic `import()` it's possible to delayed-load modules,
                    but dynamic `import()` is async,
                    which means that the "start" handler also needs to be async.
                    note at the moment this works a bit differently
                    when running on-device vs in remote dev mode:
                    *   remote (see remote-bootstrap-xp.ts):
                        the callback handler is truly async, it returns a promise,
                        and we only send the result message once the promise is resolved.
                    *   on-device (see ios-bootstrap-xp.ts):
                        the callback handler returns the result of import(),
                        which means it also returns a promise (just like in the remote case),
                        but that promise gets ignored.
                        __arscript_executeCallback() (called by native) returns immediately,
                        it doesn't really wait for import() to finish.
                        TODO theoretically __arscript_executeCallback() not waiting for
                        import() to finish could cause problems, would be nice if it did wait.
                */
                return callbackResult.then((value: any) => {
                    return JSON.stringify({ "value": value })
                })
            }
            return JSON.stringify({ "value": callbackResult })
        } catch (x) {
            //  TODO replace console.log() with something that doesn't break when running on device
            console.log("exception caught in executeCallback(): ")
            console.log(x)
            let errorMessage = "unknown"
            if (x instanceof Error)
                errorMessage = x.toString()
            return JSON.stringify({ "error": errorMessage })
        }
    }

    replicateNewObject(newObject: any, className: string, ...params: any[]) {
        let replicationInfo: NewObjectReplicationInfo | undefined = undefined
        if (params.length > 0 && params[0] instanceof NewObjectReplicationInfo)
            replicationInfo = params[0]

        if (replicationInfo === undefined) {
            //  newObject was created on the script side, with a normal constructor call.
            //  construct a corresponding object on the native side.
            const nativeObject = this.executeNative("arsdk_class_" + className, "script_construct", ...params)
            this.setObjectId(newObject, nativeObject.objectId)
        } else {
            if (replicationInfo.nativeObjectId !== undefined) {
                //  newObject was created on the native side, now replicating to script,
                //  the constructor was called from replicateNativeToScript().
                this.setObjectId(newObject, replicationInfo.nativeObjectId)
            } else if (replicationInfo.doNotReplicateToNative === true) {
                //  newObject was created on the script side, but no replication is needed.
                //  eg. base class contructor called from subclass constructor
                //  => replication will be handled by the subclass constructor.
            } else {
                throw "replicateNewObject() -- this should never happen"
            }
        }
    }

    replicateNativeToScript(
        nativeObject: { className: string, objectId: string },
        name: string | undefined = undefined)
        : any {

        const class_ = classesByName[nativeObject.className]
        if (class_ === undefined)
            throw "could not find class by name: " + nativeObject.className
        const newObject = new class_(new NewObjectReplicationInfo(nativeObject.objectId, true))
        if (newObject.objectId !== nativeObject.objectId)
            throw "newObject.objectId !== nativeObject.objectId"

        if (name !== undefined
            && this.setGlobal !== undefined) {
            this.setGlobal(name, newObject)
        }

        return newObject
    }


    //  private

    private userMainFunction = null
    private objectsById: { [objectId: string]: any } = {}
    private nextCallbackId = 0

    private setObjectId(object: any, objectId: string) {
        object.objectId = objectId
        this.objectsById[objectId] = object
    }

    private scriptValueToNativeString(scriptValue: any): string {
        return JSON.stringify(this.scriptValueToNativeValue(scriptValue))
    }

    private scriptValueToNativeValue(scriptValue: any): any {
        if (scriptValue === undefined)
            // return "arsdk__nil__"
            throw "scriptValue === undefined"
        if (scriptValue === null)
            return "arsdk__nil__"

        if (typeof scriptValue === "function") {
            const callbackId = this.nextCallbackId++
            this.callbacks[callbackId] = scriptValue
            return callbackId
        }

        if ((typeof scriptValue === "object") && ("objectId" in scriptValue)) {
            return scriptValue.objectId
        }

        if (Array.isArray(scriptValue)) {
            let scriptValues = scriptValue
            let valueCount = scriptValues.length
            let nativeValueStrings = Array(valueCount)
            for (let v = 0; v < valueCount; ++v)
                nativeValueStrings[v] = this.scriptValueToNativeValue(scriptValues[v])
            return nativeValueStrings
        }

        return scriptValue
    }

    private nativeValueToObjectId(nativeValue: any): string | null {
        if (typeof nativeValue === "string"
            && nativeValue.indexOf("arsdk__objectId_") !== -1) {
            const objectId = nativeValue.substring("arsdk__objectId_".length)
            return objectId
        }
        return null
    }

    private nativeValueStringToScriptValue(
        nativeValueString: string, isConstructorResult: boolean): any {

        if (nativeValueString === "()")
            return undefined
        if (nativeValueString === "nil")
            return null

        //  TODO is this case still in use? don't we always pass objects from native to script
        //  as a small JSON object containing objectId and className?
        let maybeObjectId = this.nativeValueToObjectId(nativeValueString)
        if (maybeObjectId !== null)
            return maybeObjectId

        //  if none of the above special cases, then it should be a JSON string.
        try {
            let nativeValueJSON = JSON.parse(nativeValueString)
            if (nativeValueJSON === undefined)
                throw "nativeValueJson === undefined"
            return this.nativeValueJSONToScriptValue(nativeValueJSON, isConstructorResult)
        } catch (x) {
            console.log("Exception caught in nativeValueStringToScriptValue(): ")
            console.log(x)
            throw x
        }
    }

    private nativeValueJSONToScriptValue(nativeValue: any, isConstructorResult: boolean): any {
        if (nativeValue === "()")
            return undefined

        let maybeObjectId = this.nativeValueToObjectId(nativeValue)
        if (maybeObjectId !== null)
            return maybeObjectId

        if (Array.isArray(nativeValue)) {
            if (isConstructorResult)
                throw "isConstructorResult vs array"
            let nativeValues = nativeValue
            let valueCount = nativeValues.length
            let scriptValues = Array(nativeValues.length)
            for (let v = 0; v < valueCount; ++v)
                scriptValues[v] = this.nativeValueJSONToScriptValue(nativeValues[v], false)
            return scriptValues
        }

        if (typeof nativeValue === "object"
            && nativeValue.arsdk_objectId !== undefined
            && nativeValue.arsdk_className !== undefined) {

            const nativeObject = {
                className: nativeValue.arsdk_className,
                objectId: nativeValue.arsdk_objectId
            }
            if (isConstructorResult) {
                //  we've received this value as the result of calling a native-side constructor,
                //  which means we're in the middle of replicating the object from script to native
                //  (executeNative() was called from replicateNewObject()),
                //  so no need to request replication of this object again.
                //  just return the objId + className to replicateNewObject().
                return nativeObject
            } else {

                var object = this.objectsById[nativeObject.objectId]
                if (object === undefined)
                    object = this.replicateNativeToScript(nativeObject)
                return object
            }
        }

        return nativeValue
    }
}

