declare module "arsdk/tween" {
  import { Component, Entity, Quaternion, Vector3 } from "arsdk";
  enum Interpolator {
    linear = "linear",
    easeInQuad = "easeInQuad",
    easeOutQuad = "easeOutQuad",
    easeInOutQuad = "easeInOutQuad",
    easeOutInQuad = "easeOutInQuad",
    easeInCubic = "easeInCubic",
    easeOutCubic = "easeOutCubic",
    easeInOutCubic = "easeInOutCubic",
    easeOutInCubic = "easeOutInCubic",
    easeInQuart = "easeInQuart",
    easeOutQuart = "easeOutQuart",
    easeInOutQuart = "easeInOutQuart",
    easeOutInQuart = "easeOutInQuart",
    easeInQuint = "easeInQuint",
    easeOutQuint = "easeOutQuint",
    easeInOutQuint = "easeInOutQuint",
    easeOutInQuint = "easeOutInQuint",
    easeInSine = "easeInSine",
    easeOutSine = "easeOutSine",
    easeInOutSine = "easeInOutSine",
    easeOutInSine = "easeOutInSine",
    easeInExpo = "easeInExpo",
    easeOutExpo = "easeOutExpo",
    easeInOutExpo = "easeInOutExpo",
    easeOutInExpo = "easeOutInExpo",
    easeInCirc = "easeInCirc",
    easeOutCirc = "easeOutCirc",
    easeInOutCirc = "easeInOutCirc",
    easeOutInCirc = "easeOutInCirc",
    easeInBack = "easeInBack",
    easeOutBack = "easeOutBack",
    easeInOutBack = "easeInOutBack",
    easeOutInBack = "easeOutInBack",
    easeInBounce = "easeInBounce",
    easeOutBounce = "easeOutBounce",
    easeInOutBounce = "easeInOutBounce",
    easeOutInBounce = "easeOutInBounce",
  }
  /**
   * Spring physics based easing.
   */
  class SpringInterpolator {
    readonly asString: string;
    constructor(mass: number, stiffness: number, damping: number, velocity: number);
  }
  enum EaseEnds {
    easeIn = "easeIn",
    easeInOut = "easeInOut",
    easeOut = "easeOut",
    easeOutIn = "easeOutIn",
  }
  /**
   * Elastic easing.
   */
  class ElasticInterpolator {
    readonly asString: string;
    constructor(easeEnds: EaseEnds, amplitude: number, period: number);
  }
  /**
   * Use your own custom cubic Bézier curves
   */
  class CubicBezierInterpolator {
    readonly asString: string;
    constructor(x1: number, y1: number, x2: number, y2: number);
  }
  /**
   * Defines the number of jumps an animation takes to arrive at its end value.
   */
  class StepsInterpolator {
    readonly asString: string;
    constructor(numberOfSteps: number);
  }
  /**
   * The direction of the tween.
   */
  enum TweenDirection {
    Normal = "normal",
    Reverse = "reverse",
    /**
     * Reverse whenever the animation reaches its end. Only makes sense if loop is enabled.
     */
    Alternate = "alternate",
  }

  /**
   * A general tweening component. Can be used to tween anything by providing a custom "update()" callback.
   */
  class TweenComponent extends Component {
    constructor(
      start: number,
      end: number,
      durationSeconds: number,
      update: (entity: Entity, a: number) => void,
      easing?: Interpolator | SpringInterpolator | CubicBezierInterpolator | ElasticInterpolator,
      onFinishCallback?: (() => void) | null,
      direction?: TweenDirection,
      loop?: Boolean
    );

    /**
     * Reverses the direction of an animation.
     */
    reverse(): void;
    /**
     * Restarts an animation from its initial values.
     */
    restart(): void;
    /**
     *  Jump to a specific time (in seconds).
     */
    seek(time: number): void;
    /**
     * Pauses a running animation.
     */
    pause(): void;
    /**
     * Plays a paused animation.
     */
    play(): void;
    update(dt: number): void;
  }
  /**
   * Animates the world position of an Entity.
   */
  class MoveTransformComponent extends TweenComponent {
    constructor(
      start: Vector3,
      end: Vector3,
      duration: number,
      onFinishCallback?: (() => void) | null,
      easing?: Interpolator | SpringInterpolator | CubicBezierInterpolator | ElasticInterpolator,
      direction?: TweenDirection,
      loop?: Boolean,
      update?: ((entity: Entity, a: Vector3) => void) | null
    );
  }

  /**
   * Animates the world scale of an Entity.
   */
  class ScaleTransformComponent extends TweenComponent {
    constructor(
      start: Vector3,
      end: Vector3,
      duration: number,
      onFinishCallback?: (() => void) | null,
      easing?: Interpolator | SpringInterpolator | CubicBezierInterpolator | ElasticInterpolator,
      direction?: TweenDirection,
      loop?: Boolean,
      update?: ((entity: Entity, a: Vector3) => void) | null
    );
  }
  /**
   * Animates the world rotation of an Entity.
   */
  class RotateTransformComponent extends TweenComponent {
    constructor(
      start: Quaternion,
      end: Quaternion,
      duration: number,
      onFinishCallback?: (() => void) | null,
      easing?: Interpolator | SpringInterpolator | CubicBezierInterpolator | ElasticInterpolator,
      direction?: TweenDirection,
      loop?: Boolean,
      update?: ((entity: Entity, a: Quaternion) => void) | null
    );
  }
  export {
    TweenComponent,
    MoveTransformComponent,
    Interpolator,
    TweenDirection,
    SpringInterpolator,
    EaseEnds,
    ElasticInterpolator,
    StepsInterpolator,
    CubicBezierInterpolator,
    RotateTransformComponent,
    ScaleTransformComponent,
  };
}
