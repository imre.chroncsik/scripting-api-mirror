import { Animation, Entity, experience, MeshRenderableComponent, ModelAnimatorComponent, OnTapComponent, System, TransformComponent } from "arsdk";
import { assert, assertEqual, assertFloatAlmostEqual, assertThrows } from "../../../lib/assert";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/CfsZD
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/ios/index.json
 * Web: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/fa3f5772-6522-475f-9f90-fbaf20a6a5cb/v1/web/index.json
 * Video: https://www.youtube.com/watch?v=UQ1rP0VwyPI
 */

assertEqual(experience.uid, "fa3f5772-6522-475f-9f90-fbaf20a6a5cb", "Wrong experience uid");

class TestSystem extends System {
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    if (!this.entity) {
      return;
    }

    let animations = this.entity.getComponent(ModelAnimatorComponent)!.getAnimations();
    let set = new Set();

    for (let anim of animations) {
      if (anim.isPlaying()) {
        assert(!set.has(anim.layer), "Maximum one animation can play at the same time for one layer");
        set.add(anim.layer);
        assert(anim.time >= 0);
      }
    }
  }
}

let testSystem = new TestSystem();
experience.addSystem(testSystem);

let cubeSceneRootAnimator: ModelAnimatorComponent | null = null;
let cube1BottomLeft: Entity | null = null;

//9 cubes scene
experience.addImmersiveObjectLifecycleListeners(
  "9b793829-cd68-4f2d-8f0e-85dddb009ff6",
  (rootEntity) => {
    testSystem.setEntity(rootEntity);

    const animator = rootEntity.getComponent(ModelAnimatorComponent)!;
    cubeSceneRootAnimator = animator;
    let animations = animator.getAnimations();
    assertEqual(animations.length, 9, "Incorrect number of animations");

    let linearTranslationAnimation = animator!.getAnimation("Linear Translation");
    assertEqual(linearTranslationAnimation.isPlaying(), false);
    assertFloatAlmostEqual(linearTranslationAnimation.duration, 1.6666666269302368);
    assertEqual(linearTranslationAnimation.name, "Linear Translation");
    linearTranslationAnimation.play();

    if (cube1BottomLeft) {
      cube1BottomLeft.destroy();
    }

    //tests for what happens if we delete or reparent entities of the subgraph
    cube1BottomLeft = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.001";
    })!;
    cube1BottomLeft.getComponent(MeshRenderableComponent)?.material.color.set(0, 1, 0, 1);
    cube1BottomLeft.setParent(null, false);
    cube1BottomLeft.transform.localPosition.set(0, 0, -2);

    let cube3Middle = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.003";
    })!;
    cube3Middle.getComponent(MeshRenderableComponent)?.material.color.set(1, 0, 0, 1);
    let cube4MiddleRight = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.004";
    })!;
    cube4MiddleRight.getComponent(MeshRenderableComponent)?.material.color.set(0, 0, 1, 1);
    cube3Middle.parent = cube4MiddleRight;

    let cube8TopRight = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cube.008";
    })!;
    cube8TopRight.destroy();

    let tapCount = 0;
    rootEntity.getChild(0)!.addComponent(
      new OnTapComponent(() => {
        let anim = animations[tapCount];
        tapCount = (tapCount + 1) % animations.length;

        //layers to allow simultaneous playing of animations
        anim.layer = tapCount;

        if (anim.isPlaying()) {
          console.log("pausing " + animations[tapCount].name);
          anim.pause();
          assertEqual(anim.isPlaying(), false, "isRunning should be false after animation was paused");
        } else {
          console.log("playing " + animations[tapCount].name);
          anim.play();
          assertEqual(anim.isPlaying(), true, "isRunning should be true after animation was started");
          anim.loop = true;
        }
      }, true)
    );
  },
  () => {
    testSystem.setEntity(null);

    let animations = cubeSceneRootAnimator!.getAnimations();
    for (let anim of animations) {
      assertEqual(anim.isPlaying(), false, "ModelAnimatorComponent was detached, all animations should be stopped");
    }

    assertThrows(() => {
      animations[0].play();
    }, "ModelAnimatorComponent was detached, so animation.play() should throw an exception");

    assertThrows(() => {
      animations[0].layer = 1;
    }, "ModelAnimatorComponent was detached, so animation.setLayer should throw an exception");

    assertThrows(() => {
      animations[0].loop = true;
    }, "ModelAnimatorComponent was detached, so animation.setLoop() should throw an exception");

    assertThrows(() => {
      animations[0].time = 1;
    }, "ModelAnimatorComponent was detached, so animation.setTime() should throw an exception");
  }
);

//Brainstem
//This System sets the position of a joint while the animation is playing
class JointPositionSetterSystem extends System {
  transformComponent: TransformComponent | null = null;

  setLeftArmTransform(transformComponent: TransformComponent) {
    this.transformComponent = transformComponent;
  }
  entity: Entity | null = null;

  setEntity(entity: Entity | null) {
    this.entity = entity;
  }

  update(dt: number): void {
    if (!this.entity) {
      return;
    }

    let rightArm = this.entity.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(1)!;
    rightArm.transform.worldPosition.set(-1, 1, 0);
    //even though the entity is inactive, the transform should still have an effect on the skinned mesh deformation
    rightArm.setActive(false);

    if (this.transformComponent) this.transformComponent.worldPosition.set(1, 1, 0);
  }
}

let jointPositionSetterSystem = new JointPositionSetterSystem();
experience.addImmersiveObjectLifecycleListeners(
  "ad183bef-1efc-4dc2-8cee-9508ebe3334f",
  (rootEntity) => {
    const animator = rootEntity.getComponent(ModelAnimatorComponent)!;
    experience.addSystem(jointPositionSetterSystem);
    jointPositionSetterSystem.setEntity(rootEntity);

    //This animation is unnamed in GLTF. As per Composer/Web alignment, it will be automatically named Animation_1.
    //This should best be sanitized in a preprocessing step.
    assertEqual(animator.getAnimations()[0].name, "Animation_1");
    let animation = animator!.getAnimation("Animation_1");
    animation.play();

    rootEntity.addComponent(
      new OnTapComponent(() => {
        animation.time = 0;
        animation.play();
      }, true)
    );

    //check that scenegraph is correct
    let count = 0;
    rootEntity.forEachDescendantInHierarchy(() => {
      count++;
    });

    assertEqual(count, 23, "Invalid number of entities in the hierarchy. Make sure joint nodes are included. See https://jira.vzbuilders.com/browse/XR-6196");

    //left arm
    let toDestroyEntity = rootEntity.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(0)!.getChild(2)!;
    toDestroyEntity.name = "left arm";
    jointPositionSetterSystem.setLeftArmTransform(toDestroyEntity.transform);
    toDestroyEntity.destroy();
  },
  () => {
    experience.removeSystem(jointPositionSetterSystem);
  }
);

//RobotAnimated
experience.addImmersiveObjectLifecycleListeners("a2244dd0-0755-4018-a7af-88d0cf89a3d9", (rootEntity) => {
  const animator = rootEntity.getComponent(ModelAnimatorComponent)!;
  let animations = animator.getAnimations();

  let tapCount = 0;
  let previousAnim: Animation | null = null;
  rootEntity.addComponent(
    new OnTapComponent(() => {
      console.log("playing " + animations[tapCount].name);
      let anim = animations[tapCount];

      anim.layer = tapCount;
      if (tapCount != 0)
        assertThrows(() => {
          anim.play();
        }, "Can not play animation on same targets");
      anim.layer = 0;
      anim.play();
      if (previousAnim) assertEqual(previousAnim.isPlaying(), false, "Animation on the same layer should have been stopped automatically");
      anim.loop = true;
      previousAnim = anim;
      tapCount = (tapCount + 1) % animations.length;
    }, true)
  );
});

//let the cube follow the animation time
class AnimFollowTestSystem extends System {
  animation: Animation | null = null;

  setAnimation(animation: Animation) {
    this.animation = animation;
  }
  update(dt: number): void {
    if (cube1BottomLeft && this.animation) {
      cube1BottomLeft.transform.worldPosition.set(this.animation.time / 8 - 0.5, 0, 0.3);
    }
  }
}
let animFollowTestSystem = new AnimFollowTestSystem();
experience.addSystem(animFollowTestSystem);

//MorphStresstTest
experience.addImmersiveObjectLifecycleListeners("9ecc7d4f-2242-43dc-be7d-45c4c745ce32", (rootEntity) => {
  const animator = rootEntity.getComponent(ModelAnimatorComponent)!;
  let animations = animator.getAnimations();
  let anim = animations[0];

  anim.play();
  anim.loop = true;
  animFollowTestSystem.setAnimation(anim);

  if (cube1BottomLeft) {
    cube1BottomLeft.transform.localScale.set(0.06, 0.06, 0.06);

    cube1BottomLeft.addComponent(
      new OnTapComponent(() => {
        if (anim.isPlaying()) {
          anim.pause();
        } else {
          anim.play();
        }
      }, true)
    );
  }
  //play animation of column that was tapped
  rootEntity.addComponent(
    new OnTapComponent((worldPos) => {
      let a = (worldPos.x + 0.5) * 8;
      anim.time = a;
    }, true)
  );
});

experience.addOnModeChangedListener((mode) => {
  cube1BottomLeft = null;
});
