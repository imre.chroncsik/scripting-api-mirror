import {
  AudioClip,
  experience,
  MeshRenderableComponent,
  ModelAnimatorComponent,
  OnTapComponent,
  TransformComponent,
  Vector3,
  AudioSourceComponent,
  System,
  ComponentClassDefinition,
  Component,
} from "arsdk";
import { assertEqual, assertVectorsEqual } from "../../../lib/assert";
import { CUBE_SCENE_OBJECT_UID, CUBE_SCENE_EXPERIENCE_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */

assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

const ICQ_SOUND_ID = "2fd468f4-8e6a-4757-af1d-d07d5b9e1e57";
const FAITH_SOUND_ID = "eeeb5063-1339-4123-8326-2a94e75fab23";

class AudioClips {
  static readonly ICQ: AudioClip = experience.getAudioClip(ICQ_SOUND_ID);
  static readonly Faith: AudioClip = experience.getAudioClip(FAITH_SOUND_ID);
}

const tests = new Map<string, Boolean>();
const totalNumberOfTests = 2;
function testComplete(key: string) {
  if (tests.get(key)) {
    return; //already completed
  }
  tests.set(key, true);
  let completedCount = 0;
  tests.forEach((element) => {
    if (element) {
      completedCount++;
    }
  });
  console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
  if (completedCount == totalNumberOfTests) {
    console.log("All tests succeeded! :)");
  }
}

function countComponentsInExperience(component: ComponentClassDefinition<Component>) {
  let totalCount = 0;
  experience.forEachComponent(component, () => {
    totalCount++;
  });

  return totalCount;
}

class TestSystem extends System {
  update(dt: number): void {
    assertExperienceComponentCount();
  }
}

function assertExperienceComponentCount() {
  //even though entity might not be active, it should still experience.forEachComponent should still return all components
  assertEqual(countComponentsInExperience(AudioSourceComponent), 1, "Components should be returned by forEachComponent even if entity is not active");
  assertEqual(countComponentsInExperience(TransformComponent), 292, "Components should be returned by forEachComponent even if entity is not active");
  assertEqual(countComponentsInExperience(ModelAnimatorComponent), 1, "Components should be returned by forEachComponent even if entity is not active");
}

console.log("To test, tap the big cubes");

let testSystem = new TestSystem();

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    //check ModelAnimatorComponent
    let modelanimatorcomponent = rootEntity.getComponent(ModelAnimatorComponent)!;
    let anim = modelanimatorcomponent.getAnimations()[0];
    anim.loop = true;

    anim.play();
    assertEqual(anim.isPlaying(), true);
    rootEntity.setActive(false);

    //explicitly setting the time of the animation should still work
    anim.time = 4;
    //setting transform should still work
    rootEntity.transform.localPosition.set(1, 0, 0);
    assertEqual(anim.isPlaying(), false, "setActive(false) should pause animations");
    anim.play(); //should not have effect
    rootEntity.setActive(true);

    assertEqual(anim.time, 4, "setTime should still work when animator is inactive");
    assertVectorsEqual(rootEntity.transform.localPosition, new Vector3(1, 0, 0), "setting Transform should still work when entity is disabled");
    assertEqual(anim.isPlaying(), false, "setActive(true) should not resume animations");
    const cube1 = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "BigCube1";
    })!;
    const cube2 = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "BigCube2";
    })!;

    //check AudioSourceComponent and MeshRenderableComponent

    let meshRenderable = cube2.getComponent(MeshRenderableComponent)!;
    let audioSource = new AudioSourceComponent(AudioClips.ICQ);
    audioSource.loop = true;
    cube2.addComponent(audioSource);
    assertExperienceComponentCount();
    audioSource.play();
    assertEqual(audioSource.isPlaying(), true);

    meshRenderable.castShadows = false;
    cube2.setActive(false);
    assertExperienceComponentCount();
    audioSource = audioSource.entity!.getComponent(AudioSourceComponent)!; //re-access AudioSourceComponent should still work
    assertEqual(cube2.activeInHierarchy, false, "cube2.activeInHierarchy");
    assertEqual(audioSource.isPlaying(), false, "setActive(false) should pause AudioSource");
    audioSource.play();
    assertEqual(audioSource.isPlaying(), false, "play should have no effect if AudioSource Entity is paused");
    meshRenderable.castShadows = true; //function should still have an effect

    //test swap component to other entity
    cube1.addComponent(cube2.removeComponent(audioSource)!);
    audioSource.play();
    assertEqual(audioSource.isPlaying(), true, "audioSource.play() should have an effect because cube1 is active");
    cube2.addComponent(cube1.removeComponent(audioSource)!);

    cube2.setActive(true);
    audioSource.play();
    assertEqual(audioSource.isPlaying(), true, "play should have an effect if entity is active");
    assertEqual(cube2.activeInHierarchy, true, "cube2.activeInHierarchy");

    const cubes = rootEntity.findDescendantInHierarchy((entity) => {
      return entity.name == "Cubes";
    })!;

    //check hierarchy
    cubes.setActive(false);
    assertExperienceComponentCount();
    assertEqual(cubes.parent!.activeInHierarchy, true, "Parent activeInHierarchy wrong");
    assertEqual(cubes.activeInHierarchy, false, "Entity activeInHierarchy wrong");
    assertEqual(cubes.getChild(0)!.activeInHierarchy, false, "Child activeInHierarchy wrong");
    cubes.setActive(true);
    assertEqual(cubes.activeInHierarchy, true, "Entity activeInHierarchy wrong");
    assertEqual(cubes.getChild(0)!.activeInHierarchy, true, "Child activeInHierarchy wrong");

    testComplete("First tests complete ");

    let active = true;
    cube1.addComponent(
      new OnTapComponent(() => {
        active = !active;

        cube2.setActive(active);

        //disabling an entity with AudioSourceComponent will pause the sound
        assertEqual(cube2.activeInHierarchy, active);
        testComplete("testSetActive1. Audio should have stopped");
      })
    );

    experience.addSystem(testSystem);
  },
  (rootEntity) => {
    experience.removeSystem(testSystem);
  }
);
