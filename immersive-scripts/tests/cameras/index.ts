import {
  ARMode,
  CameraComponent,
  Entity,
  experience,
  OnTapComponent,
  OrbitCameraControllerComponent,
  PreviewMode,
  Quaternion,
  System,
  TransformComponent,
  Vector3,
} from "arsdk";
import {
  assert,
  assertEqual,
  assertNotNull,
  assertNotStrictEqual,
  assertNull,
  assertQuaternionsAlmostEqual,
  assertThrows,
  assertVectorsAlmostEqual,
  vectorsAlmostEqual,
} from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID, CUBE_SCENE_DUCK1_UID, CUBE_SCENE_DUCK2_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

var cubeScenePlaced = false;
var duck2Placed = false;
var duck1Placed = false;
const tests = new Map<string, Boolean>();
const totalNumberOfTests = 10;
function testComplete(key: string) {
  if (tests.get(key)) {
    return; //already completed
  }
  tests.set(key, true);
  let completedCount = 0;
  tests.forEach((element) => {
    if (element) {
      completedCount++;
    }
  });
  console.log(key + " [✓] (" + completedCount + "/" + totalNumberOfTests + ")");
  if (completedCount == totalNumberOfTests) {
    console.log("All tests succeeded! :)");
  }
}

//hold onto camera entity. it should be destroyed on mode switch
let cameraEntity: Entity | null = null;
let previewModeCamera: Entity | null = null;
let orbitCameraControllerRemoved = false;
let firstFrameDuck2 = true;

experience.addOnModeChangedListener((currentMode: ARMode | PreviewMode) => {
  if (cameraEntity) {
    assertNotStrictEqual(cameraEntity, currentMode.camera, "Camera entity was not recreated");
    assertThrows(() => {
      cameraEntity!.getComponent(TransformComponent);
    }, "Camera entity should be destroyed after mode switch");
    testComplete("AR Camera destroyed");
  }

  if (previewModeCamera) {
    assertNotStrictEqual(previewModeCamera, currentMode.camera, "Camera entity was not recreated");
    assertThrows(() => {
      previewModeCamera!.getComponent(TransformComponent);
    }, "PreviewCamera should be destroyed after mode switch");
    testComplete("PreviewCamera destroyed");
  }

  if (currentMode.isAR) {
    cameraEntity = currentMode.camera;
    cameraEntity.getComponent(TransformComponent);
  } else {
    orbitCameraControllerRemoved = false;
    previewModeCamera = currentMode.camera;
    previewModeCamera.getComponent(TransformComponent);
  }
});

if (experience.currentMode.isAR) {
  cameraEntity = experience.currentMode.camera;
} else {
  previewModeCamera = experience.currentMode.camera;
}

const timeDuckCameraAnimationS = 10;
let timeDuckCameraAnimationPlaying = 0;
let cameraPositionOverrideCheckedAfterAnimation = false;

if (!experience.currentMode.isAR) {
  let pivotPositionReference = experience.currentMode.camera.getComponent(OrbitCameraControllerComponent)!.pivotPosition;
  let cameraPositionReference = experience.currentMode.camera.getComponent(OrbitCameraControllerComponent)!.entity!.transform.worldPosition;
  class TestSystem extends System {
    update(dt: number): void {
      if (!experience.currentMode.isAR) {
        if ((experience.currentMode.camera.getComponent(OrbitCameraControllerComponent) && cubeScenePlaced) || duck1Placed || duck2Placed) {
          if (
            vectorsAlmostEqual(pivotPositionReference, new Vector3(0, 0, 0)) &&
            vectorsAlmostEqual(cameraPositionReference, new Vector3(2, 2, 2)) &&
            vectorsAlmostEqual(experience.currentMode.camera.transform.worldPosition, new Vector3(2, 2, 2))
          ) {
            if (!cubeScenePlaced) {
              assert(false, "The camera animation should have been automatically cancelled on object switch.");
            } else {
              removeOrbitCameraController(experience.currentMode.camera);
              testComplete("orbitCameraController.animateTo()");
            }
          }
        }

        if (duck2Placed) {
          if (experience.currentMode.camera.getComponent(OrbitCameraControllerComponent)) {
            timeDuckCameraAnimationPlaying += dt;

            //This should always be overidden by orbitcameracontroller
            experience.currentMode.camera.transform.lookAt(new Vector3(0, 100, 0));

            const overridePositionVector = new Vector3(0, -3, 0);

            if (timeDuckCameraAnimationPlaying < timeDuckCameraAnimationS - 0.5) {
              if (vectorsAlmostEqual(experience.currentMode.camera.transform.worldPosition, overridePositionVector)) {
                assert(false, "OrbitCameraController should override the camera transform at the end of every frame during the animation");
              }
            }

            if (timeDuckCameraAnimationPlaying > timeDuckCameraAnimationS + 0.5 && !cameraPositionOverrideCheckedAfterAnimation) {
              assertVectorsAlmostEqual(
                experience.currentMode.camera.transform.worldPosition,
                overridePositionVector,
                "After OrbitCameraController animation has finished, the script should be able to override the position again"
              );
              testComplete("After OrbitCameraController animation has finished, the script should be able to override the position again");
              cameraPositionOverrideCheckedAfterAnimation = true; //only check once to avoid triggering when gesture is moving the camera afterwards
            }

            //Set the camera position. This should be overidden by orbitcameracontroller's animation while the animation is playing.
            //at the end of OrbitCameraControllers animation, it should snap to that position.
            experience.currentMode.camera.transform.worldPosition.copy(overridePositionVector);

            testComplete("OrbitCameraController should override transform changes");
          } else {
            let previousWorldRotation = experience.currentMode.camera.transform.worldRotation.clone();
            let previousPosition = experience.currentMode.camera.transform.worldPosition.clone();
            experience.currentMode.camera.transform.worldPosition.set(3, -3, 3);
            experience.currentMode.camera.transform.lookAt(new Vector3(0, 0, 0));

            if (!firstFrameDuck2) {
              assertQuaternionsAlmostEqual(
                previousWorldRotation,
                experience.currentMode.camera.transform.worldRotation,
                "Incorrect rotation. OrbitCameraController was removed from the entity, so the rotation should not be changed."
              );

              assertVectorsAlmostEqual(
                previousPosition,
                new Vector3(3, -3, 3),
                "Incorrect position. OrbitCameraController was removed from the entity, so the position should not be changed."
              );
              testComplete("Setting transform without orbitcameracontroller");
            }
            firstFrameDuck2 = false;
          }
        }
      }
    }
  }

  experience.addSystem(new TestSystem());
}

if (!experience.currentMode.isAR) {
  //No object placed yet. The camera position should be zero.
  assertNotNull(experience.currentMode.camera.getComponent(OrbitCameraControllerComponent), "OrbitCameraController null");
  assertVectorsAlmostEqual(
    experience.currentMode.camera.getComponent(OrbitCameraControllerComponent)!.pivotPosition,
    new Vector3(0, 0, 0),
    "Initial pivot position should be zero"
  );
  assertVectorsAlmostEqual(
    experience.currentMode.camera.getComponent(OrbitCameraControllerComponent)!.entity!.transform.worldPosition,
    new Vector3(0, 0, 0),
    "Initial cameraPosition should be zero"
  );
}

function removeOrbitCameraController(previewModeCamera: Entity) {
  let orbitCameraController = previewModeCamera.getComponent(OrbitCameraControllerComponent)!;
  previewModeCamera.removeComponent(previewModeCamera.getComponent(OrbitCameraControllerComponent)!);
  assertNull(previewModeCamera.getComponent(OrbitCameraControllerComponent));

  assertThrows(() => {
    orbitCameraController.pivotPosition;
  }, " orbitCameraController.pivotPosition should throw as OrbitCameraController is not attached to entity");

  assertThrows(() => {
    orbitCameraController.animateTo(new Vector3(), new Vector3(), 0);
  }, "orbitCameraController.animateTo should throw as OrbitCameraController is not attached to entity");

  assertThrows(() => {
    orbitCameraController.stopAnimating();
  }, "orbitCameraController.stopAnimating() should throw as OrbitCameraController is not attached to entity");

  orbitCameraControllerRemoved = true;
}

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    cubeScenePlaced = true;
    //TODO: Composer currently inserts one extra node. Can we remove this?
    rootEntity = rootEntity.getChild(0)!;

    if (!experience.currentMode.isAR) {
      const previewModeCamera = experience.currentMode.camera;
      assertEqual(previewModeCamera.childCount(), 0, "Camera must have 0 children");
      assertNull(previewModeCamera.transform.parent, "Camera parent is not null");
      assertEqual(previewModeCamera, previewModeCamera.transform.entity);
      assertEqual(previewModeCamera.transform.isReadOnly, false);

      assertNotNull(previewModeCamera.getComponent(CameraComponent));

      if (!previewModeCamera.getComponent(OrbitCameraControllerComponent)) {
        previewModeCamera.addComponent(new OrbitCameraControllerComponent());
        orbitCameraControllerRemoved = false;
      }

      assertNotNull(previewModeCamera.getComponent(OrbitCameraControllerComponent));

      assertVectorsAlmostEqual(
        previewModeCamera.getComponent(OrbitCameraControllerComponent)!.pivotPosition,
        new Vector3(-1.4322222, 0.99999994, -0.20388487),
        "Initial camera orbit pivot incorrect"
      );
      assertVectorsAlmostEqual(
        previewModeCamera.getComponent(OrbitCameraControllerComponent)!.entity!.transform.worldPosition,
        new Vector3(-9.669805, 7.5098166, 19.053514),
        "Initial camera orbit position incorrect"
      );

      previewModeCamera.getComponent(OrbitCameraControllerComponent)!.animateTo(new Vector3(0, 0, 0), new Vector3(2, 2, 2), 3);

      assertThrows(() => {
        previewModeCamera.destroy();
      }, "The camera entity can not be destroyed");

      assertThrows(() => {
        previewModeCamera.parent = previewModeCamera.parent;
      }, "Should not be able to change the parent of 3D camera.");

      assertThrows(() => {
        previewModeCamera.setActive(false);
      }, "Should not be able to disable 3D camera");

      //setting it to active should be allowed
      previewModeCamera.setActive(true);

      testComplete("Object placed in 3D");
    } else {
      const arCamera = experience.currentMode.camera;
      assertNotNull(arCamera.getComponent(CameraComponent));

      assertNull(arCamera.parent, "Camera parent is not null");
      assertEqual(arCamera.transform.isReadOnly, true);

      assertThrows(() => {
        arCamera.transform.worldPosition.set(0, 0, 0);
      }, "AR camera transform is not read-only");

      assertThrows(() => {
        arCamera.transform.localScale.setLength(1.0);
      }, "AR camera transform is not read-only");

      assertThrows(() => {
        arCamera.transform.localRotation.multiply(new Quaternion(0, 0, 0, 1));
      }, "AR camera transform is not read-only");

      assertThrows(() => {
        arCamera.transform.parent = rootEntity.transform;
      }, "AR camera transform is not read-only");

      assertThrows(() => {
        arCamera.parent = rootEntity;
      }, "AR camera transform is not read-only");

      //should not throw
      arCamera.transform.localPosition.clone();
      new Vector3().copy(arCamera.transform.localPosition);
      arCamera.transform.localPosition.dot(new Vector3(0, 1, 0));

      //destroying the camera entity is not allowed. It should throw an exception.
      assertThrows(() => {
        arCamera.destroy();
      }, "The camera entity can not be destroyed");

      assertEqual(experience.currentMode.camera.childCount(), 0, "Camera must have 0 children");

      assertThrows(() => {
        arCamera.parent = arCamera.parent;
      }, "Should not be able to change the parent of AR camera. The transform is read-only.");

      assertThrows(() => {
        arCamera.setActive(false);
      }, "Should not be able to disable AR camera");

      //setting it to active should be allowed
      arCamera.setActive(true);

      testComplete("Object placed in AR");
    }
  },
  () => {
    cubeScenePlaced = false;
  }
);

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_DUCK1_UID,
  (rootEntity) => {
    duck1Placed = true;
    if (!experience.currentMode.isAR) {
      if (orbitCameraControllerRemoved) {
        assertNull(experience.currentMode.camera.getComponent(OrbitCameraControllerComponent));
      }
      assertVectorsAlmostEqual(
        experience.currentMode.camera.transform.worldPosition,
        new Vector3(3.134135486113851, 4.262587987470033, 1.97442699523859),
        "Intial camera position invalid."
      );
      testComplete("Duck1 initial camera position");
    }
  },
  () => {
    duck1Placed = false;
  }
);

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_DUCK2_UID,
  (rootEntity) => {
    duck2Placed = true;
    firstFrameDuck2 = true;

    console.log("Tap duck to stop animation.");

    if (!experience.currentMode.isAR) {
      const previewModeCamera = experience.currentMode.camera;
      if (orbitCameraControllerRemoved) {
        assertNull(experience.currentMode.camera.getComponent(OrbitCameraControllerComponent));
      } else {
        timeDuckCameraAnimationPlaying = 0;
        cameraPositionOverrideCheckedAfterAnimation = false;
        previewModeCamera.getComponent(OrbitCameraControllerComponent)!.animateTo(new Vector3(0, 0, 0), new Vector3(2, 0, 0), 10);

        const duck = rootEntity.findDescendantInHierarchy((entity) => {
          return entity.name == "LOD3spShape";
        })!;
        duck.addComponent(
          new OnTapComponent(() => {
            timeDuckCameraAnimationPlaying = 999999; //set animation to finished
            cameraPositionOverrideCheckedAfterAnimation = true;
            previewModeCamera.getComponent(OrbitCameraControllerComponent)!.stopAnimating();
            testComplete("Duck tapped - animation stopped");
          })
        );
      }
    }
  },
  () => {
    duck2Placed = false;
  }
);
