#!/usr/bin/env node

const { ArgumentParser } = require("argparse");
const { version } = require("./package.json");
const webpack = require("webpack");

const argsParser = new ArgumentParser({
  prog: "immersive-scripting-api",
  description: "Immersive Scripting API CLI",
});

argsParser.add_argument("-v", "--version", { action: "version", version });

const commandsSubparser = argsParser.add_subparsers({
  title: "Command",
  dest: "command",
});

const compileArgsParser = commandsSubparser.add_parser("compile", {
  aliases: ["c"],
  help: "Compile input script",
});
compileArgsParser.add_argument("-m", "--mode", { choices: ["production", "development"], default: "production", help: "Mode" });
compileArgsParser.add_argument("platform", { choices: ["android", "ios", "web"], help: "Target platform" });
compileArgsParser.add_argument("input", { help: "Input script source file path" });
compileArgsParser.add_argument("output", { nargs: "?", help: "Output script bundle file path" });

const { command, ...commandArgs } = argsParser.parse_args();

switch (command) {
  case "compile":
    webpack(
      require("./webpack.config")(
        {
          ...process.env,
          SCRIPTING_TARGET: commandArgs.platform,
          SCRIPTING_INPUT: commandArgs.input,
          SCRIPTING_OUTPUT: commandArgs.output,
        },
        {
          mode: commandArgs.mode,
        }
      )
    ).run((err, stats) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
      if (stats.hasErrors()) {
        console.error(stats.toString());
        process.exit(1);
      }
      process.exit(0);
    });
    break;
}
