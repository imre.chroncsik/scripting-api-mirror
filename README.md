# Immersive Cross-platform Interactive Engine API

[![Pipeline Status][status-image]][status-url]

[status-image]: https://screwdriver.ouroath.com/pipelines/1048040/badge
[status-url]: https://screwdriver.ouroath.com/pipelines/1048040

## API

Please refer to the [API design document](https://docs.google.com/document/d/1s6yH9fUfOqRhZiYQ1dlSIMBE3l-cjdATBSMtIwqBYjQ/edit#heading=h.auoxqa8qv5lo).

## Compiling a script

To compile a script for a specific `PLATFORM` (`android`, `ios`, or `web`), execute following:

`npm run cli compile PLATFORM path/to/input.ts path/to/output`

or

`npx @immersive-composer/scripting-api@TAG compile PLATFORM path/to/input.ts path/to/output`,

where `TAG` can be:

-   `latest` to use most-recent release version
-   `snapshot` to use most-recent snapshot version
-   `feature1234` to use most-recent PR (with `1234` as it's number) version

## Compiling bundled scripts

The package also contains `immersive-scripts` to showcase and test the Interactive Engine. Please refer to the [Implementation status matrix](https://docs.google.com/spreadsheets/d/1He0znBIKrl4AKZYHcdfwvooJ5EMYjDhXubcd1RWhluI/edit#gid=0) for details.
