import { Component, experience, MeshRenderableComponent, TransformComponent, Color, System, Entity } from "arsdk";
import { assert, assertEqual, assertNotNull, assertNull } from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

class TestComponent extends Component {
  name: String;
  constructor(name: String) {
    super();
    this.name = name;
  }
}

class IteratorPerformanceTestSystem extends System {
  update(dt: number): void {
    let c = 0;
    experience.forEachComponent(TestComponent, (testComponent) => {
      testComponent.name = "" + c++;
      //console.log("testComponent.name " + testComponent.name);
    });
  }
}
const iteratorPerformancetestSystem = new IteratorPerformanceTestSystem();

var testCleanupEntity: Entity | null;
experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    let testCounter = 0;

    //TODO: Composer currently inserts one extra node. Can we remove this? XR-6098
    //object root -> composer extra node
    rootEntity = rootEntity.getChild(0)!;
    rootEntity.getComponent(TransformComponent)!.forEachChild((transform) => {
      testCounter++;
    });
    assertEqual(testCounter, 2, "forEachChild incorrect");

    let stuffTransform = rootEntity.getComponent(TransformComponent)!.findChild((transform) => {
      return transform.entity?.name == "Stuff";
    });
    assert(stuffTransform != null && stuffTransform.entity?.name == "Stuff", "could not find child");

    testCounter = 0;
    rootEntity.getComponent(TransformComponent)!.forEachDescendantInHierarchy((transform) => {
      testCounter++;
    });
    assertEqual(testCounter, 288, "forEachDescendantInHierarchy incorrect");

    //check equivalent JS implementation
    testCounter = 0;
    forEachDescendantJSImplementation(rootEntity, () => {
      testCounter++;
    });
    assertEqual(testCounter, 288, "forEachDescendantJSImplementation incorrect");

    let cube007Transform = rootEntity.getComponent(TransformComponent)!.findDescendantInHierarchy((transform) => {
      return transform.entity?.name == "Cube.007";
    });
    assert(cube007Transform != null && cube007Transform.entity?.name == "Cube.007", "could not find child");

    let i = 3;
    rootEntity.addComponent(new TestComponent("test component 1"));
    rootEntity.addComponent(new TestComponent("test component 2"));
    testCounter = 0;

    experience.forEachComponent(TestComponent, (testComponent) => {
      //test adding component while iterating
      let addedComponent = rootEntity.addComponent(new TestComponent("test component " + i++));
      assert(addedComponent != null);
      testCounter++;
    });
    assertEqual(testCounter, 2, "forEachComponent with addComponent incorrect");
    assertEqual(rootEntity.getComponents(TestComponent).length, 4, "wrong component count");

    testCounter = 0;
    experience.forEachComponent(TestComponent, (testComponent) => {
      testComponent.entity!.removeComponent(testComponent);
      testCounter++;
    });
    assertEqual(testCounter, 4, "forEachComponent with removeComponent incorrect");

    experience.forEachComponent(TestComponent, (testComponent) => {
      assert(false, "All TestComponent components were removed");
    });

    testCounter = 0;
    experience.forEachComponent(MeshRenderableComponent, (meshRenderable) => {
      meshRenderable.material.color.set(Math.random(), Math.random(), Math.random(), Math.random());
      testCounter++;
    });
    assertEqual(testCounter, 283, "forEachComponent MeshRenderable incorrect");

    //test nested iterators
    for (i = 0; i < 5; ++i) {
      let result = rootEntity.addComponent(new TestComponent("test component " + i));
      assertNotNull(result, "Component could not be added");
    }
    let a = 0;
    let b = 0;
    let c = 0;
    experience.forEachComponent(TestComponent, (testComponent) => {
      experience.forEachComponent(TestComponent, (testComponent) => {
        experience.forEachComponent(TestComponent, (testComponent) => {
          if (b == 0) {
            assertNotNull(testComponent.entity);
            //the newly added component is not immediately iterated over in the loop
            rootEntity.addComponent(new TestComponent("test component " + i));
          } else {
            assertNull(testComponent.entity);
          }
          if (testComponent.entity) {
            //the removed components will still be iterated over until the outer most loop is left
            let removed = testComponent.entity!.removeComponent(testComponent);
            console.log("testComponent " + testComponent.name + " removed " + removed);
          }
          c++;
        });
        b++;
      });
      a++;
    });

    assertEqual(a, 5, "Nested forEachComponent incorrect");
    assertEqual(b, 25, "Nested forEachComponent incorrect");
    assertEqual(c, 125, "Nested forEachComponent incorrect");

    let d = 0;
    experience.forEachComponent(TestComponent, (testComponent) => {
      d++;
      testComponent.entity!.removeComponent(testComponent);
    });
    assertEqual(d, 5, "forEachComponent incorrect");

    let e = 0;
    let e2 = 0;
    let testEntity = new Entity();
    testEntity.name = "Test Entity";

    let testEntity2 = new Entity();
    testEntity2.name = "Test Entity2";
    let numTestEntity2Callbacks = 0;
    let numTestEntity2CallbacksInner = 0;
    rootEntity.forEachDescendantInHierarchy((entity) => {
      if (entity === testEntity2) {
        numTestEntity2Callbacks++;
      } else {
        //this should have an effect on the callbacks,
        //as we iterate top-down and "entity"'s children were not yet iterated over
        testEntity2.parent = entity;
      }
      //this should have no effect on the callbacks
      testEntity.parent = rootEntity;
      entity.forEachDescendantInHierarchy((entity2) => {
        if (entity === testEntity2) {
          numTestEntity2CallbacksInner++;
        }
        e++;
      });
      e2++;
    });
    testEntity.destroy();
    testEntity2.destroy();

    console.log(" numTestEntity2CallbacksInner " + numTestEntity2CallbacksInner);
    assertEqual(numTestEntity2Callbacks, 288, "Nested forEachDescendantInHierarchy incorrect");
    assertEqual(e, 578, "Nested forEachDescendantInHierarchy incorrect");
    assertEqual(e2, 576, "Nested forEachDescendantInHierarchy incorrect");

    let f = 0;
    let f2 = 0;

    testCleanupEntity = new Entity();
    testCleanupEntity.name = "Clean up Entity";
    let removedChilds = new Set<Entity>();
    rootEntity.forEachDescendantInHierarchy((entity) => {
      assert(
        !removedChilds.has(entity),
        "Entity " +
          entity.name +
          " has been removed before iterating over its parents children has begun. forEachDescendantInHierarchy should not call back with it"
      );
      assertNotNull(entity.parent);
      //forEachDescendantInHierarchy will still callback with this entity even in nested call
      //as we already started iterating over children at this level (its parents children)
      entity.parent = testCleanupEntity;
      if (entity.childCount() > 0) {
        //as we have not been directly iterating over this entity's direct children yet
        //forEachDescendantInHierarchy will not call back with the removed child
        removedChilds.add(entity.getChild(0)!);
        entity.getChild(0)!.parent = testCleanupEntity;
      }
      rootEntity.forEachDescendantInHierarchy((entity2) => {
        f++;
      });

      //transform function should be equivalent
      rootEntity.transform.forEachDescendantInHierarchy((transform) => {
        f2++;
      });
    });
    assertEqual(removedChilds.size, 2, "Nested forEachDescendantInHierarchy incorrect. Incorrect number of removed children.");
    assertEqual(f, 79816, "Nested forEachDescendantInHierarchy incorrect");
    assertEqual(f2, 79816, "Nested forEachDescendantInHierarchy incorrect");

    //for performance test
    for (i = 0; i < 2000; ++i) {
      rootEntity.addComponent(new TestComponent("test component " + i));
    }
    experience.addSystem(iteratorPerformancetestSystem);

    console.log("All tests succeeded! Performance test is still running.");
  },
  () => {
    testCleanupEntity?.destroy();
    experience.removeSystem(iteratorPerformancetestSystem);
  }
);

function forEachDescendantJSImplementation(root: Entity, callback: (component: Entity) => void) {
  root.forEachChild((child) => {
    callback(child);
    forEachDescendantJSImplementation(child, callback);
  });
}
