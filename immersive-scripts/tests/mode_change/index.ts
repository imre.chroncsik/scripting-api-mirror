import { ARMode, experience, PreviewMode } from "arsdk";
import { assertEqual } from "../../../lib/assert";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 */
assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

//test AR mode switch
let isAR = experience.currentMode.isAR;
let objectPlaced = false;
console.log("isAR: " + experience.currentMode.isAR);

experience.addOnModeChangedListener((currentMode: ARMode | PreviewMode) => {
  console.log("on mode changed " + currentMode);
  isAR = currentMode.isAR;
  assertEqual(isAR, experience.currentMode.isAR);
  assertEqual(objectPlaced, false, "Object was not yet destroyed");
});

experience.addImmersiveObjectLifecycleListeners(
  CUBE_SCENE_OBJECT_UID,
  (rootEntity) => {
    objectPlaced = true;
    console.log("object placed. isAR: " + experience.currentMode.isAR);
    assertEqual(experience.currentMode.isAR, isAR, "Mode changed before addOnModeChangedListener was called");
  },
  () => {
    objectPlaced = false;
    console.log("object removed. isAR: " + experience.currentMode.isAR);
    assertEqual(experience.currentMode.isAR, isAR, "Mode changed before addOnModeChangedListener was called");
  }
);
