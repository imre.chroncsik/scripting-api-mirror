import { Color } from "arsdk";
import { assertEqual, assertColorsEqual, assertThrows, assertNotStrictEqual, assertColorsAlmostEqual, assertIsNaN } from "../../../lib/assert";

assertColorsEqual(new Color(0, 0, 0), new Color(0, 0, 0, 1));
let color = Color.fromHex("#FF0000");
assertEqual(color.r, 1, "Color.fromHex incorrect");
assertEqual(color.g, 0, "Color.fromHex incorrect");
assertEqual(color.b, 0, "Color.fromHex incorrect");
assertEqual(color.a, 1, "Color.fromHex incorrect");
let color2 = new Color(0.5, 0.5, 0.5, 0.5);
assertEqual(color2.r, 0.5, "color constructor incorrect");
assertEqual(color2.g, 0.5, "color constructor incorrect");
assertEqual(color2.b, 0.5, "color constructor incorrect");
assertEqual(color2.a, 0.5, "color constructor incorrect");
color2.set(1, 2, 3);
assertEqual(color2.r, 1, "color.set(3) incorrect");
assertEqual(color2.g, 2, "color.set(3) incorrect");
assertEqual(color2.b, 3, "color.set(3) incorrect");
assertEqual(color2.a, 0.5, "color.set(3) incorrect");
color2.set(1, 2, 3, undefined);
assertEqual(color2.a, 0.5, "color.set(3) incorrect");
color2.set(1, 2, 3, Number.NaN); //edge case in android implementation
assertIsNaN(color2.a, "color.set(3) incorrect, alpha should be NaN");
color2.set(2, 3, 4, 5);
assertEqual(color2.r, 2, "color.set incorrect");
assertEqual(color2.g, 3, "color.set incorrect");
assertEqual(color2.b, 4, "color.set incorrect");
assertEqual(color2.a, 5, "color.set incorrect");
assertColorsEqual(Color.fromHex("#0000FF"), new Color(0, 0, 1, 1));
assertColorsAlmostEqual(Color.fromHex("#80808080"), new Color(0.5019608, 0.5019608, 0.5019608, 0.5019608));
assertColorsEqual(Color.fromHex("#0000FF00"), new Color(0, 0, 1, 0));
assertColorsEqual(Color.fromHex("#FF000000"), new Color(1, 0, 0, 0));
assertColorsEqual(Color.fromHex("FF000000"), new Color(1, 0, 0, 0));
assertColorsEqual(Color.fromHex("#ff000000"), new Color(1, 0, 0, 0));
assertColorsEqual(Color.fromHex("ff000000"), new Color(1, 0, 0, 0));
assertColorsEqual(new Color(1, 0, 0, 0).setHex("#8021FA80"), Color.fromHex("#8021FA80"));
let colorOutOfRange = new Color(-1, 515, -562);
colorOutOfRange.b = -252;
assertEqual(colorOutOfRange.r, -1);
assertEqual(colorOutOfRange.g, 515);
assertEqual(colorOutOfRange.b, -252);
assertEqual(colorOutOfRange.a, 1);

assertThrows(() => {
  Color.fromHex("#f000000");
}, "Invalid color format");
assertThrows(() => {
  Color.fromHex("#f0000");
}, "Invalid color format");

assertThrows(() => {
  Color.fromHex("#FF000G");
}, "Invalid color format");

//color conversions
let baseColor = Color.fromHex("#8021FA80");
let linearColor = baseColor.clone().convertSRGBToLinear();
assertColorsAlmostEqual(
  linearColor,
  new Color(0.2158605307340622, 0.015208514407277107, 0.9559735059738159, 0.501960813999176),
  "convertSRGBToLinear() incorrect"
);
assertColorsAlmostEqual(linearColor.convertLinearToSRGB(), baseColor, "convertLinearToSRGB() incorrect");

let color1 = new Color(1, 0.3125, 1);
let color1clone = color1.clone();
assertNotStrictEqual(color1, color1clone);
assertColorsEqual(color1clone, color1);
color1clone.b = 5;
assertEqual(color1.b, 1);
color1.copy(color1clone);
assertEqual(color1.b, 5, "color.copy() incorrect");

console.log("All tests succeeded! :)");
