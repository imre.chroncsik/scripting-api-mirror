import { Entity, experience, MeshRenderableComponent, OnTapComponent, Quaternion, Vector3 } from "arsdk";
import { CUBE_SCENE_EXPERIENCE_UID, CUBE_SCENE_OBJECT_UID } from "../constants/cube_scene";
import {
  TweenComponent,
  MoveTransformComponent,
  TweenDirection,
  Interpolator,
  ScaleTransformComponent,
  RotateTransformComponent,
  SpringInterpolator,
} from "arsdk/tween";
import { assertEqual } from "../../../lib/assert";

/**
 * Test experience
 * Composer: https://short.staging.immersive.yahoo.com/mwtaL
 * Android: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/android/index.json
 * iOS: https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/ios/index.json
 * Web: https://cdn.launch3d.com/cu/xr/preview-stable/index.html?customJsonPath=https://staging-immersive-composer-published-experiences.s3.amazonaws.com/experiences/dca941bb-099a-44d0-a2ce-510cc3f0afff/v1/web/index.json
 *
 * Video: https://www.youtube.com/watch?v=22ZducT2InQ
 */

assertEqual(experience.uid, CUBE_SCENE_EXPERIENCE_UID, "Wrong experience uid");

experience.addImmersiveObjectLifecycleListeners(CUBE_SCENE_OBJECT_UID, (rootEntity) => {
  const cube1 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "BigCube1";
  })!;
  const cube2 = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "BigCube2";
  })!;
  const cubes = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Cubes";
  })!;
  const ball = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Sphere";
  })!;
  const plane = rootEntity.findDescendantInHierarchy((entity) => {
    return entity.name == "Plane";
  })!;

  let ballMoving = false;
  let ballMoveTween = new MoveTransformComponent(
    ball.transform.worldPosition,
    new Vector3(3, 3, 3),
    2.5,
    () => {
      console.log("move finished");
    },
    Interpolator.easeInOutExpo,
    TweenDirection.Alternate,
    true,
    (entity: Entity, vector: Vector3) => {}
  );
  ball.addComponent(
    new OnTapComponent(() => {
      if (!ballMoving) {
        ball.addComponent(ballMoveTween);
      } else {
        ball.removeComponent(ballMoveTween);
      }
      ballMoving = !ballMoving;
    })
  );

  cube1.addComponent(
    new OnTapComponent(() => {
      cube1.addComponent(
        new MoveTransformComponent(new Vector3(1, 1, 3), new Vector3(3, 1, 3), 1, () => {
          cube1.addComponent(
            new MoveTransformComponent(cube1.transform.worldPosition, new Vector3(3, 1, 5), 1, () => {
              cube1.addComponent(
                new MoveTransformComponent(cube1.transform.worldPosition, new Vector3(1, 1, 5), 1, () => {
                  cube1.addComponent(
                    new MoveTransformComponent(cube1.transform.worldPosition, new Vector3(1, 1, 3), 1, () => {
                      console.log("MoveTransformComponent finished");
                    })
                  );
                })
              );
            })
          );
        })
      );
    })
  );

  let cube2MoveTween = new MoveTransformComponent(
    new Vector3(1, 1, 5),
    new Vector3(3, 1, 5),
    2.5,
    () => {
      console.log("MoveTransformComponent finished");
    },
    Interpolator.easeInOutQuad,
    undefined,
    true
  );
  cube2.addComponent(
    new OnTapComponent(() => {
      if (cube2.getComponent(MoveTransformComponent)) {
        cube2MoveTween.reverse();
      } else {
        cube2.addComponent(cube2MoveTween);
      }
    })
  );

  plane.addComponent(
    new OnTapComponent(() => {
      plane.addComponent(new ScaleTransformComponent(new Vector3(1, 1, 5), new Vector3(3, 1, 5), 2.5, null, Interpolator.easeInOutQuad));
      plane.addComponent(
        new RotateTransformComponent(new Quaternion(), new Quaternion().setFromAxisAngle(new Vector3(0, 1, 0), 90), 2.5, null, Interpolator.easeInOutQuad)
      );

      let a = 0;
      experience.forEachComponent(ScaleTransformComponent, (tweenComponent) => {
        a++;
      });
      console.log("ScaleTransformComponents " + a);
    })
  );

  //generalized tweening component
  cubes.forEachDescendantInHierarchy((child) => {
    child.addComponent(
      new OnTapComponent(() => {
        child.addComponent(
          new TweenComponent(
            0,
            1,
            3.0,
            (entity: Entity, a: number) => {
              let i = 0;
              cubes.forEachDescendantInHierarchy((child2) => {
                let height = 0.3 + i++ * 0.005;
                child2.transform.localPosition.y = 0 + a * height;
                child2.getComponent(MeshRenderableComponent)!.material.color.set(1 - a, 1, 1, 1);
              });
            },
            new SpringInterpolator(1, 80, 10, 0),
            null,
            TweenDirection.Normal,
            false
          )
        );
      })
    );
  });
});
