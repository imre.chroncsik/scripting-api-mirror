import {
  AudioSourceComponent,
  BoxColliderComponent,
  Color,
  Component,
  Entity,
  MeshRenderableComponent,
  OnTapComponent,
  OrbitCameraControllerComponent,
  Quaternion,
  System,
  TransformComponent,
  Vector3,
  AudioClip,
} from "arsdk";
import { assertThrows } from "../../../lib/assert";

//non-extendable classes. Should throw error when instantiated
class EntitySubclass extends Entity {}
class TransformSubclass extends TransformComponent {}
class OnTapSubclass extends OnTapComponent {}
class MeshRenderableSubclass extends MeshRenderableComponent {}
class Vector3Subclass extends Vector3 {}
class QuaternionSubclass extends Quaternion {}
class ColorSubclass extends Color {}
class AudioSourceSubclass extends AudioSourceComponent {}
class OrbitCameraControllerSubclass extends OrbitCameraControllerComponent {}
class BoxColliderSubclass extends BoxColliderComponent {}
class AudioClipSubclass extends AudioClip {}

//extendable classes
class ComponentSubclass extends Component {}
class SystemSubclass extends System {
  update(dt: number): void {}
}

new ComponentSubclass();
new SystemSubclass();

assertThrows(() => {
  new EntitySubclass();
}, "should not be able to extend from Entity");

assertThrows(() => {
  new TransformSubclass();
}, "should not be able to extend from Transform");

assertThrows(() => {
  new OnTapSubclass(() => {});
}, "should not be able to extend from OnTap");

assertThrows(() => {
  new MeshRenderableSubclass();
}, "should not be able to extend from MeshRenderable");

assertThrows(() => {
  new Vector3Subclass();
}, "should not be able to extend from Vector3");

assertThrows(() => {
  new QuaternionSubclass();
}, "should not be able to extend from Quaternion");

assertThrows(() => {
  new ColorSubclass(1, 1, 1, 1);
}, "should not be able to extend from Color");

assertThrows(() => {
  new AudioSourceSubclass();
}, "should not be able to extend from AudioSource");

assertThrows(() => {
  new OrbitCameraControllerSubclass();
}, "should not be able to extend from OrbitCameraController");

assertThrows(() => {
  new BoxColliderSubclass();
}, "should not be able to extend from BoxCollider");

assertThrows(() => {
  new AudioClipSubclass();
}, "should not be able to extend from AudioClip");

console.log("All tests succeeded! :)");
