const fs = require("fs");
const path = require("path");

function createEntryWrappers({ entryFilePaths, wrappersFolderPath, arsdkRootFolder }) {
  let wrapperPaths = [];

  if (fs.existsSync(wrappersFolderPath)) fs.rmdirSync(wrappersFolderPath, { recursive: true });
  fs.mkdirSync(wrappersFolderPath, { recursive: true });

  let wrapperCount = 0;
  for (let entryFilePath of entryFilePaths) {
    const parsedEntryFilePath = path.parse(entryFilePath);
    const entryModulePath = path.join(parsedEntryFilePath.dir, parsedEntryFilePath.name);

    const content = `
;(globalThis as any).platform = "ios"
import { init } from "${arsdkRootFolder}/lib/ios/ios-bootstrap"
init(() => import(/*webpackMode: "eager"*/"${entryModulePath}")) `;

    const wrapperFilePath = path.join(wrappersFolderPath, `ios-entry-wrapper-${wrapperCount}.ts`);
    fs.writeFileSync(wrapperFilePath, content);

    wrapperPaths.push(wrapperFilePath);
    ++wrapperCount;
  }

  return wrapperPaths;
}

module.exports = { createEntryWrappers };
